from facemodel import MobileFaceNet
from facemodel_trainer import MobileFaceNetTrainer
import matplotlib
import logging

from tensorflow.python.client import device_lib

import os

import face_dataset
device_lib.list_local_devices()
logging.getLogger('matplotlib').setLevel(logging.WARNING)
logging.getLogger('tensorflow').setLevel(logging.WARNING)

# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
# os.environ['TF_DETERMINISTIC_OPS'] = '1'
matplotlib.use('Agg')


# Parameters
batch_size = 32             # The number of images processed simultaneously
dataset = 'CASIA'           # The dataset used for training
fold = 'fold'               # Folding of the batch normalisation
runtype = 'fullmodel'       # Quantize whole model
# runtype = 'individual'    # Quantize individual layer
alpha = 'auto'              # Automatic scaling
bits = 2                    # Nr. of quantization bits
split = False               # The split of the DSC layer


# for bits in [8, 4, 2]:
#     print(bits)
if dataset == 'CASIA':
    train_dataset = face_dataset.load_tfrecord_dataset(tfrecord_name='../dataset/CASIA.tfrecord',
                                                       batch_size=batch_size,
                                                       binary_img=True,
                                                       shuffle=True,
                                                       is_ccrop=False,
                                                       one_hot_depth=10575)
    model = MobileFaceNetTrainer(name=f'MobileFaceNet_{fold}_{bits}bit_{runtype}_{alpha}',
                                 dataset='CASIA',
                                 batch_size=batch_size,
                                 split=split)
    model.name_ref = f'MobileFaceNet_{fold}_32bit_pretrain'
    if fold == 'fold':
        if not split:
            # model.pre_train_checkpoint = 'MobileFaceNet_fold_2022-04-26T09-57-29' # no split
            model.pre_train_checkpoint = 'MobileFaceNet_fold_2022-05-03T10-59-17'  # no split no batchnormalisation after dwc

        else:
            # model.pre_train_checkpoint = 'MobileFaceNet_fold_2022-03-16T12-13-27' # split
            # model.pre_train_checkpoint = 'MobileFaceNet_fold_2022-04-15T12-31-48' # split
            model.pre_train_checkpoint = 'MobileFaceNet_fold_2022-05-04T09-35-41' # split no bn
    else:
        model.pre_train_checkpoint = 'MobileFaceNet_nofold_2022-04-12T11-38-52' # split

else:
    train_dataset = face_dataset.load_tfrecord_dataset(tfrecord_name='../dataset/ms1m.tfrecord',
                                                       batch_size=batch_size,
                                                       binary_img=True,
                                                       shuffle=True,
                                                       is_ccrop=False,
                                                       one_hot_depth=85742)
    model = MobileFaceNetTrainer(name=f'MobileFaceNet_{fold}_{bits}bit_{runtype}_ms1m',
                                 dataset='ms1m',
                                 batch_size=batch_size,
                                 split=split)
    model.name_ref = f'MobileFaceNet_{fold}_ms1m'
    if fold == 'fold':
        model.pre_train_checkpoint = 'MobileFaceNet_fold_2022-04-04T11-13-56'
    else:
        model.pre_train_checkpoint = 'MobileFaceNet_nofold_2022-04-08T11-34-06'

model.qalpha = alpha
model.folded = True if fold == 'fold' else False

# model.quantize_checkpoint = 'MobileFaceNet_fold_2022-03-20T13-03-01'

model.train_dataset = train_dataset
model.test_dataset_path = '../dataset/'
model.batch_size = batch_size
# model.df_mobilefacenet_8bit = 'MobileFaceNet_df_scores_8bit.csv'


# First train with 'SOFT' and then continue with 'ARC'
# model.pretrain_facemodel_mini('SOFT')
# model.pretrain_facemodel_mini('ARC', load_ckpt='ARC', continue_run=True, max_epochs=10)
# model.pretrain_facemodel_mini('ARC', load_ckpt='ARC', max_epochs=40, initial_epoch=19, lr_override=0.005)

#
individual = True if runtype == 'individual' else False
print(f'individual is {individual}')
print(f'scaling is {alpha}')
print(f'bits is {bits}')

# model.train_equal_qface(bit=bits, forward=True, num_epochs=10, individual=individual, load_ckpt=0)
# model.train_last_qlayer(num_epochs=10)

model.train_tflite()

# model.all_atonce(bit=bits)
# model.evaluate_full(bit=bits)
# model.mixed_precision()
