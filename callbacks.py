import keras.callbacks
import numpy as np
import matplotlib.pyplot as plt
import os
import tensorflow as tf
import logging

logging.getLogger('matplotlib').setLevel(logging.WARNING)
logging.getLogger('tensorflow').setLevel(logging.WARNING)


class CustomCallback(keras.callbacks.Callback):
    """ A Callback function for the visualisation of the network during training"""

    def __init__(self, model, name, logger, x_test, y_test, testnr=0, imgs='all'):
        self.x_test = x_test
        self.y_test = y_test
        self.testnr = testnr
        self.vmodel = model
        self.imgs = imgs
        self.logger = logger
        self.name = name

        self.losses = []
        self.acc = []
        self.val_losses = []
        self.val_acc = []
        self.logs = []
        self.training_fig, self.ax = plt.subplots(figsize=(10, 5))
        self.ax2 = self.ax.twinx()

    # def on_train_begin(self, logs=None):
    #     # Before training visualize begin state
        # if 'weights' in self.imgs or self.imgs == 'all':
        #     self.vmodel.visualize_weights(0)
        # if 'response' in self.imgs or self.imgs == 'all':
        #     self.vmodel.visualize_responses(x_test[self.testnr], 0)

    def on_train_end(self, logs=None):
        # Before training visualize begin state
        # self.vmodel.close_figs()
        # Save plt
        os.makedirs('img/' + self.vmodel.name + '/training/', exist_ok=True)
        self.ax.legend(['loss', 'val_loss'])
        self.ax2.legend(['accuracy', 'val_accuracy'])
        self.training_fig.savefig('img/' + self.vmodel.name + '/training/' + self.name + '_training.png')
        plt.close(self.training_fig)

    def on_epoch_end(self, epoch, logs={}):
        # After each epoch visualize
        curr_loss = logs.get('loss')
        curr_acc = logs.get('accuracy')
        val_loss = logs.get('val_loss')
        val_acc = logs.get('val_accuracy')
        self.logger.info("epoch = %4d  loss = %0.6f  acc = %0.6f  val_loss = %0.6d  val_acc = %0.6f"
                      % (epoch + 1, curr_loss, curr_acc, val_loss, val_acc))

        # if 'weights' in self.imgs or self.imgs == 'all':
        #     self.vmodel.visualize_weights(epoch + 1)
        # if 'response' in self.imgs or self.imgs == 'all':
        #     self.vmodel.visualize_responses(x_test[self.testnr], epoch + 1)

        # Append the logs, losses and accuracies to the lists
        self.logs.append(logs)
        self.losses.append(logs.get('loss'))
        self.acc.append(logs.get('accuracy'))
        self.val_losses.append(logs.get('val_loss'))
        self.val_acc.append(logs.get('val_accuracy'))

        # Before plotting ensure at least 2 epochs have passed
        if len(self.losses) > 1:
            N = np.arange(1, len(self.losses) + 1)

            # Plot train loss, train acc, val loss and val acc against epochs passed
            self.ax.plot(N, self.losses, c='red', label="train_loss")
            self.ax2.plot(N, self.acc, c='green', label="train_acc")
            self.ax.plot(N, self.val_losses, c='pink', label="val_loss")
            self.ax2.plot(N, self.val_acc, c='blue', label="val_acc")
            self.ax.title.set_text("Training Loss and Accuracy")
            self.ax.set_xlabel("Epoch #")
            self.ax.set_ylabel("Loss")
            self.ax2.set_ylabel("Accuracy")


class FaceEvalCallback(keras.callbacks.Callback):
    def __init__(self, eval_fn, logger, summary, score_begin, score_during, update_freq=1):
        super().__init__()
        self.eval_fn = eval_fn
        self.logger = logger
        self.summary = summary
        self.score_begin = score_begin
        self.score_during = score_during
        self.update_freq = update_freq

    def on_train_begin(self, logs=None):
        self.eval_fn(self.score_begin)
        self.logger.info(f"Original scores were: LFW ({self.score_begin[0]}), AgeDB-30 ({self.score_begin[1]})"
                         f" and CFP-FP ({self.score_begin[2]})")

    def on_epoch_end(self, epoch, logs=None):
        curr_loss = logs.get('loss')
        try:
            curr_acc = logs.get('accuracy')
            self.logger.info(f"epoch = {(epoch+1):4d} loss = {curr_loss:0.6f}  acc = {curr_acc:0.6f}")
        except:
            curr_mse = logs.get('mse')
            self.logger.info(f"epoch = {(epoch+1):4d} loss = {curr_loss:0.6f}  mse = {curr_mse:0.6f}")
        self.eval_fn(self.score_during)
        self.summary(epoch)


class MobileNetCallback(keras.callbacks.Callback):
    """ A Callback function for the visualisation of the network during training"""

    def __init__(self, logger):
        self.logger = logger

    def on_epoch_end(self, epoch, logs={}):
        # After each epoch visualize
        curr_loss = logs.get('loss')
        curr_acc = logs.get('accuracy')
        val_loss = logs.get('val_loss')
        val_acc = logs.get('val_accuracy')
        self.logger.info("epoch = %4d  loss = %0.6f  acc = %0.6f  val_loss = %0.6d  val_acc = %0.6f"
                         % (epoch + 1, curr_loss, curr_acc, val_loss, val_acc))
