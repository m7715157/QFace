from model import MainModel
from keras.layers import *
from keras.models import Model
from qkeras import *
from tensorflow import optimizers
from keras import losses
import tensorflow_model_optimization as tfmot

import logging
logging.getLogger('proplot').setLevel(logging.WARNING)


class MNISTModel(MainModel):
    def __init__(self, mnist_type=None, dataset='', seed=2, bits=None, split=False, shape=(28, 28, 1), classes=10,
                 stride=1, bias=False, folded_bn=False, auto_alpha=True, qint=0):
        self.split = split
        super().__init__()

        # MNIST Convolutional stuff
        self.activation_type = 'relu'
        self.inputs = Input(shape, name='input_0')
        self.flatten = Flatten()
        self.dense = Dense(classes, activation='softmax')
        self.qdense = QDense(classes,
                             kernel_quantizer=quantized_bits(bits=8, integer=0, alpha=1),
                             bias_quantizer=quantized_bits(bits=32, integer=0, alpha=1),
                             activation='softmax')
        self.mnist_type = mnist_type
        self.stride = stride
        self.bias = bias
        self.folded = folded_bn
        self.qalpha = 1 if auto_alpha is False else 'auto'
        self.model = self.mnist_model()
        self.df_scores_name = f'df_score_bits_{self.qalpha}.csv'
        self.seed = seed
        self.qint = qint

        # Model init stuff
        if dataset == 'mnist' or dataset == 'MNIST':
            self.dataset = ''
        else:
            self.dataset = f'{dataset}_'
        self.get_model = self.mnist_model
        self.model = self.get_model()
        self.name_ref = self.name
        self.loss_fn = losses.categorical_crossentropy
        self.optim_fn = optimizers.Adam()

        if dataset == 'mnist' or dataset == 'MNIST':
            self.dataset = ''
        else:
            self.dataset = f'{dataset}'

        self.init_paths()
        self.init_log(f'LOG_alpha-{self.qalpha}')
        # self.close_log()

    from _layers import cba, ca, depthwise_conv, residual, act, separable
    from _qlayers import Qcba, Qca, Qdepthwise_conv, Qresidual, Qact, Qseparable
    from _visualize import visualize_weights, visualize_responses

    def __getattr__(self, name):
        return getattr(self.model, name)

    def mnist_model(self):
        x = _inputs = self.inputs
        x = self.mut(x)
        x = self.flatten(x)
        if self.quantize:
            out = self.qdense(x)
        else:
            out = self.dense(x)
        self.layer_nr = 0
        return Model(_inputs, out, name=self.name)

    def mut(self, x):
        if self.mnist_type == 'conv':
            return self.mnist_conv(x)
        elif self.mnist_type == 'depthwise_conv':
            return self.mnist_depthwise(x)
        elif self.mnist_type == 'separable_conv':
            return self.mnist_separable(x, split=self.split)
        elif self.mnist_type == 'inverted_residual':
            return self.mnist_residual(x, split=self.split)
        elif self.mnist_type == 'separableMB_conv':
            return self.mnist_separable(x, split=True)
        elif self.mnist_type == 'inverted_residualMB':
            return self.mnist_residual(x, split=True)
        elif self.mnist_type == 'lin_global_depthwise_conv':
            return self.mnist_global_depthwise_lin(x)
        elif self.mnist_type == 'lin_pointwise_conv':
            return self.mnist_pointwise_lin(x)
        else:
            return print(self.mnist_type, "Not Found!")

    def mnist_conv(self, x):
        self.name = f'MNIST_{self.dataset}Model_cba_s-{self.seed}_f-{self.folded}_a-{self.qalpha}_i-{self.qint}'
        self.layer_type = 'convolutional'
        self.mut_config = 'conv-batch-act' if self.folded is False else 'fold(conv-batch)-act'
        if self.quantize:
            x = self.Qcba(depth=3, kernel=5, stride=self.stride, name='Qcba1' if self.folded is False else 'Qcbfa1')(x)
            x = self.Qcba(depth=3, kernel=5, stride=self.stride, name='Qcba2' if self.folded is False else 'Qcbfa2')(x)

        else:
            x = self.cba(depth=3, kernel=5, stride=self.stride, name='cba1' if self.folded is False else 'Qcbfa1')(x)
            x = self.cba(depth=3, kernel=5, stride=self.stride, name='cba2' if self.folded is False else 'Qcbfa2')(x)
        return x

    def mnist_depthwise(self, x):
        self.name = f'MNIST_{self.dataset}Model_dw_s-{self.seed}_f-{self.folded}_a-{self.qalpha}_i-{self.qint}'
        self.layer_type = 'depthwise convolutional'
        self.mut_config = 'cba-depthwise_conv-batch-act' if self.folded is False else 'cbfa-fold(depthwise_conv)-act'
        if self.quantize:
            x = self.Qcba(depth=3, kernel=5, name='Qcba' if self.folded is False else 'Qcbfa')(x)
            x = self.Qdepthwise_conv(kernel=5, stride=self.stride,
                                     name='Qdepthwise' if self.folded is False else 'Qdepthwisef')(x)
        else:
            x = self.cba(depth=3, kernel=5, stride=self.stride, name='cba' if self.folded is False else 'Qcbfa')(x)
            x = self.depthwise_conv(kernel=5, stride=self.stride,
                                    name='Qdepthwise' if self.folded is False else 'Qdepthwisef')(x)
        return x

    def mnist_separable(self, x, split=False):
        self.name = f'MNIST_{self.dataset}Model_sep_s-{self.seed}_f-{self.folded}_sp-{split}_a-{self.qalpha}_i-{self.qint}'
        self.layer_type = 'depthwise separable'
        self.mut_config = 'cba-separable' if self.folded is False else 'cbfa-separable'
        if self.quantize:
            x = self.Qcba(depth=3, kernel=5, name='Qcba' if self.folded is False else 'Qcbfa')(x)
            x = self.Qseparable(depth=3, kernel=5, stride=self.stride, split=self.split, name='sep1')(x)
        else:
            x = self.cba(depth=3, kernel=5, stride=self.stride, name='cba' if self.folded is False else 'cbfa')(x)
            x = self.separable(depth=3, kernel=5, stride=self.stride, split=self.split, name='sep1')(x)
        return x

    def mnist_residual(self, x, split=False):
        self.name = f'MNIST_{self.dataset}Model_res_s-{self.seed}_f-{self.folded}_sp-{split}_a-{self.qalpha}_i-{self.qint}'
        self.layer_type = 'inverted residual(depthwise separable)'
        self.mut_config = 'cba-res(sep)' if self.folded is False else 'cbfa-res(sep)'
        if self.quantize:
            x = self.Qcba(depth=3, kernel=5, name='Qcba' if self.folded is False else 'Qcbfa')(x)
            x = self.Qresidual(depth=3, kernel=5, stride=self.stride, split=self.split, name='res1')(x)
        else:
            x = self.cba(depth=3, kernel=5, stride=self.stride, name='cba' if self.folded is False else 'cbfa')(x)
            x = self.residual(depth=3, kernel=5, stride=self.stride, split=self.split, name='res1')(x)
        return x

    def mnist_global_depthwise_lin(self, x):
        self.name = f'MNIST_{self.dataset}Model_lin_gdw_s-{self.seed}_f-{self.folded}_a-{self.qalpha}_i-{self.qint}'
        self.layer_type = 'global depthwise convolutional'
        self.mut_config = 'cba-depthwise_conv-batch' if self.folded is False else 'cbfa-fold(depthwise_conv)'
        if self.quantize:
            x = self.Qcba(depth=3, kernel=5, name='Qcba' if self.folded is False else 'Qcbfa')(x)
            x = self.Qdepthwise_conv(kernel=28, stride=self.stride, act=False, padding='valid',
                                     name='Qdepthwise' if self.folded is False else 'Qdepthwisef')(x)
        else:
            x = self.cba(depth=3, kernel=5, stride=self.stride, name='cba' if self.folded is False else 'cbfa')(x)
            x = self.depthwise_conv(kernel=28, stride=self.stride, act=False, padding='valid',
                                    name='depthwise' if self.folded is False else 'depthwisef')(x)
        return x

    def mnist_pointwise_lin(self, x):
        self.name = f'MNIST_{self.dataset}Model_lin_pw_s-{self.seed}_f-{self.folded}_a-{self.qalpha}_i-{self.qint}'
        self.layer_type = 'linear pointwise convolutional'
        self.mut_config = 'cba-pw-batch' if self.folded is False else 'cbfa-folded(pw)'
        if self.quantize:
            x = self.Qcba(depth=3, kernel=5, name='Qcba' if self.folded is False else 'Qcbfa')(x)
            x = self.Qcba(depth=3, kernel=1, stride=self.stride, act=False,
                          name='Qpw' if self.folded is False else 'Qpwf')(x)
        else:
            x = self.cba(depth=3, kernel=5, stride=self.stride, name='cba' if self.folded is False else 'cbfa')(x)
            x = self.cba(depth=3, kernel=1, stride=self.stride, act=False,
                         name='pw' if self.folded is False else 'pwf')(x)
        return x