import tensorflow as tf
import numpy as np
import logging
import time
import os


# Helper function to run inference on a TFLite model
def run_tflite_model(logger, tflite_file, testx, testy, test_image_indices):
    # Initialize the interpreter
    interpreter = tf.lite.Interpreter(model_content=tflite_file)
    interpreter.allocate_tensors()

    input_details = interpreter.get_input_details()[0]
    output_details = interpreter.get_output_details()[0]

    predictions = np.zeros((len(test_image_indices),), dtype=int)
    timer = []
    for i, test_image_index in enumerate(test_image_indices):
        test_image = testx[test_image_index]
        test_label = testy[test_image_index]

        # Check if the input type is quantized, then rescale input data to uint8
        if input_details['dtype'] == np.uint8:
            input_scale, input_zero_point = input_details["quantization"]
            test_image = test_image / input_scale + input_zero_point

        test_image = np.expand_dims(test_image, axis=0).astype(input_details["dtype"])

        start = time.time()
        interpreter.set_tensor(input_details["index"], test_image)
        interpreter.invoke()
        output = interpreter.get_tensor(output_details["index"])[0]
        predictions[i] = output.argmax()
        end = time.time()
        timer.append(end - start)
    logger.info('\tAverage inference time: %2.6fs' % np.mean(timer))
    return predictions, np.mean(timer)


def get_tflite_models(logger, model, model_name, x_train):
    score_dict = {
        'score_before': 0,

        'cvt(s)_tflite0': 0,
        'cvt(s)_tflite1': 0,
        'cvt(s)_tflite2': 0,
        'cvt(s)_tflite3': 0,

        'size(kB)_tflite0': 0,
        'size(kB)_tflite1': 0,
        'size(kB)_tflite2': 0,
        'size(kB)_tflite3': 0,

        'score(%)_tflite0': 0,
        'score(%)_tflite1': 0,
        'score(%)_tflite2': 0,
        'score(%)_tflite3': 0,

        'inf(ms)_tflite0': 0,
        'inf(ms)_tflite1': 0,
        'inf(ms)_tflite2': 0,
        'inf(ms)_tflite3': 0,
    }

    # Convert to TFLite:
    #################################################################
    tflite_0, score_dict = tflite_simple(logger, model, model_name, score_dict)

    # Convert to TFLite with dynamic range quantization:
    #################################################################
    tflite_1, score_dict = tflite_dynamic_range(logger, model, model_name, score_dict)

    # Convert using float fallback quantization:
    #################################################################
    tflite_2, score_dict = tflite_float_fallback(logger, model, model_name, score_dict, x_train)

    # Convert using integer-only quantization:
    ##################################################################
    tflite_3, score_dict = tflite_int_only(logger, model, model_name, score_dict, x_train)
    return [tflite_0, tflite_1, tflite_2, tflite_3], score_dict


def representative_data_gen(x_train):
    for input_value in tf.data.Dataset.from_tensor_slices(x_train).batch(1).take(100):
        # Model has only one input so each data point has one element.
        yield [input_value]


def tflite_simple(logger, model, model_name, score_dict):
    """ Conversion of a simple TFLite model """
    start = time.time()
    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    converter.experimental_enable_resource_variables = True
    tflite_model_simple = converter.convert()
    end = time.time()
    timing = end - start
    logger.info('Converting time simple model: %0.6fs' % timing)
    file_name = 'models/' + model_name + '/' + model_name + '_tflite0.tflite'
    with open(file_name, 'wb') as f:
        f.write(tflite_model_simple)
    score_dict['cvt(s)_tflite0'] = round(timing, 3)
    score_dict['size(kB)_tflite0'] = os.path.getsize(file_name) / 1000
    return tflite_model_simple, score_dict


def tflite_dynamic_range(logger, model, model_name, score_dict):
    """ Conversion of a TFLite model with full dynamic range quantization """
    start = time.time()
    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    converter.experimental_enable_resource_variables = True
    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    tflite_model_dynamic_range = converter.convert()
    end = time.time()
    timing = end - start
    logger.info('Converting time dr model: %0.6fs' % timing)
    file_name = 'models/' + model_name + '/' + model_name + '_tflite1.tflite'
    with open(file_name, 'wb') as f:
        f.write(tflite_model_dynamic_range)
    score_dict['cvt(s)_tflite1'] = round(timing, 3)
    score_dict['size(kB)_tflite1'] = os.path.getsize(file_name) / 1000
    return tflite_model_dynamic_range, score_dict


def tflite_float_fallback(logger, model, model_name, score_dict, x_train):
    """ Conversion of a TFLite model using float fallback quantization """
    def representative_data_gen():
        for input_value in tf.data.Dataset.from_tensor_slices(x_train).batch(1).take(100):
            # Model has only one input so each data point has one element.
            yield [input_value]

    start = time.time()
    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    converter.experimental_enable_resource_variables = True
    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    converter.representative_dataset = representative_data_gen
    tflite_model_quant_float = converter.convert()
    end = time.time()
    timing = end - start
    logger.info('Converting time float model: %0.6fs' % timing)
    file_name = 'models/' + model_name + '/' + model_name + '_tflite2.tflite'
    with open(file_name, 'wb') as f:
        f.write(tflite_model_quant_float)
    score_dict['cvt(s)_tflite2'] = round(timing, 3)
    score_dict['size(kB)_tflite2'] = os.path.getsize(file_name) / 1000
    return tflite_model_quant_float, score_dict


def tflite_int_only(logger, model, model_name, score_dict, x_train):
    """ Conversion of a TFLite model using using integer-only quantization """
    def representative_data_gen():
        for input_value in tf.data.Dataset.from_tensor_slices(x_train).batch(1).take(100):
            # Model has only one input so each data point has one element.
            yield [input_value]

    start = time.time()
    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    converter.experimental_enable_resource_variables = True
    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    converter.representative_dataset = representative_data_gen
    # Ensure that if any ops can't be quantized, the converter throws an error
    converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
    # Set the input and output tensors to uint8 (APIs added in r2.3)
    converter.inference_input_type = tf.uint8
    converter.inference_output_type = tf.uint8

    tflite_model_quant_int = converter.convert()
    end = time.time()
    timing = end - start
    logger.info('Converting time int model: %0.6fs' % timing)
    file_name = 'models/' + model_name + '/' + model_name + '_tflite3.tflite'
    with open(file_name, 'wb') as f:
        f.write(tflite_model_quant_int)
    score_dict['cvt(s)_tflite3'] = round(timing, 3)
    score_dict['size(kB)_tflite3'] = os.path.getsize(file_name) / 1000
    return tflite_model_quant_int, score_dict
