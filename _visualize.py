import numpy as np
import random
import tensorflow as tf

import keras.backend as K
from keras.applications import imagenet_utils
from keras.layers import *
from keras.models import Model
from keras import initializers

import proplot as pplt
import logging
logging.getLogger('proplot').setLevel(logging.WARNING)
import os
import csv


def visualize_responses(self, img, e=None):
    img = np.expand_dims(img, axis=0)
    fig_responses = pplt.figure(figsize=(2 * self.cols, 2 * self.rows), spanx=False, sharey=False)
    self._fill_array()
    axs = fig_responses.subplots(self.array)
    axs.format(
        abc='A.', abcloc='ul',
        suptitle=self.model.name + " " + str(e),
    )
    i = 0

    for l, dlayer in enumerate(self.model.layers):
        layer = dlayer
        try:
            true_layer = layer.layer if self.quantize else dlayer
        except:
            true_layer = layer
        try:
            tester = Model(self.model.inputs, outputs=layer.output)
            output = tester.predict(img)
        except:
            continue
        l_name = self._get_layer_type(layer.name)
        if l_name == 'input':
            output_img = np.squeeze(output)
            im = axs[i].matshow(output_img, cmap='gray')
            axs[i].set_title(str(l) + ': ' + layer.name)
            axs[i].colorbar(im, loc='r')
            i += 1
        if l_name == 'conv':
            output_img = np.squeeze(output)
            output_img = np.transpose(output_img, (2, 0, 1))

            filters = layer.get_weights()[0]
            if len(filters[0]) == 1:
                filters = filters[0, :, :, :]
                filters = self._get_filter_first(layer, filters)
            else:
                filters = filters[:, :, 0, :]
                filters = self._get_filter_first(layer, filters)
            num_filters = true_layer.filters

            for j in range(num_filters):
                if len(filters[j]) == 1:
                    im = axs[i].matshow(filters[j].T, cmap='gray')
                else:
                    im = axs[i].pcolormesh(filters[j], cmap='gray')
                axs[i].set_title(str(l) + ': ' + layer.name + '\nfilter: ' + str(j))
                axs[i].colorbar(im, loc='r')
                i += 1

                im = axs[i].matshow(output_img[j], cmap='gray')
                axs[i].set_title(str(l) + ': ' + layer.name + '\nresponse: ' + str(j))
                axs[i].colorbar(im, loc='r')
                i += 1
        if l_name == 'separable':
            output_img = np.squeeze(output)
            output_img = np.transpose(output_img, (2, 0, 1))

            depth_filters = layer.get_weights()[0]
            depth_filters = depth_filters[:, :, :, 0]
            depth_filters = depth_filters.transpose(2, 0, 1)

            point_filters = layer.get_weights()[1]
            point_filters = point_filters[0, :, :, :]
            point_filters = point_filters.transpose(1, 0, 2)

            for j in range(depth_filters.shape[0]):
                im = axs[i].pcolormesh(depth_filters[j], cmap='gray')
                axs[i].set_title(str(l) + ': ' + layer.name + '\ndepth filter: ' + str(j))
                axs[i].colorbar(im, loc='r')
                i += 1

                im = axs[i].matshow(point_filters[j].T, cmap='gray')
                axs[i].set_title(str(l) + ': ' + layer.name + '\npoint filter: ' + str(j))
                axs[i].colorbar(im, loc='r')
                i += 1
                if j < true_layer.filters:
                    im = axs[i].matshow(output_img[j], cmap='gray')
                    axs[i].set_title(str(l) + ': ' + layer.name + '\nresponse: ' + str(j))
                    axs[i].colorbar(im, loc='r')
                    i += 1
        if l_name == 'bn' or l_name == 'act' or l_name == 'add':
            output_img = np.squeeze(output)
            output_img = np.transpose(output_img, (2, 0, 1))
            for j in range(layer.output_shape[-1]):
                im = axs[i].matshow(output_img[j], cmap='gray')
                axs[i].set_title(str(l) + ': ' + layer.name + '\nresponse: ' + str(j))
                axs[i].colorbar(im, loc='r')
                i += 1
        if l_name == 'dense':
            im = axs[i].matshow(np.array([output]).T, cmap='gray')
            axs[i].set_title(str(l) + ': ' + layer.name)
            axs[i].colorbar(im, loc='r')
    fig_responses.save(self.img_r + self.model.name + "_r_" + str(e) + '.png')
    pplt.close(fig_responses)


def visualize_weights(self, e=0):
    labels = []
    vi = 1
    hs = []
    weight_log = []
    for l, layer in enumerate(self.model.trainable_weights):
        type = self._get_layer_type(layer.name.split("/")[0])
        part = layer.name.split("/")[-1].split(":")[0]
        values = layer.numpy().reshape(-1)
        label = type + '/' + part

        value_indexes = np.arange(vi, vi + len(values), 1)
        vi += len(values)
        colour = min(e, 9)

        if type in self.conv_types:
            labels.append(label)
            weight_log.append([e, layer.name, *values])
            if part == 'depthwise_kernel':
                h = self.wax[0].scatter(values.min(), values.max(), label=label, c='pink' + str(colour))
                self.wax[1].errorbar(
                    l,
                    np.mean(values),
                    np.std(values), label=label, c='pink' + str(colour))
                self.wax[2].scatter(value_indexes, values, label=label, c='pink' + str(colour))
            if part == 'pointwise_kernel':
                h = self.wax[0].scatter(values.min(), values.max(), label=label, c='orange' + str(colour))
                self.wax[1].errorbar(
                    l,
                    np.mean(values),
                    np.std(values), label=label, c='orange' + str(colour))
                self.wax[2].scatter(value_indexes, values, label=label, c='orange' + str(colour))
            if part == 'kernel':
                h = self.wax[0].scatter(values.min(), values.max(), label=label, c='blue'+str(colour))
                self.wax[1].errorbar(
                    l,
                    np.mean(values),
                    np.std(values), label=label, c='blue' + str(colour))
                self.wax[2].scatter(value_indexes, values, label=label, c='blue' + str(colour))
            if part == 'bias':
                h = self.wax[0].scatter(values.min(), values.max(), label=label, c='cyan'+str(colour))
                self.wax[1].errorbar(
                    l,
                    np.mean(values),
                    np.std(values), label=label, c='cyan' + str(colour))
                self.wax[2].scatter(value_indexes, values, label=label, c='cyan' + str(colour))
        if type == 'bn':
            labels.append(label)
            weight_log.append([e, layer.name, *values])
            if part == 'gamma':
                h = self.wax[0].scatter(values.min(), values.max(), label=label, c='red'+str(colour))
                self.wax[1].errorbar(
                    l,
                    np.mean(values),
                    np.std(values), label=label, c='red' + str(colour))
                self.wax[2].scatter(value_indexes, values, label=label, c='red' + str(colour))
            if part == 'beta':
                h = self.wax[0].scatter(values.min(), values.max(), label=label, c='violet'+str(colour))
                self.wax[1].errorbar(
                    l,
                    np.mean(values),
                    np.std(values), label=label, c='violet' + str(colour))
                self.wax[2].scatter(value_indexes, values, label=label, c='violet' + str(colour))
        hs.append(h)
    if len(self.fig_weights.axes) > 3:
        self.wax[2].legend(hs, labels=labels, label='legend label', center=True, frame=True, loc='b', space=-1.6)
    else:
        self.wax[2].legend(hs, labels=labels, label='legend label', center=True, frame=True, loc='b')
    self.fig_weights.save(self.img_w + self.model.name + "_w_" + str(e)+'.png')
    self._update_weight_log(weight_log)


def visualize_weights_preQAT(self):
    labels = []
    vi = 1
    hs = []
    for l, layer in enumerate(self.model.trainable_weights):
        type = self._get_layer_type(layer.name.split("/")[0])
        part = layer.name.split("/")[-1].split(":")[0]
        values = layer.numpy().reshape(-1)
        label = type + '/' + part
        labels.append(label) if label not in labels else None
        value_indexes = np.arange(vi, vi + len(values), 1)
        vi += len(values)

        if type in self.conv_types:
            labels.append(label) if label not in labels else labels
            if part == 'depthwise_kernel':
                h = self.wax[0].scatter(values.min(), values.max(), label=label, c='gray5')
                self.wax[1].errorbar(
                    l,
                    np.mean(values),
                    np.std(values), label=label, c='gray5')
                self.wax[2].scatter(value_indexes, values, label=label, c='gray5')
            if part == 'pointwise_kernel':
                h = self.wax[0].scatter(values.min(), values.max(), label=label, c='gray7')
                self.wax[1].errorbar(
                    l,
                    np.mean(values),
                    np.std(values), label=label, c='gray7')
                self.wax[2].scatter(value_indexes, values, label=label, c='gray7')
            if part == 'kernel':
                h = self.wax[0].scatter(values.min(), values.max(), label=label, c='gray6')
                self.wax[1].errorbar(
                    l,
                    np.mean(values),
                    np.std(values), label=label, c='gray6')
                self.wax[2].scatter(value_indexes, values, label=label, c='gray6')
            if part == 'bias':
                h = self.wax[0].scatter(values.min(), values.max(), label=label, c='gray4')
                self.wax[1].errorbar(
                    l,
                    np.mean(values),
                    np.std(values), label=label, c='gray4')
                self.wax[2].scatter(value_indexes, values, label=label, c='gray4')
        if type == 'bn':
            labels.append(label) if label not in labels else labels
            if part == 'gamma':
                h = self.wax[0].scatter(values.min(), values.max(), label=label, c='dark')
                self.wax[1].errorbar(
                    l,
                    np.mean(values),
                    np.std(values), label=label, c='dark')
                self.wax[2].scatter(value_indexes, values, label=label, c='dark')
            if part == 'beta':
                h = self.wax[0].scatter(values.min(), values.max(), label=label, c='charcoal')
                self.wax[1].errorbar(
                    l,
                    np.mean(values),
                    np.std(values), label=label, c='charcoal')
                self.wax[2].scatter(value_indexes, values, label=label, c='charcoal')
        hs.append(h)
    if len(self.fig_weights.axes) > 3:
        self.wax[2].legend(hs, label='legend label', center=True, frame=True, loc='b', space=-1.6)
    else:
        self.wax[2].legend(hs, label='legend label', center=True, frame=True, loc='b')
    self.fig_weights.save(self.img_w + "PRE_" + self.model.name + '_w.png')


def _init_wax(self):
    self.fig_weights = pplt.figure(spanx=False, sharey=False)

    array = [
        [1, 2],
        [3, 3]
    ]

    self.wax = self.fig_weights.subplots(array)
    self.wax.format(
        suptitle="Weight training distribution"
    )

    self.wax[0].format(
        title='Min-Max weight distribution',
        xlabel='Minimum weight value',
        ylabel='Maximum weight value'
    )
    self.wax[1].format(
        title='Mean and STD weight distribution',
        xlabel='Layer',
        ylabel='Mean weight'
    )
    self.wax[2].format(
        title='Individual weight distribution',
        xlabel='Weights',
        ylabel='Values'
    )