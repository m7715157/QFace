import datetime

import keras.callbacks

from model import MainModel
from keras.layers import *
from keras.models import Model
from qkeras import *
from tensorflow import optimizers
from keras import losses
from keras.regularizers import L2
import tensorflow_model_optimization as tfmot
from keras.utils import data_utils
from keras.applications import imagenet_utils
import callbacks
import os

from keras import backend as K
from keras import regularizers
import math
from _face_eval import perform_val, get_val_data
import time

from keras.applications.mobilenet_v2 import MobileNetV2

import logging
logging.getLogger('proplot').setLevel(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
BASE_WEIGHT_PATH = ('https://storage.googleapis.com/tensorflow/'
                    'keras-applications/mobilenet_v2/')


class MobileNet(MainModel):
    def __init__(self, name=False, shape=(224, 224, 3), classes=1000, lr=0.1):
        super().__init__()

        # Convolutional stuff
        self.activation_type = 'relu6'
        self.inputs = Input(shape, name='input_0')
        self.num_classes = classes
        self.emb_shape = 128
        self.batch_size = 512
        self.model_len = 10
        self.score_cont = [0, 0, 0]
        self.score_qkeras = [0, 0, 0]
        self.backwards = False
        self.bias = False
        self.folded = False

        # Model init stuff
        self.name = 'MobileNetV2' if not name else name
        self.head = 'ARC'
        self.name_ref = self.name
        self.quantize_layer = 0

        self.basemodel = None
        self.model = None
        self.get_mbmodel()

        self.load_ckpt = False
        self.individual_q = False

        self.loss_fn = losses.SparseCategoricalCrossentropy()
        self.lr = lr
        #
        self.optim_fn = optimizers.SGD(learning_rate=self.lr, momentum=0.9, nesterov=True)
        self.summary_writer_path = f'./logs/QMobileNet{self.now}'
        self.summary_writer = tf.summary.create_file_writer(self.summary_writer_path)
        self.summary_writer.set_as_default()
        #
        self.pre_train_checkpoint = None
        self.train_dataset = None
        self.test_dataset_path = '../dataset/'
        self.df_model = 'MobileNet_df_scores_8bit.csv'

        self.init_paths()
        self.init_log()

    from _layers import cba, depthwise_conv, separable, residual_model, residual, act
    from _qlayers import Qcba, Qdepthwise_conv, Qseparable, Qresidual_model, Qresidual, Qact
    from _visualize import visualize_weights, visualize_responses
    from callbacks import FaceEvalCallback

    def __getattr__(self, name):
        return getattr(self.model, name)

    def _depth(self, v, divisor=8, min_value=None):
        if min_value is None:
            min_value = divisor
        new_v = max(min_value, int(v + divisor / 2) // divisor * divisor)
        # Make sure that round down does not go down by more than 10%.
        if new_v < 0.9 * v:
            new_v += divisor
        return new_v

    def load_weights(self, layer=0):
        if layer == 0:
            model_name = ('mobilenet_v2_weights_tf_dim_ordering_tf_kernels_' +
                          str(float(1.0)) + '_' + str(224) + '.h5')
            weight_path = BASE_WEIGHT_PATH + model_name
            weights_path = data_utils.get_file(
                model_name, weight_path, cache_subdir='models')
            self.model.load_weights(weights_path)
        else:
            if self.load_ckpt:
                path = f'{self.mdir}{self.quantize_checkpoint}/{self.head}/Q{self.name_ref}_{layer}/'
                self.load_ckpt = False
            else:
                path = f'{self.mdir}{self.name_ref}{self.now}/{self.head}/Q{self.name_ref}_{layer}/'
            print(path)
            self.log.info(f"[*] looking for cpkt in {path}")
            ckpt = tf.train.latest_checkpoint(path)
            self.log.info(f"[*] load ckpt from {ckpt}")
            self.model.load_weights(ckpt)

    def get_mbmodel(self, quantize_layer=0):
        self.loss_fn = 'categorical_crossentropy'
        self.quantize_layer = quantize_layer
        self.model = self.basemodel = self.mobilenet()
        self.model.summary()

    def mobilenet(self):
        self.model_len = 10
        self.block_id = 0
        x = _inputs = self.inputs

        # 1: CBA layer
        x = self.mb1(x)

        # 2: Stack 1: [1, 16, 1, 1]
        x = self.mbstack(1, x)

        # 3: Stack 2: [6, 24, 2, 2]
        x = self.mbstack(2, x)

        # 4: Stack 3: [6, 32, 3, 2]
        x = self.mbstack(3, x)

        # 5: Stack 4: [6, 64, 4, 2]
        x = self.mbstack(4, x)

        # 6: Stack 5: [6, 96, 3, 1]
        x = self.mbstack(5, x)

        # 7: Stack 6: [6, 160, 3, 2]
        x = self.mbstack(6, x)

        # 8: Stack 7: [6, 320, 1, 1]
        x = self.mbstack(7, x)

        # 9: Pointwise Convolutional layer
        x = self.mb2(x)

        # 10: Classification layer
        out = self.mb3(x)
        # out = Flatten()(x)

        return Model(_inputs, out, name=self.name)

    def mb1(self, x):
        # 1: CBA layer
        layer_nr = 1 if not self.backwards else 10

        if self.quantize_layer == layer_nr:
            self.layer_type = 'conv-batch-act'
            self.layer_name = f'layer {layer_nr}'
            self.name = f'{self.name_ref}_{layer_nr}'
            return self.Qcba(depth=32, kernel=3, stride=2, name='QConv1')(x)
        elif self.quantize_layer > layer_nr and not self.individual_q:
            return self.Qcba(depth=32, kernel=3, stride=2, name='QConv1')(x)
        else:
            return self.cba(depth=32, kernel=3, stride=2, name='Conv1')(x)

    def mbstack(self, nr, x, alpha=1.0):
        def depth(d):
            return self._depth(d * alpha)
        stack = [
            [1, 16, 1, 1],
            [6, 24, 2, 2],
            [6, 32, 3, 2],
            [6, 64, 4, 2],
            [6, 96, 3, 1],
            [6, 160, 3, 2],
            [6, 320, 1, 1],
        ]

        expansion, ch_out, block_depth, s = stack[nr-1]
        layer_nr = 1 + nr if not self.backwards else 9 - nr

        q = ''
        for i in range(block_depth):
            stride = s if i == 0 else 1
            if self.quantize_layer == layer_nr:
                self.layer_type = 'Inverse Residual Block'
                self.layer_name = f'layer {self.quantize_layer}_{i}'
                q = 'Q'
                self.name = f'{self.name_ref}_{layer_nr}'
                x = self.Qresidual(depth=depth(ch_out), stride=stride, expansion=expansion,
                                        split=True, name=f'Qblock_{self.block_id}_')(x)
            elif self.quantize_layer > layer_nr and not self.individual_q:
                q = 'Q'
                x = self.Qresidual(depth=depth(ch_out), stride=stride, expansion=expansion,
                                        split=True, name=f'Qblock_{self.block_id}_')(x)
            else:
                x = self.residual(depth=depth(ch_out), stride=stride, expansion=expansion,
                                       split=True, name=f'block_{self.block_id}_')(x)
            self.block_id += 1
        # stackblock = Model(_inputs, x, name=f'{q}block_{layer_nr}')
        # x = stackblock(x)
        return x

    def mb2(self, x):
        # 8: Convolutional layer
        layer_nr = 9 if not self.backwards else 2
        if self.quantize_layer == layer_nr:
            self.layer_type = 'Pointwise Convolution'
            self.layer_name = f'layer {layer_nr}'
            self.name = f'{self.name_ref}_{layer_nr}'
            return self.Qcba(depth=1280, kernel=1, stride=1, name=f'QPW_{layer_nr}')(x)
        elif self.quantize_layer > layer_nr and not self.individual_q:
            return self.Qcba(depth=1280, kernel=1, stride=1, name=f'QPW_{layer_nr}')(x)
        else:
            return self.cba(depth=1280, kernel=1, stride=1, name=f'PW_{layer_nr}')(x)

    def mb3(self, x):
        # 10: Pointwise Convolutional layer
        layer_nr = 10 if not self.backwards else 1
        x = GlobalAveragePooling2D()(x)
        imagenet_utils.validate_activation('softmax', 'imagenet')
        if self.quantize_layer == layer_nr:
            self.layer_type = 'Pointwise Convolution'
            self.layer_name = f'layer {layer_nr}'
            self.name = f'{self.name_ref}_{layer_nr}'
            return QDense(self.num_classes, kernel_quantizer=quantized_bits(bits=self.qbit,
                                                                            integer=self.qint,
                                                                            alpha=self.qalpha,
                                                                            symmetric=True,
                                                                            use_stochastic_rounding=False),
                          activation='softmax', name='predictions')(x)
        elif self.quantize_layer > layer_nr and not self.individual_q:
            return QDense(self.num_classes, kernel_quantizer=quantized_bits(bits=self.qbit,
                                                                            integer=self.qint,
                                                                            alpha=self.qalpha,
                                                                            symmetric=True,
                                                                            use_stochastic_rounding=False),
                          activation='softmax', name='predictions')(x)
        else:
            return Dense(self.num_classes, activation='softmax', name='predictions')(x)







