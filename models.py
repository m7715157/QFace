import numpy as np
import random
import tensorflow as tf
# random.seed(2)
# np.random.seed(2)
# tf.random.set_seed(2)
# tf.keras.utils.set_random_seed(2)
# tf.config.experimental.enable_op_determinism()

from keras.layers import *
from keras.models import Model
import tensorflow_model_optimization as tfmot

import logging
logging.getLogger('proplot').setLevel(logging.WARNING)
from model import MainModel


class MNISTModel(MainModel):
    def __init__(self, model_type=None, shape=(28, 28, 1), classes=10, quantize=False):
        super().__init__()

        # MNIST Convolutional stuff
        self.activation_type = 'relu'
        self.inputs = Input(shape, name='input_0')
        self.flatten = Flatten()
        self.dense = Dense(classes, activation='softmax')

        # Model init stuff
        self.quantize = quantize
        self.model = self.mnist_type(model_type)

        if self.quantize:
            self.model_ref = self.name[:-4]
            self.model.load_weights('models/' + self.model_ref + '/' + self.model_ref)
            self.init_log()
            self.init_paths()
            # self.array = np.zeros((self.rows, self.cols))
            # self.visualize_weights_preQAT()
            self.model = tfmot.quantization.keras.quantize_model(self.model)
        else:
            self.model_ref = self.name
            self.init_log()
            self.init_paths()
            # self.array = np.zeros((self.rows, self.cols))

    def __getattr__(self, name):
        return getattr(self.model, name)

    def get_model(self):
        return self.model_def

    def mnist_type(self, model_type):
        if model_type == 'mnist1':
            return self.mnist1()
        elif model_type == 'mnist2':
            return self.mnist2()
        elif model_type == 'mnist0':
            return self.mnist0()
        elif model_type == 'mnist3':
            return self.mnist3()
        elif model_type == 'mnist4':
            return self.mnist4()
        else:
            return print(model_type, "Not Found!")

    def mnist1(self):
        self.name = 'MNISTModel1'
        if self.quantize:
            self.name += '_QAT'
        x = _inputs = self.inputs
        x = self.cba(depth=3, kernel=5, stride=1)(x)
        x = self.flatten(x)
        out = self.dense(x)
        # self.cols += 1
        return Model(_inputs, out, name=self.name)

    def mnist0(self):
        self.name = 'MNISTModel0'
        if self.quantize:
            self.name += '_QAT'
        x = _inputs = self.inputs
        x = self.ca(depth=3, kernel=5, stride=1)(x)
        x = self.flatten(x)
        out = self.dense(x)
        # self.cols += 1
        return Model(_inputs, out, name=self.name)

    def mnist2(self):
        self.name = 'MNISTModel2'
        if self.quantize:
            self.name += '_QAT'
        x = _inputs = self.inputs
        x = self.cba(depth=3, kernel=5, stride=2)(x)
        x = self.flatten(x)
        out = self.dense(x)
        # self.cols += 1
        return Model(_inputs, out, name=self.name)

    def mnist3(self):
        self.name = 'MNISTModel3'
        if self.quantize:
            self.name += '_QAT'
        x = _inputs = self.inputs
        x = self.cba(depth=3, kernel=5, stride=2)(x)
        x = self.depthwise_conv(depth=3, kernel=3, stride=1)(x)
        x = self.flatten(x)
        out = self.dense(x)
        # self.cols += 1
        return Model(_inputs, out, name=self.name)

    def mnist4(self):
        self.name = 'MNISTModel4'
        if self.quantize:
            self.name += '_QAT'
        x = _inputs = self.inputs
        x = self.cba(depth=3, kernel=5, stride=2)(x)
        x = self.residual(depth=3, stride=1)(x)
        x = self.flatten(x)
        out = self.dense(x)
        # self.cols += 1
        return Model(_inputs, out, name=self.name)