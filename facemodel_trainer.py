import datetime
import keras.callbacks

from model import MainModel
from facemodel import MobileFaceNet
from keras.layers import *
from keras.models import Model
from qkeras import *
from tensorflow import optimizers
from keras import losses
from keras.regularizers import L2
import tensorflow_model_optimization as tfmot
from keras.initializers import HeNormal
from keras.applications import imagenet_utils
from qkeras.utils import model_save_quantized_weights
import callbacks
import os

from keras import backend as K
from keras import regularizers
import math
from _face_eval import perform_val, get_val_data
import time

import logging

logging.getLogger('proplot').setLevel(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class MobileFaceNetTrainer(MobileFaceNet):
    def __init__(self, name=False, dataset='CASIA', batch_size=512, split=False):
        super().__init__(name=name, split=split)
        if dataset == 'ms1m':
            self.shape = (112, 112, 3)
            self.num_classes = 85742
            self.num_samples = 5822653

            self.num_epochs = 16
            self.lr_milestones = [8, 12, 14]
        else:
            self.shape = (112, 112, 3)
            self.num_classes = 10575
            self.num_samples = 435779
            self.num_epochs = 100
            self.lr_milestones = [40, 60, 70]

        self.batch_size = batch_size
        self.steps_per_epoch = self.num_samples // self.batch_size

        self.initial_epoch = 0
        # self.num_epochs = 20
        self.best_loss = 1000
        self.mixed = False

    def lr_scheduler2(self, epoch, lr):
        if epoch in self.lr_milestones:
            self.lr_milestones.pop(0)
            return lr * 0.1
        else:
            return lr

    def lr_scheduler(self, epoch, lr):
        if self.head == 'SOFT':
            if epoch * self.steps_per_epoch >= (60000):
                self.lr = 0.001
                return 0.001
            elif epoch * self.steps_per_epoch >= (40000):
                self.lr = 0.01
                return 0.01
            else:
                return lr
        if self.head == 'ARC':
            if epoch * self.steps_per_epoch >= (80000):
                self.lr = 0.001
                return 0.001
            elif epoch * self.steps_per_epoch >= (40000):
                self.lr = 0.01
                return 0.01
            else:
                return lr

    def qlr_scheduler(self, epoch, lr):
        return lr

    def qlr_scheduler2(self, epoch, lr):
        if (epoch % self.num_epochs) < self.num_epochs / 4:
            self.log.info(f'updating learning rate to: {self.lr}')
            return self.lr
        elif self.num_epochs / 2 <= (epoch % self.num_epochs) < 3 * (self.num_epochs / 2):
            self.log.info(f'updating learning rate to: {self.lr * 100}')
            return self.lr * 100
        else:
            self.log.info(f'updating learning rate to: {self.lr * 10}')
            return self.lr * 10

    def mini_summary_logger(self, epoch):
        tf.summary.scalar('param/learning_rate', self.optim_fn.lr, step=epoch)
        tf.summary.scalar('test/LFW', self.score_qkeras[0], step=epoch)
        tf.summary.scalar('test/AgeDB30', self.score_qkeras[1], step=epoch)
        tf.summary.scalar('test/CFP-FP', self.score_qkeras[2], step=epoch)
        tf.summary.scalar('test_diff/LFW', self.score_qkeras[0] - self.score_cont[0], step=epoch)
        tf.summary.scalar('test_diff/AgeDB30', self.score_qkeras[1] - self.score_cont[1], step=epoch)
        tf.summary.scalar('test_diff/CFP-FP', self.score_qkeras[2] - self.score_cont[2], step=epoch)

    def get_callbacks(self, quantization=False):
        if quantization:
            lr_scheduler = self.qlr_scheduler
            lfw_score = int(self.score_qkeras[0] * 100)
            file_path = f'{self.mdir}{self.name_ref}{self.now}/{self.head}/{self.name}/cp_{self.qbit}_l{self.quantize_layer}.ckpt'

        else:
            lr_scheduler = self.lr_scheduler
            file_path = f'{self.mdir}{self.name_ref}{self.now}/{self.head}/{self.name}/cp.ckpt'

        lr_callback = keras.callbacks.LearningRateScheduler(lr_scheduler)
        checkpoint_callback = keras.callbacks.ModelCheckpoint(
            filepath=file_path,
            save_weights_only=True,
            monitor='accuracy',
            mode='max',
            save_best_only=True,
            save_freq='epoch'
        )
        face_eval_callback = callbacks.FaceEvalCallback(
            self.evaluate_face_model,
            self.log,
            self.mini_summary_logger,
            self.score_cont,
            self.score_qkeras,
            update_freq='epoch'
        )
        tb_callback = keras.callbacks.TensorBoard(log_dir=self.summary_writer_path, update_freq='epoch')
        return [
            lr_callback,
            checkpoint_callback,
            face_eval_callback,
            tb_callback
        ]

    def pretrain_facemodel_mini(self,
                                head='ARC',
                                quantize_layer=0,
                                continue_run=False,
                                load_ckpt=False,
                                max_epochs=False,
                                initial_epoch=False,
                                lr_override=False):
        """
        Function to train the model using the fit-method of Keras
        without quantization
        """
        self.head = head  # Choose the final stage of model
        self.trainmode = 'mini'

        # Override learning rate
        if lr_override:
            self.lr = lr_override
            print('Learning rate starts with ', self.lr)
        else:
            self.lr = 0.1

        # Set the number of epochs to train model
        if max_epochs:
            self.num_epochs = max_epochs
            print('max epochs are going to be ', self.num_epochs)

        # If you want to load a checkpoint that used an ArcFace Header
        if load_ckpt == 'ARC':
            self.get_facemodel(0) # Get model without quantized layers
            path = f'models/{self.name_ref}/{self.pre_train_checkpoint}/ARC/{self.name_ref}/'
            # path = f'{self.mdir}{self.pre_train_checkpoint}/ARC/{self.name}/'
            self.log.info(f"[*] looking for cpkt in {path}")
            ckpt = tf.train.latest_checkpoint(path)
            self.log.info(f"[*] load ckpt from {ckpt}")

            self.model.load_weights(ckpt)
            self.model.save(f'{self.mdir}{self.name_ref}{self.now}/{self.head}/{self.name}/initial_model/')

        # Continue training with ArcFace header with pretrained weights from Softmax
        elif continue_run:
            path = f'{self.mdir}{self.name_ref}{self.now}/SOFT/{self.name}/'
            self.log.info(f"[*] looking for cpkt in {path}")
            ckpt = tf.train.latest_checkpoint(path)
            self.log.info(f"[*] load ckpt from {ckpt}")
            self.model.load_weights(ckpt)

        # If you want to load a checkpoint that used an Softmax Header
        elif load_ckpt == 'SOFT':
            self.head = 'SOFT'
            self.get_facemodel(0)
            path = f'{self.mdir}{self.pre_train_checkpoint}/SOFT/{self.name}/'
            self.log.info(f"[*] looking for cpkt in {path}")
            ckpt = tf.train.latest_checkpoint(path)
            self.log.info(f"[*] load ckpt from {ckpt}")
            self.model.load_weights(ckpt)
            self.head = 'ARC'

            self.get_facemodel(quantize_layer, continue_run=True)
            self.model.save(f'{self.mdir}{self.name_ref}{self.now}/{self.head}/{self.name}/initial_model/')
        else:
            self.get_facemodel(quantize_layer, continue_run=continue_run)
            self.model.save(f'{self.mdir}{self.name_ref}{self.now}/{self.head}/{self.name}/initial_model/')

        callbacks = self.get_callbacks()  # Set the callbacks during training
        self.optim_fn = optimizers.SGD(learning_rate=self.lr, momentum=0.9, nesterov=True)
        self.model.compile(optimizer=self.optim_fn, metrics='accuracy', loss=self.loss_fn)
        self.model.fit(self.train_dataset,
                       batch_size=self.batch_size,
                       shuffle=True,
                       steps_per_epoch=self.steps_per_epoch,
                       epochs=self.num_epochs,
                       initial_epoch=initial_epoch,
                       verbose=2,
                       callbacks=callbacks)
        self.evaluate_face_model(self.score_qkeras)  # Evaluate the model

    def train_layer_n(self, layer, trial=0, last=False):
        """
        Function to quantize a single layer and train the whole
        model using the fit-method of Keras without quantization
        """
        self.layer_nr = layer  # Select which layer will be quantized
        self.trainmode = 'mini'

        # Create model
        self.log.info(f'layer: {layer}')
        if self.mixed:  # Different types of quantization levels in model
            self.get_mixmodel()
        else:  # normal operation
            self.get_facemodel(layer)
        self.name = 'Q' + self.name
        self.log.info(f'created: {self.name}')
        load_ckpt = self.load_ckpt

        # Load weights
        if self.individual_q:  # If only one layer is quantized
            self.load_weights(0)
            self.log.info(f'loaded pretrain weights')
        elif last:  # Only quantize the last layer block
            self.load_weights(0)
            self.log.info(f'loaded pretrain weights')
        else:  # Load the last session
            self.load_weights(layer - 1)
            self.log.info(f'loaded weights layer: {layer - 1}')

        # Training setup
        callbacks = self.get_callbacks(quantization=True)
        self.log.info(f'starting learning rate is: {self.lr}')
        # self.optim_fn = optimizers.SGD(learning_rate=self.lr) #, momentum=0.9, nesterov=True)
        self.optim_fn = optimizers.Adam(learning_rate=self.lr)
        self.model.compile(optimizer=self.optim_fn,
                           metrics='accuracy',
                           loss=self.loss_fn)
        max_epochs = self.initial_epoch + self.num_epochs

        # Training:
        self.model.fit(self.train_dataset,
                       batch_size=self.batch_size,
                       shuffle=True,
                       steps_per_epoch=self.steps_per_epoch,
                       epochs=max_epochs,
                       initial_epoch=self.initial_epoch,
                       verbose=2,
                       callbacks=callbacks)
        # self.model_fit(max_epochs)

        self.log.info('Evaluate best model')
        self.load_weights(layer)
        self.evaluate_face_model(self.score_qkeras)
        if np.mean(self.score_qkeras) < 0.6 and trial < 1:  # If the model diverges
            self.lr *= 0.1
            self.train_layer_n(layer, trial + 1)
            self.load_ckpt = load_ckpt
        else:
            self.initial_epoch = max_epochs

    def flatten_model(self):
        """
        Belongs to train_tflite, doesn't work yet
        """
        self.blublub = 456

        def get_layers(layers):

            layers_flat = []
            for layer in layers:
                try:
                    layers_flat.extend(get_layers(layer.layers))
                except AttributeError:
                    if 'nput' in layer.name:
                        layer._name += 'new' + str(self.blublub)
                        self.blublub += 1
                    layers_flat.append(layer)
            return layers_flat

        self.basemodel = tf.keras.models.Sequential(
            get_layers(self.basemodel.layers)
        )

    def train_tflite(self):
        """
        Doesn't work yet
        """
        import tensorflow_model_optimization as tfmot
        self.get_facemodel(0)
        ckpt = tf.train.latest_checkpoint(
            f'models/MobileFaceNet_fold_32bit_pretrain/{self.pre_train_checkpoint}/ARC/MobileFaceNet_fold_32bit_pretrain/')
        self.model.load_weights(ckpt)
        model_save_quantized_weights(self.basemodel, self.mdir + 'QKeras/' + self.name)
        self.basemodel.save(self.mdir + 'QKeras/' + self.name)
        weights = self.basemodel.get_weights()

        self.tflite = True
        self.get_facemodel(0)

        self.basemodel.set_weights(weights)

        self.basemodel = unfold_model(self.basemodel)
        model_save_quantized_weights(self.basemodel, self.mdir + 'QKeras/' + self.name)
        self.basemodel.save(self.mdir + 'QKeras/' + self.name)
        weights = self.basemodel.get_weights()

        self.folded = False
        self.get_facemodel(0)

        pop_list = [1, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 64, 67]
        for p in pop_list:
            weights.pop(p)

        self.basemodel.set_weights(weights)
        self.evaluate_face_model(self.score_qkeras)

        callbacks = self.get_callbacks(quantization=True)
        self.optim_fn = optimizers.SGD(learning_rate=0.001, momentum=0.9, nesterov=True)
        self.model.compile(optimizer=self.optim_fn,
                           metrics='accuracy',
                           loss=self.loss_fn)

        # Training:
        self.model.fit(self.train_dataset,
                       batch_size=self.batch_size,
                       shuffle=True,
                       steps_per_epoch=self.steps_per_epoch,
                       epochs=10,
                       initial_epoch=self.initial_epoch,
                       verbose=2,
                       callbacks=callbacks)

        model_save_quantized_weights(self.basemodel, self.mdir + 'QKeras/' + self.name)
        self.basemodel.save(self.mdir + 'QKeras/' + self.name)
        converter = tf.lite.TFLiteConverter.from_saved_model(self.mdir + 'QKeras/' + self.name)
        converter.experimental_enable_resource_variables = True
        tflite_model = converter.convert()

        interpreter = tf.lite.Interpreter(model_content=tflite_model)
        interpreter.allocate_tensors()  # Needed before execution!

        open(f"models/preQAT_{self.name}.tflite", "wb").write(tflite_model)

        quantize_model = tfmot.quantization.keras.quantize_model
        self.basemodel = quantize_model(self.basemodel)

        callbacks = self.get_callbacks(quantization=True)
        self.model.compile(optimizer=self.optim_fn,
                           metrics='accuracy',
                           loss=self.loss_fn)

        # Training:
        self.model.fit(self.train_dataset,
                       batch_size=self.batch_size,
                       shuffle=True,
                       steps_per_epoch=self.steps_per_epoch,
                       epochs=20,
                       initial_epoch=self.initial_epoch,
                       verbose=2,
                       callbacks=callbacks)
        # self.model_fit(max_epochs)

        self.log.info('Evaluate best model')
        self.evaluate_face_model(self.score_qkeras)

        converter = tf.lite.TFLiteConverter.from_keras_model(self.basemodel)
        converter.optimizations = [tf.lite.Optimize.DEFAULT]

        quantized_tflite_model = converter.convert()
        interpreter = tf.lite.Interpreter(model_content=quantized_tflite_model)
        interpreter.allocate_tensors()  # Needed before execution!

        open(f"models/QAT_{self.name}.tflite", "wb").write(quantized_tflite_model)

    def save_model(self, epoch, step, loss):
        """
        Saves the model
        """
        if loss < self.best_loss:
            self.log.info(f'Saving ... {loss:3f} is smaller than before: {self.best_loss:3f}')
            print(f'Saving ... {loss:3f} is smaller than before: {self.best_loss:3f}')
            self.best_loss = loss
            self.model.save_weights(f'{self.mdir}{self.name_ref}{self.now}/{self.head}/{self.name}/'
                                    f'e_{epoch}_b_{step % self.steps_per_epoch}.ckpt')
            self.log.info(f'Saved weights: {self.mdir}/{self.name_ref}{self.now}/{self.head}/'
                          f'{self.name}/e_{epoch}_b_{step % self.steps_per_epoch}.ckpt')

            self.basemodel.save_weights(f'{self.mdir}{self.name_ref}{self.now}/'
                                        f'{self.name}/e_{epoch}_b_{step % self.steps_per_epoch}.ckpt')
            self.basemodel.save(f'{self.mdir}{self.name_ref}{self.now}/model/{self.name}.h5')

    def model_fit(self, max_epochs):
        """
        Function to train the model using the step for step training using Keras
        without quantization
        """
        epoch = self.initial_epoch
        step = self.initial_epoch * self.steps_per_epoch
        start_time = time.time()
        self.best_loss = 1000
        try:
            self.train_dataset = iter(self.train_dataset)
        except:
            pass
        while epoch < max_epochs:
            inputs, labels = next(self.train_dataset)
            with tf.GradientTape() as tape:
                logist = self.model(inputs, training=True)
                reg_loss = tf.reduce_sum(self.model.losses)
                pred_loss = self.loss_fn(labels, logist)
                total_loss = pred_loss + reg_loss

            grads = tape.gradient(total_loss, self.model.trainable_variables)
            self.optim_fn.apply_gradients(zip(grads, self.model.trainable_variables))

            if step % self.steps_per_epoch == 0:
                elapsed_time = time.time() - start_time
                self.evaluate_face_model(self.score_qkeras)
                self.mini_summary_logger(epoch)
                self.log.info(f'Epoch {epoch}/{max_epochs} - {elapsed_time:.1f}s - reg loss: {reg_loss:3f} '
                              f'+ pred loss: {pred_loss:3f} = total loss: {total_loss:3f}')
                print(f'Epoch {epoch}/{max_epochs} - {elapsed_time:.1f}s - reg loss: {reg_loss:3f} '
                      f'+ pred loss: {pred_loss:3f} = total loss: {total_loss:3f}')
                self.save_model(epoch, step, total_loss)
                start_time = time.time()
                learning_rate = self.qlr_scheduler(epoch, self.optim_fn.lr.numpy())
                self.optim_fn.lr.assign(learning_rate)
                self.log.info(f'Update learning rate to: {learning_rate:5f}')

            step += 1
            epoch = step // self.steps_per_epoch

    def train_equal_qface(self, bit=8, forward=True, num_epochs=20, individual=False, load_ckpt=0):
        """
        Function to train the quantized layer for layer
        """
        self.qbit = bit
        self.head = 'ARC'
        self.quantize = True
        self.name = 'Q' + self.name if self.name[0] != 'Q' else self.name
        self.individual_q = individual
        self.backwards = False if forward else True
        self.num_epochs = num_epochs
        self.load_ckpt = load_ckpt
        self.initial_epoch = load_ckpt * num_epochs

        # Load normal model
        self.get_facemodel(self.load_ckpt)
        self.load_weights(self.load_ckpt)
        self.evaluate_face_model(self.score_cont)
        self.log.info(f"Original scores were: LFW ({self.score_cont[0]}), AgeDB-30 ({self.score_cont[1]})"
                      f" and CFP-FP ({self.score_cont[2]})")
        self.load_ckpt = load_ckpt

        # Define direction
        for layer in range(load_ckpt + 1, self.model_len + 1):
            self.lr = 1e-3
            self.train_layer_n(layer=layer)
            self.log_model_bits()

    def evaluate_full(self, bit=8):
        """
        Function to evaluate the quantized model
        """
        self.qbit = bit
        self.head = 'ARC'
        self.quantize = True
        self.get_facemodel(10)
        self.load_weights(0)
        self.evaluate_face_model(self.score_cont)
        self.log.info(
            f"Post {bit}-Quantization scores were: LFW ({self.score_cont[0]}), AgeDB-30 ({self.score_cont[1]})"
            f" and CFP-FP ({self.score_cont[2]})")

    def all_atonce(self, bit=8, num_epochs=20):
        """
        Function to quantize the whole model and train it
        """
        self.get_facemodel(0)
        self.load_weights(0)
        self.evaluate_face_model(self.score_cont)
        self.log.info(f"Original scores were: LFW ({self.score_cont[0]}), AgeDB-30 ({self.score_cont[1]})"
                      f" and CFP-FP ({self.score_cont[2]})")

        self.num_epochs = num_epochs
        self.qbit = bit
        self.head = 'ARC'
        self.quantize = True
        self.get_facemodel(10)
        self.load_weights(0)
        self.evaluate_face_model(self.score_cont)
        self.log.info(
            f"Post {bit}-Quantization scores were: LFW ({self.score_cont[0]}), AgeDB-30 ({self.score_cont[1]})"
            f" and CFP-FP ({self.score_cont[2]})")
        self.lr = 1e-3
        self.train_layer_n(layer=10, last=True)
        self.log_model_bits()
        self.log.info(
            f"{bit}-Quantize Aware Training scores were: LFW ({self.score_cont[0]}), AgeDB-30 ({self.score_cont[1]})"
            f" and CFP-FP ({self.score_cont[2]})")

    def mixed_precision(self, num_epochs=20):
        """
        Function to train the quantized model with different quantization
        configurations per layer
        """
        self.get_facemodel(0)
        self.load_weights(0)
        self.evaluate_face_model(self.score_cont)
        self.log.info(f"Original scores were: LFW ({self.score_cont[0]}), AgeDB-30 ({self.score_cont[1]})"
                      f" and CFP-FP ({self.score_cont[2]})")

        self.num_epochs = num_epochs
        self.head = 'ARC'
        self.mixed = True
        self.quantize = True
        self.get_mixmodel()
        self.load_weights(0)
        self.evaluate_face_model(self.score_cont)
        self.log.info(
            f"Post Mixed precision Quantization scores were: LFW ({self.score_cont[0]}), AgeDB-30 ({self.score_cont[1]})"
            f" and CFP-FP ({self.score_cont[2]})")
        self.lr = 1e-3
        self.train_layer_n(layer=10, last=True)
        self.log_model_bits()
        self.log.info(
            f"Mixed precision Quantize Aware Training scores were: LFW ({self.score_cont[0]}), AgeDB-30 ({self.score_cont[1]})"
            f" and CFP-FP ({self.score_cont[2]})")

    def train_last_qlayer(self, num_epochs=20):
        """
        Function to train the quantized last layer
        """
        self.backwards = True
        self.head = 'ARC'
        self.quantize = True
        self.num_epochs = num_epochs

        self.get_facemodel(0)
        self.load_weights(0)
        self.evaluate_face_model(self.score_cont)
        self.log.info(f"Original scores were: LFW ({self.score_cont[0]}), AgeDB-30 ({self.score_cont[1]})"
                      f" and CFP-FP ({self.score_cont[2]})")
        for bit in [2, 4, 8]:
            self.qbit = bit
            self.lr = 1e-3
            print(f'Converting to {self.qbit} bits')

            self.name = 'Q' + self.name + f'_q{self.qbit}' if self.name[0] != 'Q' else self.name + f'_q{self.qbit}'
            self.train_layer_n(layer=3, last=True)
            self.log_model_bits()
