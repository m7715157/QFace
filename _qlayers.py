import keras.backend as K
from keras.applications import imagenet_utils
from keras.layers import *
from qkeras import *
from keras.models import Model


def Qact(self, name):
    if self.activation_type == 'relu':
        return QActivation(quantized_relu(bits=self.qbit, integer=3), name=f'{name}_Qrelu-{self.qbit}b')
    elif self.activation_type == 'relu6':
        return QActivation(quantized_relu(bits=self.qbit, integer=3), name=f'{name}_Qrelu6-{self.qbit}b')


def act(self, name):
    if self.activation_type == 'relu':
        return ReLU(name=f'{name}_relu')
    elif self.activation_type == 'relu6':
        return ReLU(6., name=f'{name}_relu6')


def Qcba(self, depth, kernel=3, stride=1, act=True, name=None):
    self.layer_counter += 1
    name = '' if name is None else f'{name}_'

    def cba_block(x):
        if self.folded:
            x = QConv2DBatchnorm(filters=depth,
                                 kernel_size=kernel,
                                 strides=stride,
                                 padding='same',
                                 use_bias=self.bias,
                                 kernel_initializer=self.initializer,
                                 kernel_quantizer=quantized_bits(bits=self.qbit, integer=self.qint, alpha=self.qalpha, symmetric=True, use_stochastic_rounding=False),
                                 name=f'{name}Qconv{self.layer_counter}-{self.qbit}b'
                                 )(x)
        else:
            x = QConv2D(filters=depth,
                        kernel_size=kernel,
                        strides=stride,
                        padding='same',
                        use_bias=self.bias,
                        kernel_initializer=self.initializer,
                        kernel_quantizer=quantized_bits(
                            bits=self.qbit,
                            integer=self.qint,
                            alpha=self.qalpha,
                            symmetric=True,
                            use_stochastic_rounding=False),
                        name=name
                        )(x)
            x = QBatchNormalization(
                axis=-1,
                momentum=0.999,
                epsilon=1e-3,
                name=f'{name}_QBN'
            )(x)
        if act:
            x = self.Qact(name=name)(x)
        return x
    return cba_block


def Qca(self, depth, kernel=3, stride=1, name=None):
    self.layer_counter += 1
    name = '' if name is None else f'{name}_'

    def ca_block(x):
        x = QConv2D(depth,
                    kernel_size=kernel,
                    strides=stride,
                    padding='same',
                    use_bias=self.bias,
                    kernel_initializer=self.initializer,
                    kernel_quantizer=quantized_bits(bits=self.qbit, integer=self.qint, alpha=self.qalpha, symmetric=True, use_stochastic_rounding=False),
                    name=f'{name}Qconv{self.layer_counter}-{self.qbit}')(x)
        x = self.Qact(name=f'{name}Qact{self.layer_counter}-{self.qbit}')(x)
        return x
    return ca_block


def Qdepthwise_conv(self, kernel=3, stride=1, padding='same', act=True, name=None):
    self.layer_counter += 1
    name = '' if name is None else f'{name}_'

    def depthwise_block(x):
        # if self.folded and kernel == 3:
        #     x = QDepthwiseConv2DBatchnorm(kernel_size=kernel,
        #                                   strides=stride,
        #                                   padding=padding,
        #                                   use_bias=self.bias,
        #                                   depthwise_initializer=self.initializer,
        #                                   depthwise_quantizer=quantized_bits(
        #                                       bits=self.qbit,
        #                                       integer=self.qint,
        #                                       alpha=self.qalpha,
        #                                       symmetric=True,
        #                                       use_stochastic_rounding=False),
        #                                   name=f'{name}_folded')(x)
        # else:
        x = QDepthwiseConv2D(kernel_size=kernel,
                             strides=stride,
                             padding=padding,
                             use_bias=self.bias,
                             depthwise_initializer=self.initializer,
                             depthwise_quantizer=quantized_bits(
                                 bits=self.qbit,
                                 integer=self.qint,
                                 alpha=self.qalpha,
                                 symmetric=True,
                                 use_stochastic_rounding=False),
                             name=name+f'-{self.qbit}b')(x)
        # x = QBatchNormalization(
        #     axis=-1,
        #     epsilon=1e-3,
        #     momentum=0.999,
        #     name=f'{name}_QBN')(x)
        if act:
            x = self.Qact(name=name)(x)
        return x
    return depthwise_block


def Qresidual(self, depth, kernel=3, stride=1, expansion=1, split=True, name=None):
    self.layer_counter += 1

    def res(x):
        ch_in = K.int_shape(x)[-1]
        hidden_dim = int(round(ch_in * expansion))
        shortcut = x

        if expansion != 1:
            x = self.Qcba(hidden_dim, kernel=1, name=f'{name}expand')(x)
        if stride == 2:
            x = ZeroPadding2D(
                padding=imagenet_utils.correct_pad(x, kernel), name=name + 'pad'
            )(x)
        x = self.Qseparable(depth, kernel, stride, split=split,
                            name=name if expansion != 1 else 'expanded_conv_')(x)
        if stride == 1 and ch_in == depth:
            x = Add(name=f'{name}add')([shortcut, x])
        return x
    return res


def Qresidual_model(self, shape, depth, kernel=3, stride=1, expansion=1, split=False, name='res_model'):
    self.layer_counter += 1

    ch_in = shape[-1]
    x = _inputs = Input(shape[1:], name='input')
    hidden_dim = int(round(ch_in * expansion))
    shortcut = x

    if expansion != 1:
        x = self.Qcba(hidden_dim, kernel=1, name=f'{name}expand')(x)
    if stride == 2:
        x = ZeroPadding2D(
            padding=imagenet_utils.correct_pad(x, kernel), name=name + 'pad'
        )(x)

    x = self.Qseparable(depth, kernel, stride, split=split,
                        name=name if expansion != 1 else 'expanded_conv_')(x)
    if stride == 1 and ch_in == depth:
        x = Add(name=f'{name}add')([shortcut, x])
    return Model(_inputs, x, name=name + f'-{self.qbit}b')


def Qseparable(self, depth, kernel=3, stride=1, split=True, name=None):
    self.layer_counter += 1

    def sep(x):
        if split:
            x = QDepthwiseConv2D(
                kernel_size=3,
                strides=stride,
                activation=None,
                use_bias=False,
                padding='same' if stride == 1 else 'valid',
                depthwise_quantizer=quantized_bits(
                    bits=self.qbit,
                    integer=self.qint,
                    alpha=self.qalpha,
                    symmetric=True,
                    use_stochastic_rounding=False),
                name=name + 'Qdepthwise')(x)
            x = QBatchNormalization(
                axis=-1,
                epsilon=1e-3,
                momentum=0.999,
                name=name + 'Qdepthwise_BN')(x)
            x = self.Qact(name=f'{name}Qdepthwise')(x)

            x = QConv2D(depth,
                        kernel_size=1,
                        activation=None,
                        use_bias=False,
                        padding='same' if stride == 1 else 'valid',
                        kernel_quantizer=quantized_bits(
                            bits=self.qbit,
                            integer=self.qint,
                            alpha=self.qalpha,
                            symmetric=True,
                            use_stochastic_rounding=False),
                        name=name + 'project')(x)
            x = QBatchNormalization(
                axis=-1,
                epsilon=1e-3,
                momentum=0.999,
                name=name + 'project_BN')(x)
        else:
            x = QSeparableConv2D(depth,
                                 kernel_size=kernel,
                                 strides=(stride, stride),
                                 padding='same' if stride == 1 else 'valid',
                                 depthwise_initializer=self.initializer,
                                 depthwise_quantizer=quantized_bits(bits=self.qbit, integer=self.qint, alpha=self.qalpha,
                                                                    symmetric=True, use_stochastic_rounding=False),
                                 pointwise_initializer=self.initializer,
                                 pointwise_quantizer=quantized_bits(bits=self.qbit, integer=self.qint, alpha=self.qalpha,
                                                                    symmetric=True, use_stochastic_rounding=False),
                                 use_bias=self.bias,
                                 name=f'{name}Qseparable{self.layer_counter}')(x)
        return x
    return sep
