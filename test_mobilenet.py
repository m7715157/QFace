import keras.utils.data_utils

from mbmodel_trainer import MobileNetTrainer
from keras.applications.mobilenet_v2 import MobileNetV2
from keras.applications.mobilenet_v2 import preprocess_input, decode_predictions
import tensorflow as tf
import keras.utils as utils
import matplotlib
import matplotlib.pyplot as plt
import logging
import sys, os, time
from pathlib import Path
from glob import glob
import csv
import tqdm
import numpy as np

logging.getLogger('matplotlib').setLevel(logging.WARNING)
logging.getLogger('tensorflow').setLevel(logging.WARNING)
# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
# os.environ['TF_DETERMINISTIC_OPS'] = '1'
# matplotlib.use('Agg')


def preprocess(images, labels):
    return preprocess_input(images), labels


# dataset_path = Path('/deepstore/datasets/dmb/MachineLearning/ImageNet/ILSVRC/Data/CLS-LOC/train/')
# valset_path = Path('/deepstore/datasets/dmb/MachineLearning/ImageNet/ILSVRC/Data/CLS-LOC/val/')
original_model = MobileNetV2()

valset_path = Path('../dataset/val')

batch_size = 32
# train_set = tf.keras.utils.image_dataset_from_directory(dataset_path,
#                                                         labels='inferred',
#                                                         image_size=(224, 224),
#                                                         batch_size=batch_size,
#                                                         shuffle=False,
#                                                         label_mode='categorical')

val_set = tf.keras.utils.image_dataset_from_directory(valset_path,
                                                      labels='inferred',
                                                      image_size=(224, 224),
                                                      batch_size=batch_size,
                                                      shuffle=False,
                                                      label_mode='categorical')

# train_set = train_set.map(preprocess)
val_set = val_set.map(preprocess)

model = MobileNetTrainer(batch_size=batch_size)
# model.qint = 3
model.get_mbmodel(2)

# model.train_dataset = train_set
model.val_set = val_set

model.model.set_weights(original_model.get_weights())
model.compile(optimizer='Adam', loss='categorical_crossentropy', metrics='accuracy')
# model.evaluate(val_set)
# model.train_equal_q(8, num_epochs=10)
# model.evaluate(val_set)
#
val = iter(val_set)
img, label = next(val)
#
# output1 = original_model.predict(img)[0]
# output2 = model.predict(img)[0]
#
plt.close()
for i, (m1, m2) in enumerate(zip(model.model.trainable_variables, original_model.trainable_variables)):
    if m1.numpy != m2.numpy:
        print(f'{i}: ({m1.name}) {m1.shape} --- {m2.shape} ({m2.name})')

for l, (l1, l2) in enumerate(zip(model.model.layers, original_model.layers)):
    t1 = keras.models.Model(model.model.inputs, outputs=l1.output)
    t2 = keras.models.Model(original_model.inputs, outputs=l2.output)

    # if l < 154:
    #     continue

    try:
        o2 = t2.predict(img)[0, :, :, 0]
        o1 = t1.predict(img)[0, :, :, 0]
        print(l1.name, o1.shape, l2.name, o2.shape)
        plt.imshow(np.hstack([o1, o2]))
        plt.title(str(l))
        plt.pause(1)
        plt.clf()
    except:
        o2 = t2.predict(img)[0]
        o1 = t1.predict(img)[0]
        shape = o1.shape
        print(l1.name, o1.shape, l2.name, o2.shape)
        plt.imshow(np.hstack([o1.reshape((-1, 10)), o2.reshape((-1, 10))]))
        plt.title(str(l))
        plt.pause(1)
        plt.clf()


#
# # model.train_equal_q(8, num_epochs=10)
