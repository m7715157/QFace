import datetime

import keras.callbacks

from model import MainModel
from keras.layers import *
from keras.models import Model
from qkeras import *
from tensorflow import optimizers
from keras import losses
from keras.regularizers import L2
import tensorflow_model_optimization as tfmot
from keras.initializers import HeNormal
from keras.applications import imagenet_utils
import callbacks
import os

from keras import backend as K
from keras import regularizers
import math
from _face_eval import perform_val, get_val_data
import time

import logging
logging.getLogger('proplot').setLevel(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class L2Normalization(Layer):
    """This layer normalizes the inputs with l2 normalization."""

    def __init__(self, **kwargs):
        super(L2Normalization, self).__init__(**kwargs)

    @tf.function
    def call(self, inputs):
        inputs = tf.nn.l2_normalize(inputs, axis=1)

        return inputs


class ArcLayer(Layer):
    def __init__(self, units, **kwargs):
        super(ArcLayer, self).__init__(**kwargs)
        self.units = units

    def build(self, input_shape):
        self.kernel = self.add_weight(
            shape=[input_shape[-1], self.units],
            dtype=tf.float32,
            initializer=HeNormal(),
            regularizer=L2(5e-4),
            trainable=True,
            name='kernel'
        )
        self.built = True

    @tf.function
    def call(self, inputs):
        weights = tf.nn.l2_normalize(self.kernel, axis=0)
        return tf.matmul(inputs, weights)


class ArcLoss(losses.Loss):
    def __init__(self, num_classes, margin=0.5, scale=64, mode='eager', name="arcloss"):
        """Build an additive angular margin loss object for Keras model."""
        super().__init__(name=name)
        self.num_classes = num_classes
        self.margin = margin
        self.scale = scale
        self.threshold = tf.math.cos(math.pi - margin)
        self.cos_m = tf.math.cos(margin)
        self.sin_m = tf.math.sin(margin)
        self.mode = mode

        # Safe margin: https://github.com/deepinsight/insightface/issues/108
        self.safe_margin = self.sin_m * margin

    @tf.function
    def call(self, y_true, y_pred):
        # Calculate the cosine value of theta + margin.
        cos_t = y_pred
        sin_t = tf.math.sqrt(1 - tf.math.square(cos_t))
        cos_t_margin = tf.where(cos_t > self.threshold,
                                cos_t * self.cos_m - sin_t * self.sin_m,
                                cos_t - self.safe_margin)

        # The labels here had already been onehot encoded.
        mask = y_true
        cos_t_onehot = cos_t * mask
        cos_t_margin_onehot = cos_t_margin * mask

        # Calculate the final scaled logits.
        logits = (cos_t + cos_t_margin_onehot - cos_t_onehot) * self.scale
        losses = tf.nn.softmax_cross_entropy_with_logits(y_true, logits)
        return losses


class MobileFaceNet(MainModel):
    def __init__(self, name=False, shape=(112, 112, 3), classes=10575, num_samples=435779, lr=0.1, split=False):
        super().__init__()

        # Convolutional stuff
        self.activation_type = 'relu'
        self.inputs = Input(shape, name='input_0')
        self.num_classes = classes
        self.emb_shape = 128
        self.batch_size = 512
        self.steps_per_epoch = num_samples // self.batch_size
        self.model_len = 10
        self.score_cont = [0, 0, 0]
        self.score_qkeras = [0, 0, 0]
        self.backwards = False
        self.split = split
        self.tflite = False

        # Model init stuff
        self.name = 'MobileFaceNet' if not name else name
        self.head = 'ARC'
        self.name_ref = self.name
        self.quantize_layer = 0
        self.basemodel = self.mobilefacenet()
        self.model = self.basemodel
        self.load_ckpt = False
        self.individual_q = False
        self.trainmode = 'eager'

        self.loss_fn = ArcLoss(self.num_classes)
        self.lr = lr
        # self.loss_fn = losses.SparseCategoricalCrossentropy()
        self.optim_fn = optimizers.SGD(learning_rate=self.lr, momentum=0.9, nesterov=True)
        self.summary_writer_path = f'./logs/QMobileFaceNet{self.now}'
        self.summary_writer = tf.summary.create_file_writer(self.summary_writer_path)
        self.summary_writer.set_as_default()
        #
        self.pre_train_checkpoint = None
        self.train_dataset = None
        self.test_dataset_path = '../dataset/'
        self.df_model = 'MobileFaceNet_df_scores_8bit.csv'

        self.init_paths()
        self.init_log()

    from _layers import cba, depthwise_conv, separable, residual_model, act, residual
    from _qlayers import Qcba, Qdepthwise_conv, Qseparable, Qresidual_model, Qact
    from _visualize import visualize_weights, visualize_responses
    from callbacks import FaceEvalCallback

    def __getattr__(self, name):
        return getattr(self.model, name)

    def ArcHead(self, name='ArcHead'):
        """
        ArcFace header layer
        """
        def arc_head(x_in):
            x = input_1 = Input(x_in.shape[1:])
            x = L2Normalization()(x)
            logists = ArcLayer(self.num_classes)(x)
            return Model(input_1, logists, name=name)(x_in)
        return arc_head

    def SoftHead(self, regularizer=None, name='SoftHead'):
        """
        Softmax header layer
        """
        def soft_head(x_in):
            x = input_1 = Input(x_in.shape[1:])
            x = Dense(self.num_classes, kernel_regularizer=regularizer)(x)
            logists = Softmax()(x)
            return Model(input_1, logists, name=name)(x_in)
        return soft_head

    def _depth(self, v, divisor=8, min_value=None):
        # calculate proper depth of network
        if min_value is None:
            min_value = divisor
        new_v = max(min_value, int(v + divisor / 2) // divisor * divisor)
        # Make sure that round down does not go down by more than 10%.
        if new_v < 0.9 * v:
            new_v += divisor
        return new_v

    def load_weights(self, layer=0):
        """
        Function to load the weights in the model
        """
        if layer == 0:
            path = f'./models/{self.name_ref}/{self.pre_train_checkpoint}/{self.head}/{self.name_ref}'
            print(path)
            ckpt = tf.train.latest_checkpoint(path)
            self.log.info(f"[*] load ckpt from {ckpt}")
            self.model.load_weights(ckpt)
        else:
            if self.load_ckpt:
                path = f'{self.mdir}{self.quantize_checkpoint}/{self.head}/Q{self.name_ref}_{layer}/'
                self.load_ckpt = False
            else:
                path = f'{self.mdir}{self.name_ref}{self.now}/{self.head}/Q{self.name_ref}_{layer}/'
            print(path)
            self.log.info(f"[*] looking for cpkt in {path}")
            ckpt = tf.train.latest_checkpoint(path)
            self.log.info(f"[*] load ckpt from {ckpt}")
            self.model.load_weights(ckpt)

    def get_facemodel(self, quantize_layer, continue_run=False):
        """
        Quantizes the proper layers and otherwise uses the normal layers
        """
        if not continue_run:
            self.quantize_layer = quantize_layer
            print('quantize_layer', quantize_layer)
            self.basemodel = self.mobilefacenet()
        x = _inputs = self.inputs
        x = self.basemodel(x)
        self.basemodel.summary()
        if self.head == 'ARC':
            logists = self.ArcHead()(x)
            self.loss_fn = ArcLoss(self.num_classes, mode=self.trainmode)
        else:
            logists = self.SoftHead()(x)
            self.loss_fn = losses.CategoricalCrossentropy()
        self.model = Model(_inputs, logists, name=self.basemodel.name + self.head)
        self.model.summary()

    def get_mixmodel(self):
        self.quantize_layer = 10
        self.basemodel = self.mixmodel()
        x = _inputs = self.inputs
        x = self.basemodel(x)
        self.basemodel.summary()
        logists = self.ArcHead()(x)
        self.loss_fn = ArcLoss(self.num_classes, mode=self.trainmode)
        self.model = Model(_inputs, logists, name=self.basemodel.name + self.head)
        self.model.summary()

    def evaluate_face_model(self, score, model=False):
        if not model:
            model = self.basemodel

        lfw, agedb_30, cfp_fp, lfw_issame, agedb_30_issame, cfp_fp_issame = get_val_data(self.test_dataset_path)

        self.log.info("[*] Perform Evaluation on LFW...")
        acc_lfw, best_th1 = perform_val(embedding_size=self.emb_shape, batch_size=self.batch_size,
                                        model=model, carray=lfw, issame=lfw_issame)
        self.log.info("    acc {:.4f}, th: {:.2f}".format(acc_lfw, best_th1))
        print("    acc {:.4f}, th: {:.2f}".format(acc_lfw, best_th1))
        score[0] = acc_lfw

        self.log.info("[*] Perform Evaluation on AgeDB30...")
        acc_agedb30, best_th2 = perform_val(embedding_size=self.emb_shape, batch_size=self.batch_size,
                                            model=model, carray=agedb_30, issame=agedb_30_issame)
        self.log.info("    acc {:.4f}, th: {:.2f}".format(acc_agedb30, best_th2))
        print("    acc {:.4f}, th: {:.2f}".format(acc_agedb30, best_th2))
        score[1] = acc_agedb30

        self.log.info("[*] Perform Evaluation on CFP-FP...")
        acc_cfp_fp, best_th3 = perform_val(embedding_size=self.emb_shape, batch_size=self.batch_size,
                                           model=model, carray=cfp_fp, issame=cfp_fp_issame)
        self.log.info("    acc {:.4f}, th: {:.2f}".format(acc_cfp_fp, best_th3))
        print("    acc {:.4f}, th: {:.2f}".format(acc_cfp_fp, best_th3))
        score[2] = acc_cfp_fp

    def mobilefacenet(self):
        """
        Generates the network structure of MobileFaceNet
        """
        self.model_len = 10
        x = _inputs = self.inputs

        # 1: CBA layer
        x = self.mb1(x)

        # 2: Depthwise Separable Convolutional layer
        x = self.mb2(x)

        # 3: Stack 1: [2, 64, 5, 2]
        x = self.mbstack(1, x)

        # 4: Stack 2: [4, 128, 1, 2]
        x = self.mbstack(2, x)

        # 5: Stack 3: [2, 128, 6, 1]
        x = self.mbstack(3, x)

        # 6: Stack 4: [4, 128, 1, 2]
        x = self.mbstack(4, x)

        # 7: Stack 5: [2, 128, 2, 1]
        x = self.mbstack(5, x)

        # 8: Pointwise Convolutional layer
        x = self.mb3(x)

        # 9: Global Depthwise Convolution
        x = self.mb4(x)

        # 10: Pointwise Convolution layer
        x = self.mb5(x)
        out = Flatten()(x)

        return Model(_inputs, out, name=self.name)

    def mixmodel(self):
        self.model_len = 10
        x = _inputs = self.inputs

        # 1: CBA layer
        bit = 8
        x = QConv2DBatchnorm(filters=64,
                             kernel_size=3,
                             strides=2,
                             padding='same',
                             use_bias=self.bias,
                             kernel_initializer=self.initializer,
                             kernel_quantizer=quantized_bits(bits=bit, integer=self.qint, alpha=self.qalpha,
                                                             symmetric=True, use_stochastic_rounding=False),
                             name=f'QConv1_{bit}b'
                             )(x)
        x = QActivation(quantized_relu(bits=bit, integer=3), name=f'Conv1_Qrelu-{bit}b')(x)

        # 2: Depthwise Separable Convolutional layer
        x = QDepthwiseConv2D(kernel_size=3,
                             strides=1,
                             padding='same',
                             use_bias=self.bias,
                             depthwise_initializer=self.initializer,
                             depthwise_quantizer=quantized_bits(
                                 bits=bit,
                                 integer=self.qint,
                                 alpha=self.qalpha,
                                 symmetric=True,
                                 use_stochastic_rounding=False),
                             name=f'depthwise2-{bit}b')(x)
        x = QActivation(quantized_relu(bits=bit, integer=3), name=f'depthwise2_Qrelu-{bit}b')(x)

        # self.qbit = 4
        # 3: Stack 1: [2, 64, 5, 2]
        x = self.mbstack_bits(1, x, bits=2)

        # 4: Stack 2: [4, 128, 1, 2]
        x = self.mbstack_bits(2, x, bits=2)

        # 5: Stack 3: [2, 128, 6, 1]
        x = self.mbstack_bits(3, x, bits=2)

        # 6: Stack 4: [4, 128, 1, 2]
        x = self.mbstack_bits(4, x, bits=2)

        # 7: Stack 5: [2, 128, 2, 1]
        x = self.mbstack_bits(5, x, bits=2)

        # 8: Pointwise Convolutional layer
        x = QConv2DBatchnorm(filters=512,
                             kernel_size=1,
                             strides=1,
                             padding='same',
                             use_bias=self.bias,
                             kernel_initializer=self.initializer,
                             kernel_quantizer=quantized_bits(bits=2, integer=self.qint, alpha=self.qalpha,
                                                             symmetric=True, use_stochastic_rounding=False),
                             name=f'Qpointwise1-2b'
                             )(x)
        x = QActivation(quantized_relu(bits=2, integer=3), name=f'Qpointwise_Qrelu-2b')(x)

        # 9: Global Depthwise Convolution
        x = QDepthwiseConv2D(kernel_size=7,
                             strides=1,
                             padding='valid',
                             use_bias=self.bias,
                             depthwise_initializer=self.initializer,
                             depthwise_quantizer=quantized_bits(
                                 bits=2,
                                 integer=self.qint,
                                 alpha=self.qalpha,
                                 symmetric=True,
                                 use_stochastic_rounding=False),
                             name=f'Qgdc-2b')(x)

        # 10: Pointwise Convolution layer
        x = QConv2DBatchnorm(filters=self.emb_shape,
                             kernel_size=1,
                             strides=1,
                             padding='same',
                             use_bias=self.bias,
                             kernel_initializer=self.initializer,
                             kernel_quantizer=quantized_bits(bits=2, integer=self.qint, alpha=self.qalpha,
                                                             symmetric=True, use_stochastic_rounding=False),
                             name=f'Qpointwise2-2b'
                             )(x)

        out = Flatten()(x)

        return Model(_inputs, out, name=self.name)

    def mb1(self, x):
        # 1: CBA layer
        layer_nr = 1 if not self.backwards else 10

        if self.quantize_layer == layer_nr:
            self.layer_type = 'conv-batch-act'
            self.layer_name = f'layer {layer_nr}'
            self.name = f'{self.name_ref}_{layer_nr}'
            return self.Qcba(depth=64, kernel=3, stride=2, name='Conv1')(x)
        elif self.quantize_layer > layer_nr and not self.individual_q:
            return self.Qcba(depth=64, kernel=3, stride=2, name='Conv1')(x)
        else:
            return self.cba(depth=64, kernel=3, stride=2, name='Conv1')(x)

    def mb2(self, x):
        # 2: Depthwise Convolutional layer
        layer_nr = 2 if not self.backwards else 9
        if self.quantize_layer == layer_nr:
            self.layer_type = 'DepthwiseConv2D'
            self.layer_name = f'layer {layer_nr}'
            self.name = f'{self.name_ref}_{layer_nr}'
            return self.Qdepthwise_conv(kernel=3, stride=1, name='depthwise2')(x)
        elif self.quantize_layer > layer_nr and not self.individual_q:
            return self.Qdepthwise_conv(kernel=3, stride=1, name='depthwise2')(x)
        else:
            return self.depthwise_conv(kernel=3, stride=1, name='depthwise2')(x)

    def mbstack(self, nr, x, alpha=1.0):
        def depth(d):
            return self._depth(d * alpha)
        stack = [
                 [2, 64, 5, 2],
                 [4, 128, 1, 2],
                 [2, 128, 6, 1],
                 [4, 128, 1, 2],
                 [2, 128, 2, 1],
                ]

        if self.tflite:
            return self.mbstack_full(nr, x)

        expansion, ch_out, block_depth, s = stack[nr-1]
        layer_nr = 2 + nr if not self.backwards else 9 - nr
        q = ''
        x_temp = _inputs = Input(x.shape[1:], name=f'block_{layer_nr}_input')
        for i in range(block_depth):
            stride = s if i == 0 else 1
            if self.quantize_layer == layer_nr:
                self.layer_type = 'Inverse Residual Block'
                self.layer_name = f'layer {self.quantize_layer}_{i}'
                q = 'Q'
                self.name = f'{self.name_ref}_{layer_nr}'
                x_temp = self.Qresidual_model(shape=x_temp.shape, depth=depth(ch_out), stride=stride, expansion=expansion,
                                              split=self.split, name=f'Qblock_{layer_nr}_{i}')(x_temp)
            elif self.quantize_layer > layer_nr and not self.individual_q:
                q = 'Q'
                x_temp = self.Qresidual_model(shape=x_temp.shape, depth=depth(ch_out), stride=stride, expansion=expansion,
                                              split=self.split, name=f'Qblock_{layer_nr}_{i}')(x_temp)
            else:
                if self.tflite:
                    x_temp = self.residual(shape=x_temp.shape, depth=depth(ch_out), stride=stride,
                                           expansion=expansion,
                                           split=self.split, name=f'block_{layer_nr}_{i}')(x_temp)
                else:
                    x_temp = self.residual_model(shape=x_temp.shape, depth=depth(ch_out), stride=stride,
                                                 expansion=expansion,
                                                 split=self.split, name=f'block_{layer_nr}_{i}')(x_temp)
        stackblock = Model(_inputs, x_temp, name=f'{q}block_{layer_nr}')
        x = stackblock(x)
        return x

    def mbstack_full(self, nr, x, alpha=1.0):
        def depth(d):
            return self._depth(d * alpha)
        stack = [
                 [2, 64, 5, 2],
                 [4, 128, 1, 2],
                 [2, 128, 6, 1],
                 [4, 128, 1, 2],
                 [2, 128, 2, 1],
                ]

        expansion, ch_out, block_depth, s = stack[nr-1]
        layer_nr = 2 + nr if not self.backwards else 9 - nr

        for i in range(block_depth):
            stride = s if i == 0 else 1
            x = self.residual(shape=x.shape, depth=depth(ch_out), stride=stride,
                              expansion=expansion,
                              split=self.split, name=f'block_{layer_nr}_{i}')(x)
        return x

    def mb3(self, x):
        # 8: Pointwise Convolutional layer
        layer_nr = 8 if not self.backwards else 3
        if self.quantize_layer == layer_nr:
            self.layer_type = 'Pointwise Convolution'
            self.layer_name = f'layer {layer_nr}'
            self.name = f'{self.name_ref}_{layer_nr}'
            return self.Qcba(depth=512, kernel=1, stride=1, name=f'Qpointwise_{layer_nr}')(x)
        elif self.quantize_layer > layer_nr and not self.individual_q:
            return self.Qcba(depth=512, kernel=1, stride=1, name=f'Qpointwise_{layer_nr}')(x)
        else:
            return self.cba(depth=512, kernel=1, stride=1, name=f'pointwise_{layer_nr}')(x)

    def mb4(self, x):
        # 9: Global Depthwise Convolution
        layer_nr = 9 if not self.backwards else 2
        if self.quantize_layer == layer_nr:
            self.layer_type = 'Global Depthwise Convolution'
            self.layer_name = f'layer {layer_nr}'
            self.name = f'{self.name_ref}_{layer_nr}'
            return self.Qdepthwise_conv(kernel=7, stride=1, padding='valid', act=False, name='Qgdc')(x)
        elif self.quantize_layer > layer_nr and not self.individual_q:
            return self.Qdepthwise_conv(kernel=7, stride=1, padding='valid', act=False, name='Qgdc')(x)
        else:
            return self.depthwise_conv(kernel=7, stride=1, padding='valid', act=False, name='gdc')(x)

    def mb5(self, x):
        # 10: Pointwise Convolutional layer
        layer_nr = 10 if not self.backwards else 1
        if self.quantize_layer == layer_nr:
            self.layer_type = 'Pointwise Convolution'
            self.layer_name = f'layer {layer_nr}'
            self.name = f'{self.name_ref}_{layer_nr}'
            return self.Qcba(depth=self.emb_shape, kernel=1, stride=1, act=False, name=f'Qpointwise_{layer_nr}')(x)
        elif self.quantize_layer > layer_nr and not self.individual_q:
            return self.Qcba(depth=self.emb_shape, kernel=1, stride=1, act=False, name=f'Qpointwise_{layer_nr}')(x)
        else:
            return self.cba(depth=self.emb_shape, kernel=1, stride=1, act=False, name=f'pointwise_{layer_nr}')(x)

    def mbstack_bits(self, nr, x, alpha=1.0, bits=2):
        def depth(d):
            return self._depth(d * alpha)
        stack = [
                 [2, 64, 5, 2],
                 [4, 128, 1, 2],
                 [2, 128, 6, 1],
                 [4, 128, 1, 2],
                 [2, 128, 2, 1],
                ]

        expansion, ch_out, block_depth, s = stack[nr-1]
        layer_nr = 2 + nr if not self.backwards else 9 - nr
        x_temp = _inputs = Input(x.shape[1:], name=f'block_{layer_nr}_input')
        for i in range(block_depth):
            stride = s if i == 0 else 1
            x_temp = self.Qresidual_model_bits(bits=bits, shape=x_temp.shape, depth=depth(ch_out), stride=stride,
                                               expansion=expansion, name=f'Qblock_{layer_nr}_{i}')(x_temp)

        stackblock = Model(_inputs, x_temp, name=f'Qblock_{nr}-{bits}b')
        x = stackblock(x)
        return x

    def Qresidual_model_bits(self, bits, shape, depth, kernel=3, stride=1, expansion=1, name='res_model'):
        ch_in = shape[-1]
        x = _inputs = Input(shape[1:], name='input')
        hidden_dim = int(round(ch_in * expansion))
        shortcut = x

        if expansion != 1:
            x = QConv2DBatchnorm(filters=hidden_dim,
                                 kernel_size=1,
                                 strides=1,
                                 padding='same',
                                 use_bias=self.bias,
                                 kernel_initializer=self.initializer,
                                 kernel_quantizer=quantized_bits(bits=bits, integer=self.qint, alpha=self.qalpha,
                                                                 symmetric=True, use_stochastic_rounding=False),
                                 name=f'{name}_expand-{bits}b'
                                 )(x)
            x = QActivation(quantized_relu(bits=bits, integer=3), name=f'Conv1_Qrelu-{bits}b')(x)
        if stride == 2:
            x = ZeroPadding2D(
                padding=imagenet_utils.correct_pad(x, kernel), name=name + 'pad'
            )(x)
        name = name if expansion != 1 else 'expanded_conv_'
        x = QSeparableConv2D(depth,
                             kernel_size=kernel,
                             strides=(stride, stride),
                             padding='same' if stride == 1 else 'valid',
                             depthwise_initializer=self.initializer,
                             depthwise_quantizer=quantized_bits(bits=bits, integer=self.qint, alpha=self.qalpha,
                                                                symmetric=True, use_stochastic_rounding=False),
                             pointwise_initializer=self.initializer,
                             pointwise_quantizer=quantized_bits(bits=bits, integer=self.qint, alpha=self.qalpha,
                                                                symmetric=True, use_stochastic_rounding=False),
                             use_bias=self.bias,
                             name=f'{name}_Qseparable-{bits}b')(x)
        if stride == 1 and ch_in == depth:
            x = Add(name=f'{name}add')([shortcut, x])
        return Model(_inputs, x, name=name + f'-{bits}b')





