import datetime
import keras.callbacks
import matplotlib.pyplot as plt

from model import MainModel
from facemodel import MobileFaceNet
from keras.layers import *
from keras.models import Model
from qkeras import *
from tensorflow import optimizers
from keras import losses
from keras.regularizers import L2
import tensorflow_model_optimization as tfmot
from keras.initializers import HeNormal
from keras.applications import imagenet_utils
from qkeras.utils import model_save_quantized_weights
import callbacks
import os
import pickle
import cv2

from keras import backend as K
from keras import regularizers
import math
from _face_eval import perform_val, get_val_data
import time
import pandas as pd
import seaborn as sns
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
import qkeras.bn_folding_utils as bnf

import logging
logging.getLogger('proplot').setLevel(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class MobileFaceNetAnalyser(MobileFaceNet):
    def __init__(self, name=False, dataset='CASIA'):
        if dataset == 'CASIA':
            self.shape = (112, 112, 3)
            self.num_classes = 10575
            self.num_samples = 435779
        super().__init__(name=name)
        self.faces_set = 'faces/'
        plt.close()

    def evaluate_last_qlayer(self, path, bit):
        self.backwards = True
        self.head = 'ARC'
        self.qbit = bit
        self.get_facemodel(1)
        self.log.info(f"[*] looking for ckpt: {path}/ck_{bit}_l1.ckpt")
        self.model.load_weights(f'{path}/cp_{bit}_l1.ckpt')
        self.evaluate_face_model(self.score_qkeras)

    def get_correlation(self, emb_set):
        nr_faces = sum(
            [len([f for f in files if f.endswith('.jpg')]) for r, d, files in os.walk(self.faces_set) if
             len(files) != 0])
        scores = np.zeros((nr_faces, nr_faces))
        for idx, ref in enumerate(emb_set):
            for idy, test in enumerate(emb_set):
                diff = np.subtract(ref, test)
                dist = np.sum(np.square(diff))
                scores[idx, idy] = dist

        plt.matshow(scores)
        plt.colorbar()
        plt.clim(0, 2.5)
        plt.title(f'Similarity Matrix for {self.qbit}-bits')
        plt.savefig(f'q-vector/correlation/similarity_{self.qbit}bit.png')
        plt.close()

    def get_tsne(self, emb_set, emb_labels, n=2):
        if n == 3:
            metrics = TSNE(n_components=3).fit_transform(emb_set)
            df = pd.DataFrame([emb_labels, metrics[:, 0], metrics[:, 1], metrics[:, 2]],
                              index=['label', 'tsne_x', 'tsne_y', 'tsne_z'])
        else:
            metrics = TSNE(n_components=2).fit_transform(emb_set)
            sorting = np.argsort(emb_labels)
            new_labels = np.array([f'id {label}' for label in emb_labels.astype(int)])[sorting]
            tsnex = np.array(metrics[:, 0])[sorting]
            tsney = np.array(metrics[:, 1])[sorting]

            df = pd.DataFrame([new_labels, tsnex, tsney], index=['label', 'tsne_x', 'tsne_y'])
        df = df.transpose()

        plt.close()
        plt.figure()
        if n == 3:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')

            x = df['tsne_x']
            y = df['tsne_y']
            z = df['tsne_z']
            m = df['label']

            ax.set_xlabel("tsne_x")
            ax.set_ylabel("tsne_y")
            ax.set_zlabel("tsne_z")

            ax.scatter(x, y, z, s=3, c=m)
            # plt.show()
        else:
            g = sns.scatterplot(x='tsne_x', y='tsne_y', hue='label', palette="tab10", data=df)
            g.legend(loc='center left', bbox_to_anchor=(1, 0.5), title="ms1m id's")

        # plt.savefig(f'tsne_classification_{self.qbit}bit.png')
        plt.title(f't-SNE for {self.qbit}-bits')
        plt.savefig(f'q-vector/tsne/tsne_{n}d_classification_{self.qbit}bit.png', bbox_inches='tight')
        plt.close()

    def vector_test(self, path):
        # self.backwards = True
        self.head = 'ARC'

        # self.get_facemodel(0)
        ckpt = tf.train.latest_checkpoint(
            'models/MobileFaceNet_fold_32bit_pretrain/MobileFaceNet_fold_2022-05-03T10-59-17/ARC/MobileFaceNet_fold_32bit_pretrain/')
        self.model.load_weights(ckpt)

        self.qbit = 32
        # self.save_embs()
        emb_set, emb_labels = self.load_emb()
        # self.get_correlation(emb_set)
        self.get_correlation(emb_set)
        self.get_tsne(emb_set, emb_labels)

        for bit in [8, 4, 2]:
            self.qbit = bit
            self.get_facemodel(10)
            self.log.info(f"[*] looking for ckpt: {path}/ck_{bit}_l10.ckpt")
            self.model.load_weights(f'{path}/cp_{bit}_l10.ckpt')
            # ckpt = tf.train.latest_checkpoint('models/MobileFaceNet_fold/MobileFaceNet_fold_2022-03-16T12-13-27/ARC/MobileFaceNet_fold/')
            # self.model.load_weights(ckpt)
            # self.basemodel = bnf.unfold_model(self.basemodel)
            # self.evaluate_face_model(self.score_qkeras)
            # model_save_quantized_weights(self.basemodel, self.mdir + 'QKeras/' + self.name)
            self.save_embs()
            emb_set, emb_labels = self.load_emb()

            self.get_correlation(emb_set)
            self.get_tsne(emb_set, emb_labels)
            # self.get_tsne(emb_set, emb_labels, 3)

    def _hflip(self, imgs):
        assert len(imgs.shape) == 4
        return imgs[:, :, ::-1, :]

    def _l2_norm(self, x, axis=1):
        """l2 norm"""
        norm = np.linalg.norm(x, axis=axis)
        output = x / norm
        return output

    def save_embs(self):
        nr_faces = sum(
            [len([f for f in files if f.endswith('.jpg')]) for r, d, files in os.walk(self.faces_set) if len(files) != 0])
        test_set = np.zeros((nr_faces, 128))
        labels = np.zeros((nr_faces, ))
        self.batch_size = 1
        j = 0
        for root, dirs, files in os.walk(self.faces_set):
            for file in files:
                folder = root.split('/')[-1]
                if file.endswith('.jpg'):
                    name = file.split('.')[0]
                    img = cv2.imread(os.path.join(root, file))
                    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                    img = cv2.resize(img, (112, 112))
                    img = img.astype(np.float32) / 255.
                    if len(img.shape) == 3:
                        img = np.expand_dims(img, 0)
                    img = np.array(img, dtype=np.float32)
                    emb = self.basemodel(img).numpy()

                    test_set[j] = self._l2_norm(emb)
                    labels[j] = int(folder)
                    j += 1
        f = open(f'{self.faces_set}/{self.name}_{self.qbit}.pkl', 'wb')
        pickle.dump([test_set, labels], f)
        f.close()
        return None

    def load_emb(self, path=None):
        if path is None:
            file = open(f'{self.faces_set}/{self.name}_{self.qbit}.pkl', 'rb')
        else:
            file = open(path, 'rb')
        embs, labels = pickle.load(file)
        file.close()
        return embs, labels

    def save_tflite(self):
        self.get_facemodel(0)
        ckpt = tf.train.latest_checkpoint(
            f'models/MobileFaceNet_fold_32bit_pretrain/{self.pre_train_checkpoint}/ARC/MobileFaceNet_fold_32bit_pretrain/')
        self.model.load_weights(ckpt)

        model_save_quantized_weights(self.basemodel, self.mdir + 'QKeras/' + self.name)
        self.basemodel.save(self.mdir + 'QKeras/' + self.name)

        converter = tf.lite.TFLiteConverter.from_saved_model(self.mdir + 'QKeras/' + self.name)
        converter.experimental_enable_resource_variables = True
        tflite_model = converter.convert()

        interpreter = tf.lite.Interpreter(model_content=tflite_model)
        interpreter.allocate_tensors()  # Needed before execution!

        open(f"models/{self.name}.tflite", "wb").write(tflite_model)

    def plt_FAR(self, tpr, fpr, bit):
        # FAR01 = np.where(fpr == fpr[np.abs(fpr - 0.001).argmin()])[0][-1]
        FAR1 = np.where(fpr == fpr[np.abs(fpr - 0.01).argmin()])[0][-1]
        # FAR2 = np.where(fpr == fpr[np.abs(fpr - 0.02).argmin()])[0][-1]

        # TAR01 = tpr[FAR01]
        TAR1 = tpr[FAR1]
        # TAR2 = tpr[FAR2]

        # plt.annotate(f'{TAR01:.2f} @ 0.1% FAR for {bit}-bits', (fpr[FAR01], tpr[FAR01]),
        #              textcoords="offset points", xytext=(10, 0), ha='left')
        # plt.plot(fpr[FAR01], tpr[FAR01], 'x')

        if bit == 8:
            plt.annotate(f'{TAR1:.2f} @ 1% FAR for {bit}-bits', (fpr[FAR1], 0.85),
                         textcoords="offset points", xytext=(10, 0), ha='left')
        if bit == 2:
            plt.annotate(f'{TAR1:.2f} @ 1% FAR for {bit}-bits', (fpr[FAR1], 0.80),
                         textcoords="offset points", xytext=(10, 0), ha='left')
        if bit == 4:
            plt.annotate(f'{TAR1:.2f} @ 1% FAR for {bit}-bits', (fpr[FAR1], 0.90),
                         textcoords="offset points", xytext=(10, 0), ha='left')
        if bit == 32:
            plt.annotate(f'{TAR1:.2f} @ 1% FAR for {bit}-bits', (fpr[FAR1], 0.95),
                         textcoords="offset points", xytext=(10, 0), ha='left')

        plt.plot(fpr[FAR1], tpr[FAR1], 'x', color='yellow')

        # plt.annotate(f'{TAR2:.2f} @ 2% FAR for {bit}-bits', (fpr[FAR2], tpr[FAR2]),
        #              textcoords="offset points", xytext=(10, 0), ha='left')
        # plt.plot(fpr[FAR2], tpr[FAR2], 'x')

    def get_roc(self, path):
        lfw, agedb_30, cfp_fp, lfw_issame, agedb_30_issame, cfp_fp_issame = get_val_data(self.test_dataset_path)
        # age_emb = self.get_embeds(agedb_30)
        # self.load_weights(0)
        # self.get_facemodel(0)
        dict = {
            'lfw': {
                32: [],
                8: [],
                4: [],
                2: []
            },
            'agedb_30': {
                32: [],
                8: [],
                4: [],
                2: []
            },
        }

        for dataset in [
            'lfw',
            # 'agedb_30'
        ]:
            if dataset == 'lfw':
                data = lfw
                issame = lfw_issame
            else:
                data = agedb_30
                issame = agedb_30_issame

            ckpt = tf.train.latest_checkpoint(
                'models/MobileFaceNet_fold_32bit_pretrain/MobileFaceNet_fold_2022-05-03T10-59-17/ARC/MobileFaceNet_fold_32bit_pretrain/')
            self.model.load_weights(ckpt)

            bit = 32
            emb = self.get_embeds(data)
            th, tpr, fpr, acc, auc = self.calc_roc(emb, issame)
            print(auc)
            plt.plot(fpr, tpr, label=f'ROC for {bit}-bits (AUC: {auc:0.2f})')
            self.plt_FAR(tpr, fpr, bit)
            dict[dataset][bit] = [th, tpr, fpr, acc]
            np.save('q-vector/roc.npy', dict)
            print(f'filled dictonary with {dataset} of {bit} bits')
            # plt.legend()
            plt.savefig(f'q-vector/roc/roc_{dataset}.png', bbox_inches='tight')

            for bit in [8, 4, 2]:
                self.qbit = bit
                self.get_facemodel(10)
                self.log.info(f"[*] looking for ckpt: {path}/ck_{bit}_l10.ckpt")
                self.model.load_weights(f'{path}/cp_{bit}_l10.ckpt')

                emb = self.get_embeds(data)
                th, tpr, fpr, acc, auc = self.calc_roc(emb, issame)
                print(acc, auc)
                print(fpr, tpr)
                plt.plot(fpr, tpr, label=f'ROC for {bit}-bits (AUC: {auc:0.2f})')
                self.plt_FAR(tpr, fpr, bit)
                dict[dataset][bit] = [th, tpr, fpr, acc]
                np.save('q-vector/roc.npy', dict)
                print(f'filled dictonary with {dataset} of {bit} bits')
                # plt.legend()
                plt.savefig(f'q-vector/roc/roc_{dataset}2.png', bbox_inches='tight')

            plt.xlabel('False Positive Rate')
            plt.ylabel('True Positive Rate')

            if dataset == 'lfw':
                plt.xlim(0, 1)
                plt.ylim(0, 1)
            else:
                plt.xlim(0, 1)
                plt.ylim(0, 1)
            plt.legend()

            # Find 0.1 % FAR


            plt.title(f'ROC-curve for several bit levels {dataset} dataset')
            plt.savefig(f'q-vector/roc/roc_{dataset}.png', bbox_inches='tight')
            plt.close()
            # plt.plot(th, tpr, label='True Positive Rate')
            # plt.plot(th, 1 - fpr, label='True Negative Rate')
            # plt.plot(th, acc, label='Accuracy')
            # plt.xlabel('Thresholds')
            # plt.ylabel('Score')
            # plt.legend()
            # plt.title(f'ROC-curve for {self.qbit}-bits AgeDB-30 dataset')
            # plt.savefig(f'q-vector/roc/roc_{self.qbit}bit_agedb.png', bbox_inches='tight')
            # plt.close()

    def calc_roc(self, embeddings, actual_issame):
        from _face_eval import KFold

        thresholds = np.arange(0, 3, 0.001)
        nrof_folds = 10
        embeddings1 = embeddings[0::2]
        embeddings2 = embeddings[1::2]

        assert (embeddings1.shape[0] == embeddings2.shape[0])
        assert (embeddings1.shape[1] == embeddings2.shape[1])

        nrof_pairs = min(len(actual_issame), embeddings1.shape[0])
        nrof_thresholds = len(thresholds)
        k_fold = KFold(n_splits=nrof_folds, shuffle=False)

        tprs_roc = np.zeros((nrof_folds, nrof_thresholds))
        fprs_roc = np.zeros((nrof_folds, nrof_thresholds))
        acc_roc = np.zeros((nrof_folds, nrof_thresholds))
        auc_roc = np.zeros((nrof_folds, nrof_thresholds))

        tprs = np.zeros((nrof_folds, nrof_thresholds))
        fprs = np.zeros((nrof_folds, nrof_thresholds))
        accuracy = np.zeros((nrof_folds))
        auc = np.zeros((nrof_folds))

        best_thresholds = np.zeros((nrof_folds))
        indices = np.arange(nrof_pairs)
        diff = np.subtract(embeddings1, embeddings2)
        dist = np.sum(np.square(diff), 1)

        for i, (em1, emb2) in enumerate(zip(embeddings1, embeddings2)):
            dot = np.dot(em1, emb2)
            dist[i] = np.arccos(dot)

        # diff = np.subtract(embeddings1, embeddings2)
        # dist = np.sum(np.square(diff), 1)

        for fold_idx, (train_set, test_set) in enumerate(k_fold.split(indices)):
            # Find the best threshold for the fold
            for threshold_idx, threshold in enumerate(thresholds):
                tprs_roc[fold_idx, threshold_idx], \
                fprs_roc[fold_idx, threshold_idx], \
                acc_roc[fold_idx, threshold_idx], \
                auc_roc[fold_idx, threshold_idx] = self.calculate_accuracy(
                    threshold,
                    dist[train_set],
                    actual_issame[train_set])
            best_threshold_index = np.argmax(acc_roc[fold_idx])

            best_thresholds[fold_idx] = thresholds[best_threshold_index]
            _,_,accuracy[fold_idx], auc[fold_idx] = self.calculate_accuracy(
                thresholds[best_threshold_index],
                dist[test_set],
                actual_issame[test_set])
        tpr = np.mean(tprs_roc, 0)
        fpr = np.mean(fprs_roc, 0)
        acc = np.mean(accuracy, 0)
        auc = np.mean(auc, 0)
        # Accuracy = (sensitivity) (prevalence) + (specificity) (1 - prevalence)
        # Accuracy = (TPR) ()
        # bam = (tpr + (1 - fpr))/2
        return thresholds, tpr, fpr, acc, auc

    def calculate_accuracy(self, threshold, dist, actual_issame):
        from sklearn import metrics

        predict_issame = np.less(dist, threshold)
        tp = np.sum(np.logical_and(predict_issame, actual_issame))
        fp = np.sum(np.logical_and(predict_issame, np.logical_not(actual_issame)))
        tn = np.sum(np.logical_and(np.logical_not(predict_issame),
                                   np.logical_not(actual_issame)))
        fn = np.sum(np.logical_and(np.logical_not(predict_issame), actual_issame))

        tpr = 0 if (tp + fn == 0) else float(tp) / float(tp + fn)
        fpr = 0 if (fp + tn == 0) else float(fp) / float(fp + tn)
        acc = float(tp + tn) / dist.size
        auc = metrics.roc_auc_score(actual_issame, predict_issame)
        return tpr, fpr, acc, auc


    def get_embeds(self, carray):
        from _face_eval import hflip_batch, l2_norm
        embeddings = np.zeros([len(carray), self.emb_shape])

        for idx in range(0, len(carray), self.batch_size):
            batch = carray[idx:idx + self.batch_size]
            batch = np.transpose(batch, [0, 2, 3, 1]) * 0.5 + 0.5
            batch = batch[:, :, :, ::-1]  # convert BGR to RGB

            fliped = hflip_batch(batch)
            emb_batch = self.basemodel(batch) + self.basemodel(fliped)
            embeddings[idx:idx + self.batch_size] = l2_norm(emb_batch)
        return embeddings
