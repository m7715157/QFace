# Limited Resource Face Recognition
This repository contains the code that was used to quantize MobileFaceNet using the QKeras library.

## Code structure
The main file for QMobileFaceNet is: `main_face.py`.
This file creates an instance of the MobileFaceNetTraining-class.
This class can be found in `facemodel_trainer.py`.

This class is a trainer wrap around the MobileFaceNet class and allows for configuring and running different kinds of training sessions.
* `model.pretrain_facemodel_mini()` - creates a training session based on the built-in fit method of Keras.
    * `head`: Selecting either the ArcFace or Softmax head
* `train_layer_n()` - Function to quantize a single layer and train the whole
        model using the fit-method of Keras without quantization
* `model_fit()` - Function to train the model using the step for step training using Keras
        without quantization
* `train_equal_qface()` - Function to train the quantized layer for layer
* `evaluate_full()` - Function to evaluate the quantized model
* `all_atonce()` - Function to quantize the whole model and train it
* `mixed_precision()` - Function to train the quantized model with different quantization
        configurations per layer
* `train_last_qlayer()` - Function to train the quantized last layer


The model is described in `facemodel.py`. This class describes the functions that are necessary to build QMobileFaceNet.
* `ArcHead()` - ArcFace header layer
* `SoftHead()` - Softmax header layer
* `load_weights()` - Load weights in model
* `get_facemodel()` - Quantizes the proper layers and otherwise uses the normal layers 
* `mobilefacenet()` - Generates the network structure of MobileFaceNet

In order to quantize MobileFaceNet I had to implement QLayer equivalents which can be found in `_qlayers.py`


The main model class is based on MainModel and is described in `model.py`.
This class houses the general stuff that is needed to train a model such as the logging and path definition.

This code structure is then also implemented for MobileNet (the quantized network did not train properly) and a 
MNIST (Fashion) model to test training parameters.