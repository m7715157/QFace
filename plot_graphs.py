import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
sns.set()
sns.set_context("notebook")
sns.set_context("talk")
 # scores2 = scores
# # scores = pd.read_csv('df_score_bits_auto.csv')
#
# # # df = pd.melt(scores, id_vars=['Configuration', 'Q-bits', 'Stride', 'Dataset'], value_vars=['Accuracy (%)', 'Reference (%)'], value_name='Reference')
# #
# # scores2['_'.join(['Accuracy (%)', 'Reference (%)'])] = \
# #       pd.Series(scores2.reindex(['Accuracy (%)', 'Reference (%)'], axis='columns').astype('str').values.tolist()).str.join('_')
# #
# # # PLOT WITH hue
# # sns.relplot(x='Q-bits', y='Accuracy (%)', hue='_'.join(['Accuracy (%)', 'Reference (%)']), data=scores, aspect=1.5)
# # plt.show()
#
# # ref = scores['Q-bits', 'Reference (%)', 'Stride', 'Configuration']
# # plt.style.use('ggplot')
#
scores = pd.read_csv('df_experiment4.csv', sep=';')
ax = sns.relplot(x='Q-bits',
                 y='Accuracy loss (%p)',
                 data=scores[
                     # ((scores['Layer_type'] == 'convolutional') |
                     #  (scores['Layer_type'] == 'depthwise separable') |
                     #  (scores['Layer_type'] == 'depthwise convolutional')
                     #  ) &
                     (scores['Folded'] == True) &
                     (scores['Split'] == False) &
                     (scores['Integer'] == 0)
                 ],
                 col='Layer_type',
                 col_wrap=3,
                 col_order=[
                     'convolutional',
                     'depthwise separable',
                     'inverted residual(depthwise separable)',
                     'depthwise convolutional',
                     'global depthwise convolutional',
                     'linear pointwise convolutional',
                 ],
                 # row='Folded',
                 # hue_order=[
                 #     'conv-batch-act',
                 #     'cba-depthwise_conv-batch-act',
                 #     'cba-separable',
                 #     'cba-res(sep)',
                 #     'cba-depthwise_conv-batch',
                 #     'cba-pw-batch',
                 #     'fold(conv-batch)-act',
                 #     'cbfa-fold(depthwise_conv)-act',
                 #     'cbfa-separable',
                 #     'cbfa-res(sep)',
                 #     'cbfa-fold(depthwise_conv)',
                 #     'cbfa-folded(pw)'
                 # ],
                 # style='Folded',
                 hue='QAlpha',
                 style='Dataset',
                 markers=True,
                 kind='line',
                 # showfliers=False,
                 facet_kws={'sharey': False, 'sharex': False, 'margin_titles': True},
                 # height=3, aspect=2
                 height=3, aspect=1.5
                 )
for f in ax.axes.ravel():
    f.invert_xaxis()
    f.set_ylim(-2, 10)
    # f.set(yscale="log")

ax.axes.ravel()[3].set_ylim(-20, 20)
ax.axes.ravel()[4].set_ylim(-20, 20)
ax.axes.ravel()[5].set_ylim(-20, 20)
ax.axes.ravel()[4].set_title('linear global depthwise convolutional')
ax.axes.ravel()[5].set_ylim(-20, 20)
ax.fig.subplots_adjust(top=0.9)
ax.fig.suptitle('Comparing different convolution layers')
ax.set_titles(row_template='Folded: {row_name}', col_template='{col_name}',fontweight="bold")


scores = pd.read_csv('df_experiment2.csv')
# plt.figure()
fig, axs = plt.subplots(1, 2)

for b, blub in enumerate([
    # 'convolutional',
    # 'depthwise convolutional',
    # 'depthwise separable',
    'inverted residual(depthwise separable)',
    # 'global depthwise convolutional',
    # 'linear pointwise convolutional'
    ]):
    data = scores[(scores['Layer_type'] == blub) &
                  (scores['Seed'] == 2) &
                  (scores['QAlpha'] == '1') &
                  (scores['Folded'] == True) &
                  (scores['Dataset'] == 'MNIST')]
    data2 = scores[(scores['Layer_type'] == blub) &
                  (scores['Seed'] == 2) &
                  (scores['QAlpha'] == 'auto') &
                  (scores['Folded'] == True) &
                  (scores['Dataset'] == 'MNIST')]
    axs[0].set_title('Inverted Residual block with no scaling', fontweight="bold")
    sns.heatmap(data.pivot('Q-bits', 'Integer', 'Accuracy loss (%p)'), annot=True, ax=axs[0], vmax=10, vmin=-2,
                center=0, cmap="crest")
    axs[1].set_title('Inverted Residual block with automatic scaling', fontweight="bold")
    sns.heatmap(data2.pivot('Q-bits', 'Integer', 'Accuracy loss (%p)'), annot=True, ax=axs[1], vmax=10, vmin=-2,
                center=0, cmap="crest")



# ax.fig.axes[0].set_ylim(-2, 5)
# ax.fig.axes[0].set_title('QAlpha = default\nconvolutional')
# ax.fig.axes[1].set_ylim(-2, 5)
# ax.fig.axes[1].set_title('QAlpha = default\ndepthwise separable')
# ax.fig.axes[2].set_ylim(-2, 5)
# ax.fig.axes[2].set_title('QAlpha = default\ninverted residual(depthwise separable)')

# ax.fig.axes[3].set_ylim(-2, 10)
# ax.fig.axes[3].set_title('QAlpha = auto\nconvolutional')
# ax.fig.axes[4].set_ylim(-2, 10)
# ax.fig.axes[4].set_title('QAlpha = auto\ndepthwise separable')
# ax.fig.axes[5].set_ylim(-2, 10)
# ax.fig.axes[5].set_title('QAlpha = auto\ninverted residual(depthwise separable)')


# ax.fig.axes[0].set_ylim(-5, 10)
# ax.fig.axes[0].set_title('QAlpha = 1\nconvolutional')
# ax.fig.axes[1].set_ylim(-5, 10)
# ax.fig.axes[1].set_title('QAlpha = 1\ndepthwise convolutional')
# ax.fig.axes[2].set_ylim(-5, 10)
# ax.fig.axes[2].set_title('QAlpha = 1\ndepthwise separable')
#
# ax.fig.axes[3].set_ylim(-5, 10)
# ax.fig.axes[3].set_title('QAlpha = 1\ninverted residual(depthwise separable)')
# ax.fig.axes[4].set_ylim(-20, 20)
# ax.fig.axes[4].set_title('QAlpha = 1\nlinear global depthwise convolutional')
# ax.fig.axes[5].set_ylim(-5, 10)
# ax.fig.axes[5].set_title('QAlpha = 1\nlinear pointwise convolutional')
#
# ax.fig.axes[6].set_ylim(-5, 10)
# ax.fig.axes[6].set_title('QAlpha = auto\nconvolutional')
# ax.fig.axes[7].set_ylim(-5, 10)
# ax.fig.axes[7].set_title('QAlpha = auto\ndepthwise convolutional')
# ax.fig.axes[8].set_ylim(-5, 10)
# ax.fig.axes[8].set_title('QAlpha = auto\ndepthwise separable')
#
# ax.fig.axes[9].set_ylim(-5, 10)
# ax.fig.axes[9].set_title('QAlpha = auto\ninverted residual(depthwise separable)')
# ax.fig.axes[10].set_ylim(-20, 20)
# ax.fig.axes[10].set_title('QAlpha = auto\nlinear global depthwise convolutional')
# ax.fig.axes[11].set_ylim(-5, 10)
# ax.fig.axes[11].set_title('QAlpha = auto\nlinear pointwise convolutional')


#
# ax2 = sns.relplot(x='Q-bits',
#                   y='Accuracy loss (pp)',
#                   data=scores[scores['Dataset'] == 'MNISTFashion'],
#                   col='Layer_type',
#                   col_wrap=3,
#                   col_order=[
#                       'convolutional',
#                       'depthwise convolutional',
#                       'depthwise separable',
#                       'inverted residual(depthwise separable)',
#                       'global depthwise convolutional',
#                       'linear pointwise convolutional'],
#                   hue='Configuration',
#                   hue_order=[
#                      'conv-batch-act',
#                      'cba-depthwise_conv-batch-act',
#                      'cba-separable',
#                      'cba-res(sep)',
#                      'cba-depthwise_conv-batch',
#                      'cba-pw-batch',
#                      'fold(conv-batch)-act',
#                      'cbfa-fold(depthwise_conv)-act',
#                      'cbfa-separable',
#                      'cbfa-res(sep)',
#                      'cbfa-fold(depthwise_conv)',
#                      'cbfa-folded(pw)'],
#                   size='Folded',
#                   style='QAlpha',
#                   markers=True,
#                   kind='line',
#                   facet_kws={'sharey': False, 'sharex': False},
#                   height=3, aspect=2
#                   )
# for f in ax2.axes:
#     f.invert_xaxis()
# ax2.fig.axes[0].set_ylim(-2, 10)
# ax2.fig.axes[4].set_ylim(-2, 10)
# ax2.fig.suptitle('Experiments with MNIST Fashion Dataset')
# ax2.fig.subplots_adjust(top=0.9)
#


def twin_lineplot(x, y, color, **kwargs):
    ax = plt.twinx()
    sns.lineplot(x=x, y=y, color=color, **kwargs, ax=ax)


ind_scores = pd.read_csv('tensorboardcsv/individualscores.csv', sep=';')

ax3 = sns.relplot(x='Epoch',
                  y='Accuracy (%)',
                  data=ind_scores,
                  # ci=1,
                  style='Model',
                  row='Dataset',
                  # size='Q-bits',
                  hue='Layer',
                  palette="tab10",
                  kind='line',
                  facet_kws={'sharey': False, 'sharex': False}
                  )
# ax2.fig.axes[1].axline(xy1=(98.45, 98.45), slope=0, color="0", dashes=(2, 1), zorder=0, lw=3)
# ax2.axes[0].invert_yaxis()
# ax2.fig.axes[0].set_ylim(-40, 0)
ax3.fig.axes[0].set_ylim(85, 92)
ax3.fig.axes[1].set_ylim(97, 100)

plt.figure()

ax4 = sns.barplot(x='Layer',
                  y='Parameters',
                  data=ind_scores,
                  hue='Layer',
                  palette="tab10",
                  # legend=False,
                  dodge=False)
ax4.yaxis.tick_right()
ax4.yaxis.set_label_position("right")
ax4.set(yscale="log")
ax4.legend([], [], frameon=False)
ax4.set_ylim(1e2, 1e8)
ax4.set_facecolor('white')

mixed_scores = pd.read_csv('mixed.csv', sep=';')
ax5 = sns.relplot(x='Total weight bits (million)',
                  y='Accuracy (%)',
                  data=mixed_scores,
                  hue='Type',
                  col='Dataset',
                  # kind='line',
                  facet_kws={'sharex': True}
                  )
ax5.fig.axes[0].set_ylim(72.5, 100)

#
# ax = sns.relplot(x='Q-bits',
#                  y='Accuracy loss (pp)',
#                  data=scores[scores['Dataset'] == 'MNIST'],
#                  col='Layer_type',
#                  col_wrap=3,
#                  col_order=[
#                      'convolutional',
#                      'depthwise convolutional',
#                      'depthwise separable',
#                      'inverted residual(depthwise separable)',
#                      'global depthwise convolutional',
#                      'linear pointwise convolutional'],
#                  hue='Configuration',
#                  hue_order=[
#                      'conv-batch-act',
#                      'cba-depthwise_conv-batch-act',
#                      'cba-separable',
#                      'cba-res(sep)',
#                      'cba-depthwise_conv-batch',
#                      'cba-pw-batch',
#                      'fold(conv-batch)-act',
#                      'cbfa-fold(depthwise_conv)-act',
#                      'cbfa-separable',
#                      'cbfa-res(sep)',
#                      'cbfa-fold(depthwise_conv)',
#                      'cbfa-folded(pw)'
#                  ],
#                  size='Folded',
#                  style='Seed',
#                  markers=True,
#                  kind='line',
#                  facet_kws={'sharey': False, 'sharex': False},
#                  height=3, aspect=2
#                  )
# for f in ax.axes:
#     f.invert_xaxis()
# ax.fig.axes[0].set_ylim(-2, 10)
# ax.fig.subplots_adjust(top=0.9)
# ax.fig.suptitle('Experiments with MNIST Dataset')

# sns.relplot(x='Q-bits',
#             y='Accuracy (%)',
#             data=scores[scores['Layer_type'] != 'convolutional'],
#             col='Layer_type',
#             hue='Configuration',
#             style='Dataset',
#             markers=True,
#             kind='line',
#             )
# plt.ylim(80, 100)
#
# g = sns.relplot(x='Q-bits',
#                 y='Accuracy loss (%)',
#                 data=scores,
#                 col='Stride',
#                 hue='Configuration',
#                 size='Dataset',
#                 style='Layer_type',
#                 markers=True,
#                 kind='line',
#                 )
#
# g.map(plt.axhline, y=0, color="0", dashes=(2, 1), zorder=0, lw=3)


#
# g.data = scores
# g.map(sns.lineplot, 'Q-bits', 'Reference (%)', 'Configuration')

# sns.lineplot(x='Q-bits',
#              y='Reference (%)',
#              data=scores,
#              hue='Configuration',
#              style='Dataset',
#              markers=True,
#              legend=False,
#              ax=g.axes[0, 0].twinx()
#              )


plt.show()