import numpy as np
import random
import os

import tensorflow as tf
import keras.callbacks
from layers import *
import keras.utils as ku
from visualize import *
from tflite_helper import *
from models import MNISTModel
from keras.datasets import mnist as mnist
from keras.optimizers import *
import matplotlib.pyplot as plt
from collections import Counter
import logging
import pandas as pd
logging.getLogger('matplotlib').setLevel(logging.WARNING)
logging.getLogger('tensorflow').setLevel(logging.WARNING)
# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"


def set_seed():
    random.seed(2)
    np.random.seed(2)
    tf.random.set_seed(2)
    tf.keras.utils.set_random_seed(2)
    tf.config.experimental.enable_op_determinism()


# Load data to the correct datatype
def pre_process_float(x1, x2, y1, y2, is_float=True):
    """ Load data to the correct datatype """
    if is_float:
        x1 = (x1.reshape(-1, 28, 28, 1) - 127.5).astype('float32')
        x2 = (x2.reshape(-1, 28, 28, 1) - 127.5).astype('float32')
        x1 /= 127.5
        x2 /= 127.5
    else:
        x1 = (x1.reshape(-1, 28, 28, 1) - 128).astype('float32')
        x2 = (x2.reshape(-1, 28, 28, 1) - 128).astype('float32')
    print(x1.shape[0], 'train samples')
    print(x2.shape[0], 'test samples')
    num_classes = 10

    # convert class vectors to binary class matrices
    y1 = keras.utils.to_categorical(y1, num_classes)
    y2 = keras.utils.to_categorical(y2, num_classes)
    return x1, x2, y1, y2, num_classes


# Load dataset as train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test, y_train, y_test, num_class = pre_process_float(x_train, x_test, y_train, y_test)

# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
# os.environ["CUDA_VISIBLE_DEVICES"] = ""

modellst = ['mnist0', 'mnist1', 'mnist2', 'mnist3', 'mnist4']

for mdl in ['mnist0', 'mnist1', 'mnist2']:
    set_seed()

    # Create model
    model = MNISTModel(mdl, quantize=False)
    model.summary()
    # model.visualize_weights()
    model_name = model.name

    # Training loop
    visual_callback = CustomCallback(model, x_test, y_test)
    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.SGD(learning_rate=0.1, momentum=0.9, nesterov=True),
                  metrics=['accuracy'])
    logger = model.log

    hist = model.fit(x_train, y_train,
                     batch_size=128,
                     shuffle=False,
                     epochs=10,
                     verbose=1,
                     validation_data=(x_test, y_test),
                     callbacks=[visual_callback]
                     )

    score = model.evaluate(x_test, y_test, verbose=1)

    print("Test loss {:.4f}, accuracy {:.2f}%".format(score[0], score[1] * 100))

    model.save_weights('models/' + model_name + '/' + model_name)

    ###########################################
    #               Test TFLite               #
    ###########################################
    print(f"Evaluating: {model_name}...")

    tflite_models, score_list = get_tflite_models(logger, model, model_name, x_train)
    modellist = ['tflite0', 'tflite1', 'tflite2', 'tflite3']
    if model.quantize:
        modellist = modellist[1:]
        tflite_models = tflite_models[1:]

    for t, tfm in enumerate(tflite_models):
        try:
            test_img_idx = range(x_test.shape[0])
            predictions, timing = run_tflite_model(logger, tfm, x_test, y_test, test_img_idx)

            score_list['inf(ms)_' + modellist[t]] = round(timing * 1000, 3)

            accuracy = (np.sum(np.argmax(y_test, axis=1) == predictions) * 100) / len(x_test)
            score_list['score(%)_' + modellist[t]] = round(accuracy, 2)

            logger.info('%s model accuracy is %.4f%% (Number of test samples=%d)' % (
                modellist[t], accuracy, len(x_test)))
        except Exception as e: print(modellist[t], e)

    score_list['score_before'] = round(score[1] * 100, 2)

    scores = {model_name: score_list}
    scores_df = pd.DataFrame(scores).transpose()

    if os.path.exists('all_scores.csv'):
        scores_total = pd.read_csv('all_scores.csv', index_col=0)
        scores_total = pd.concat([scores_total, scores_df])
    else:
        scores_total = scores_df
    scores_total.to_csv('all_scores.csv')
    model.close_log()
