import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
sns.set()

scores = pd.read_csv('sep_split.csv', sep=';')

ax = sns.relplot(x='Q-bits',
                 y='Accuracy loss (%p)',
                 data=scores[(scores['Folded'] == True) &
                             (scores['QAlpha'] == 'auto')
                             ],
                 col='Layer_type',
                 col_order=[
                     'depthwise separable',
                     'inverted residual(depthwise separable)',
                 ],
                 # row='Split',
                 # row_order=[True, False],
                 # style='Folded',
                 hue='Split',
                 style='Dataset',
                 markers=True,
                 kind='line',
                 # showfliers=False,
                 facet_kws={'sharey': False, 'sharex': False, 'margin_titles': True},
                 # height=3, aspect=2
                 height=3, aspect=1.5
                 )
for f in ax.axes.ravel():
    f.invert_xaxis()
    f.set_ylim(-2, 10)
    # f.set(yscale="log")

ax.fig.subplots_adjust(top=0.9)
ax.fig.suptitle('Comparing different convolution layers')
ax.set_titles(row_template='Split: {row_name}', col_template='{col_name}', fontweight="bold")


scores = pd.read_csv('df_experiment4.csv', sep=';')

ax2 = sns.relplot(x='Q-bits',
                 y='Accuracy loss (%p)',
                 data=scores[(scores['Folded'] == True) &
                             (scores['Split'] == False) &
                             (scores['Integer'] == 0)],
                 col='Layer_type',
                 col_order=[
                     'depthwise separable',
                     'inverted residual(depthwise separable)',
                 ],
                 # row='Split',
                 # row_order=[True, False],
                 # style='Folded',
                 # hue='Split',
                 hue='QAlpha',
                 style='Dataset',
                 markers=True,
                 kind='line',
                 # showfliers=False,
                 facet_kws={'sharey': False, 'sharex': False, 'margin_titles': True},
                 # height=3, aspect=2
                 height=3, aspect=1.5
                 )
for f in ax2.axes.ravel():
    f.invert_xaxis()
    f.set_ylim(-2, 10)
    # f.set(yscale="log")

ax2.fig.subplots_adjust(top=0.9)
ax2.fig.suptitle('Comparing different convolution layers')
ax2.set_titles(row_template='Split: {row_name}', col_template='{col_name}', fontweight="bold")
plt.show()