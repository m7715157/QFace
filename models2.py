from model import MainModel
from keras.layers import *
from keras.models import Model
from qkeras import *
from tensorflow import optimizers
from keras import losses
import tensorflow_model_optimization as tfmot

import logging
logging.getLogger('proplot').setLevel(logging.WARNING)


class MNISTModel(MainModel):
    def __init__(self, model_type=None, dataset='', bits=None, shape=(28, 28, 1), classes=10):
        super().__init__()

        # MNIST Convolutional stuff
        self.activation_type = 'relu'
        self.inputs = Input(shape, name='input_0')
        self.flatten = Flatten()
        self.dense = Dense(classes, activation='softmax')
        self.qdense = QDense(classes,
                             kernel_quantizer=quantized_bits(bits=8, integer=0, alpha=1),
                             bias_quantizer=quantized_bits(bits=32, integer=0, alpha=1),
                             activation='softmax')

        # Model init stuff
        self.dataset = dataset
        self.model_type = model_type
        self.get_model = self.mnist_model
        self.model = self.get_model()
        self.name_ref = self.name
        self.loss_fn = losses.categorical_crossentropy
        self.optim_fn = optimizers.Adam()

        self.init_paths()
        self.init_log()

    from _layers import cba, ca, depthwise_conv, residual, act, separable
    from _qlayers import Qcba, Qca, Qdepthwise_conv, Qresidual, Qact, Qseparable
    from _visualize import visualize_weights, visualize_responses

    def __getattr__(self, name):
        return getattr(self.model, name)

    def mnist_model(self):
        x = _inputs = self.inputs
        x = self.mut(x)
        x = self.flatten(x)
        if self.quantize:
            out = self.qdense(x)
        else:
            out = self.dense(x)
        self.layer_nr = 0
        return Model(_inputs, out, name=self.name)

    def mut(self, x):
        if self.model_type == 'mnist0':
            return self.mnist0(x)
        elif self.model_type == 'mnist1':
            return self.mnist1(x)
        elif self.model_type == 'mnist2':
            return self.mnist2(x)
        elif self.model_type == 'mnist3':
            return self.mnist3(x)
        elif self.model_type == 'mnist4':
            return self.mnist4(x)
        elif self.model_type == 'mnist5':
            return self.mnist5(x)
        elif self.model_type == 'mnist6':
            return self.mnist6(x)
        elif self.model_type == 'mnist7':
            return self.mnist7(x)
        elif self.model_type == 'mnist8':
            return self.mnist8(x)
        elif self.model_type == 'mnist9':
            return self.mnist9(x)
        elif self.model_type == 'mnist10':
            return self.mnist10(x)
        elif self.model_type == 'mnist11':
            return self.mnist11(x)
        elif self.model_type == 'mnist12':
            return self.mnist12(x)
        elif self.model_type == 'mnist13':
            return self.mnist13(x)
        else:
            return print(self.model_type, "Not Found!")

### Convolutional options
    def mnist0(self, x):
        self.name = 'MNIST' + self.dataset + 'Model_ca1'
        self.layer_type = 'convolutional'
        self.mut_config = 'conv-act'
        self.stride = 1
        self.folded = False

        if self.quantize:
            x = self.Qca(depth=3, kernel=5, stride=self.stride, name='Qca')(x)
        else:
            x = self.ca(depth=3, kernel=5, stride=self.stride, name='ca')(x)
        return x

    def mnist1(self, x):
        self.name = 'MNIST' + self.dataset + 'Model_ca2'
        self.layer_type = 'convolutional'
        self.mut_config = 'conv-act'
        self.stride = 2
        self.folded = False
        if self.quantize:
            x = self.Qca(depth=3, kernel=5, stride=self.stride, name='Qca')(x)
        else:
            x = self.ca(depth=3, kernel=5, stride=self.stride, name='ca')(x)
        return x

    def mnist2(self, x):
        self.name = 'MNIST' + self.dataset + 'Model_cba1'
        self.layer_type = 'convolutional'
        self.mut_config = 'conv-batch-act'
        self.stride = 1
        self.folded = False
        if self.quantize:
            x = self.Qcba(depth=3, fold=False, kernel=5, stride=self.stride, name='Qcba')(x)
        else:
            x = self.cba(depth=3, kernel=5, stride=self.stride, name='cba')(x)
        return x

    def mnist3(self, x):
        self.name = 'MNIST' + self.dataset + 'Model_cba2'
        self.layer_type = 'convolutional'
        self.mut_config = 'conv-batch-act'
        self.stride = 2
        self.folded = False
        if self.quantize:
            x = self.Qcba(depth=3, fold=False, kernel=5, stride=self.stride, name='Qcba')(x)
        else:
            x = self.cba(depth=3, kernel=5, stride=self.stride, name='Qcba')(x)
        return x

    def mnist4(self, x):
        self.name = 'MNIST' + self.dataset + 'Model_cbfa1'
        self.layer_type = 'convolutional'
        self.mut_config = 'fold(conv-batch)-act'
        self.stride = 1
        self.folded = True
        if self.quantize:
            x = self.Qcba(depth=3, kernel=5, stride=self.stride, fold=self.folded, name='Qcbfa')(x)
        else:
            x = self.cba(depth=3, kernel=5, stride=self.stride, name='cbfa')(x)
        return x

    def mnist5(self, x):
        self.name = 'MNIST' + self.dataset + 'Model_cbfa2'
        self.layer_type = 'convolutional'
        self.mut_config = 'fold(conv-batch)-act'
        self.stride = 2
        self.folded = True
        if self.quantize:
            x = self.Qcba(depth=3, kernel=5, stride=self.stride, fold=self.folded, name='Qcbfa')(x)
        else:
            x = self.cba(depth=3, kernel=5, stride=self.stride, name='cbfa')(x)
        return x

### Depthwise Separable
    def mnist6(self, x):
        self.name = 'MNIST' + self.dataset + 'Model_sep'
        self.layer_type = 'depthwise_separable'
        self.mut_config = 'cba-SeparableConv2D'
        self.stride = 1
        self.folded = False
        if self.quantize:
            # self.name += '_QAT'
            x = self.Qcba(depth=3, kernel=3, fold=self.folded, name='conv1')(x)
            x = self.Qseparable(depth=3, kernel=5, stride=self.stride, name='sep1')(x)
        else:
            x = self.cba(depth=3, kernel=3, name='conv1')(x)
            x = self.separable(depth=3, kernel=5, stride=self.stride, name='sep1')(x)
        return x

    def mnist7(self, x):
        self.name = 'MNIST' + self.dataset + 'Model_sepf'
        self.layer_type = 'depthwise_separable'
        self.mut_config = 'cbfa-SeparableConv2D'
        self.stride = 1
        self.folded = True
        if self.quantize:
            # self.name += '_QAT'
            x = self.Qcba(depth=3, kernel=3, fold=self.folded, name='conv1')(x)
            x = self.Qseparable(depth=3, kernel=5, stride=self.stride, name='sep1')(x)
        else:
            x = self.cba(depth=3, kernel=3, name='conv1')(x)
            x = self.separable(depth=3, kernel=5, stride=self.stride, name='sep1')(x)
        return x

### Depthwise Separable Split
    def mnist8(self, x):
        self.name = 'MNIST' + self.dataset + 'Model_dwpw'
        self.layer_type = 'depthwise_separable'
        self.mut_config = 'cba-dw-pw-ba'
        self.stride = 1
        self.folded = False
        if self.quantize:
            # self.name += '_QAT'
            x = self.Qcba(depth=3, kernel=3, fold=self.folded, name='Qconv1')(x)
            x = self.Qdepthwise_conv(kernel=5, stride=self.stride, fold=self.folded, name='Qdw1')(x)
            x = self.Qcba(depth=3, kernel=1, fold=self.folded, name='Qpw1')(x)
        else:
            x = self.cba(depth=3, kernel=3, name='conv1')(x)
            x = self.depthwise_conv(kernel=5, stride=self.stride, name='dw1')(x)
            x = self.cba(depth=3, kernel=1, name='pw1')(x)

        return x

    def mnist9(self, x):
        self.name = 'MNIST' + self.dataset + 'Model_dwpwf'
        self.layer_type = 'depthwise_separable'
        self.mut_config = 'fold(cba-dw-pw-ba)'
        self.stride = 1
        self.folded = True
        if self.quantize:
            # self.name += '_QAT'
            x = self.Qcba(depth=3, kernel=3, fold=self.folded, name='conv1')(x)
            x = self.Qdepthwise_conv(kernel=5, stride=self.stride, fold=self.folded, name='dw1')(x)
            x = self.Qcba(depth=3, kernel=1, fold=self.folded, name='pw1')(x)
        else:
            x = self.cba(depth=3, kernel=3, name='conv1')(x)
            x = self.depthwise_conv(kernel=5, stride=self.stride, name='dw1')(x)
            x = self.cba(depth=3, kernel=1, name='pw1')(x)
        return x

### Residual
    ### Depthwise Separable
    def mnist10(self, x):
        self.name = 'MNIST' + self.dataset + 'Model_res_sep'
        self.layer_type = 'residual depthwise_separable'
        self.mut_config = 'cba-res(SeparableConv2D)'
        self.stride = 1
        self.folded = False
        if self.quantize:
            # self.name += '_QAT'
            x = self.Qcba(depth=3, kernel=3, fold=self.folded, name='conv1')(x)
            x = self.Qresidual(depth=3, kernel=5, stride=self.stride, split=False, fold=self.folded, name='res1')(x)
        else:
            x = self.cba(depth=3, kernel=3, name='conv1')(x)
            x = self.residual(depth=3, kernel=5, stride=self.stride, name='res1')(x)
        return x

    def mnist11(self, x):
        self.name = 'MNIST' + self.dataset + 'Model_res_sepf'
        self.layer_type = 'residual depthwise_separable'
        self.mut_config = 'cbfa-res(SeparableConv2D)'
        self.stride = 1
        self.folded = True
        if self.quantize:
            # self.name += '_QAT'
            x = self.Qcba(depth=3, kernel=3, fold=self.folded, name='conv1')(x)
            x = self.Qresidual(depth=3, kernel=5, stride=self.stride, split=False, fold=self.folded, name='res1')(x)
        else:
            x = self.cba(depth=3, kernel=3, name='conv1')(x)
            x = self.residual(depth=3, kernel=5, stride=self.stride, split=False, name='res1')(x)
        return x

    ### Depthwise Separable Split
    def mnist12(self, x):
        self.name = 'MNIST' + self.dataset + 'Model_res_dwpw'
        self.layer_type = 'residual depthwise_separable'
        self.mut_config = 'cba-res(dw-pw)'
        self.stride = 1
        self.folded = False
        if self.quantize:
            # self.name += '_QAT'
            x = self.Qcba(depth=3, kernel=3, fold=self.folded, name='conv1')(x)
            x = self.Qresidual(depth=3, kernel=5, stride=self.stride, split=True, fold=self.folded, name='res1')(x)
        else:
            x = self.cba(depth=3, kernel=3, name='conv1')(x)
            x = self.residual(depth=3, kernel=5, stride=self.stride, split=True, name='res1')(x)
        return x

    def mnist13(self, x):
        self.name = 'MNIST' + self.dataset + 'Model_res_dwpwf'
        self.layer_type = 'residual depthwise_separable'
        self.mut_config = 'cbfa-res(dwf-pwf)'
        self.stride = 1
        self.folded = True
        if self.quantize:
            # self.name += '_QAT'
            x = self.Qcba(depth=3, kernel=3, fold=self.folded, name='conv1')(x)
            x = self.Qresidual(depth=3, kernel=5, stride=self.stride, split=True, fold=self.folded, name='res1')(x)
        else:
            x = self.cba(depth=3, kernel=3, name='conv1')(x)
            x = self.residual(depth=3, kernel=5, stride=self.stride, split=True, name='res1')(x)
        return x
