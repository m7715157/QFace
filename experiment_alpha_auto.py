import numpy as np
import random
import os

import tensorflow as tf
import keras.callbacks
import keras.utils as ku
from tflite_helper import *
from models3 import MNISTModel
from keras.datasets import fashion_mnist
from keras.datasets import mnist
from keras.optimizers import *
import matplotlib.pyplot as plt
import matplotlib
from collections import Counter
import logging
import pandas as pd
logging.getLogger('matplotlib').setLevel(logging.WARNING)
logging.getLogger('tensorflow').setLevel(logging.WARNING)
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
os.environ['TF_DETERMINISTIC_OPS'] = '1'
matplotlib.use('Agg')


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    tf.random.set_seed(seed)
    try:
      tf.config.experimental.enable_op_determinism()
    except:
      pass


# Load data to the correct datatype
def pre_process_float(x1, x2, y1, y2, is_float=True):
    """ Load data to the correct datatype """
    if is_float:
        x1 = (x1.reshape(-1, 28, 28, 1) - 127.5).astype('float32')
        x2 = (x2.reshape(-1, 28, 28, 1) - 127.5).astype('float32')
        x1 /= 127.5
        x2 /= 127.5
    else:
        x1 = (x1.reshape(-1, 28, 28, 1) - 128).astype('float32')
        x2 = (x2.reshape(-1, 28, 28, 1) - 128).astype('float32')
    print(x1.shape[0], 'train samples')
    print(x2.shape[0], 'test samples')
    num_classes = 10

    # convert class vectors to binary class matrices
    y1 = ku.np_utils.to_categorical(y1, num_classes)
    y2 = ku.np_utils.to_categorical(y2, num_classes)
    return x1, x2, y1, y2, num_classes


# # Load dataset as train and test sets
# (x_train, y_train), (x_test, y_test) = mnist.load_data()
# x_train, x_test, y_train, y_test, num_class = pre_process_float(x_train, x_test, y_train, y_test)



for qint in [
    0,
    # 1,
    # 2,
    # 3,
    # 4,
    # 5,
    # 6,
    # 7
]:
    for dataset in [
        'mnist',
        'Fashion'
    ]:
        if dataset == 'mnist':
            print('loading mnist dataset')
            (x_train, y_train), (x_test, y_test) = mnist.load_data()
            x_train, x_test, y_train, y_test, num_class = pre_process_float(x_train, x_test, y_train, y_test)
        elif dataset == 'Fashion':
            print('loading mnist Fashion dataset')
            (x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()
            x_train, x_test, y_train, y_test, num_class = pre_process_float(x_train, x_test, y_train, y_test)
        else:
            print('!!!!!!!!!!Dataset not found!!!!!!!!!!!!!!!!!!')
            break
        for seed in [
            2,
            3,
            4,
            5
        ]:
            for t in [
                      # 'conv',
                      # 'depthwise_conv',
                      'separable_conv',
                      # 'separableMB_conv',
                      'inverted_residual',
                      # 'inverted_residualMB',
                      # 'lin_global_depthwise_conv',
                      # 'lin_pointwise_conv'
            ]:
                for f in [
                    True,
                    False
                ]:
                    for split in [
                        True,
                        False
                    ]:

                        set_seed(seed)
                        model = MNISTModel(mnist_type=t,
                                           dataset=dataset,
                                           seed=seed,
                                           split=split,
                                           folded_bn=f,
                                           auto_alpha=True,
                                           qint=qint,
                                           )
                        model.summary()
                        scores = pd.read_csv(model.df_scores_name)
                        if model.name not in list(scores['Name']):
                            try:
                                model.setup_training(x_test, y_test)
                                # model.pre_train_model(x_train, y_train)
                                model.qkeras_train_bit(x_train, y_train)
                            except Exception as e:
                                print(e)
                                print("!!!!!ERRRORRRR!!!ERROR!!!" + model.name)
