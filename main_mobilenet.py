import keras.utils.data_utils

from mbmodel_trainer import MobileNetTrainer
from keras.applications.mobilenet_v2 import MobileNetV2
from keras.applications.mobilenet_v2 import preprocess_input, decode_predictions
import tensorflow as tf
import keras.utils as utils
import matplotlib
import matplotlib.pyplot as plt
import logging
import sys, os, time
from pathlib import Path
from glob import glob
import csv
import tqdm
import numpy as np
import callbacks
from tensorflow import optimizers

logging.getLogger('matplotlib').setLevel(logging.WARNING)
logging.getLogger('tensorflow').setLevel(logging.WARNING)
# os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
# os.environ['TF_DETERMINISTIC_OPS'] = '1'
matplotlib.use('Agg')


def preprocess(images, labels):
    return preprocess_input(images), labels


dataset_path = Path('/deepstore/datasets/dmb/MachineLearning/ImageNet/ILSVRC/Data/CLS-LOC/train/')
valset_path = Path('/deepstore/datasets/dmb/MachineLearning/ImageNet/ILSVRC/Data/CLS-LOC/val/')

batch_size = 128
train_set = tf.keras.utils.image_dataset_from_directory(dataset_path,
                                                        labels='inferred',
                                                        image_size=(224, 224),
                                                        batch_size=batch_size,
                                                        label_mode='categorical')

val_set = tf.keras.utils.image_dataset_from_directory(valset_path,
                                                      labels='inferred',
                                                      image_size=(224, 224),
                                                      batch_size=batch_size,
                                                      label_mode='categorical')

train_set = train_set.map(preprocess)
val_set = val_set.map(preprocess)

model = MobileNetTrainer(batch_size=batch_size)

model.train_dataset = train_set
model.val_set = val_set
model.load_weights(0)
optimizer = optimizers.Adam(learning_rate=0.001)
model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics='accuracy')
# model.evaluate(val_set)
# runlog = callbacks.MobileNetCallback(model.log)

# model.model.fit(train_set,
#                 validation_data=val_set,
#                 batch_size=batch_size,
#                 # shuffle=True,
#                 epochs=1,
#                 # verbose=2,
#                 callbacks=[runlog])
model.quantize_checkpoint = 'MobileNetV2_2022-04-17T15-41-22'

model.train_equal_q(8, load_ckpt=10, num_epochs=20)
