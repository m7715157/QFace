import keras.backend as K
from keras.applications import imagenet_utils
from keras.layers import *
from keras.models import Model
from qkeras import QConv2DBatchnorm, QDepthwiseConv2DBatchnorm, QMobileNetSeparableConv2D


def act(self, name):
    if self.activation_type == 'relu':
        return ReLU(name=name)
    elif self.activation_type == 'relu6':
        return ReLU(6., name=name)


def cba(self, depth, kernel=3, stride=1, act=True, name=None):
    self.layer_counter += 1
    # name = '' if name is None else f'{name}_'

    # noinspection PyCallingNonCallable
    def cba_block(x):
        if self.folded: # and not self.tflite:
            x = QConv2DBatchnorm(filters=depth,
                                 kernel_size=kernel,
                                 strides=stride,
                                 padding='same',
                                 use_bias=self.bias,
                                 kernel_initializer=self.initializer,
                                 kernel_quantizer=None,
                                 name=f'{name}conv{self.layer_counter}'
                                 )(x)
        else:
            x = Conv2D(depth,
                       kernel_size=kernel,
                       strides=stride,
                       padding='same',
                       use_bias=self.bias,
                       kernel_initializer=self.initializer,
                       name=name
                       )(x)
            if not self.tflite:
                x = BatchNormalization(
                    axis=-1,
                    epsilon=1e-3,
                    momentum=0.999,
                    name=f'{name}_BN'
                )(x)
        if act:
            x = self.act(name=f'{name}_relu')(x)
        return x
    return cba_block


def ca(self, depth, kernel=3, stride=1, name=None):
    self.layer_counter += 1
    name = '' if name is None else f'{name}_'

    def ca_block(x):
        x = Conv2D(depth,
                   kernel_size=kernel,
                   strides=stride,
                   padding='same',
                   use_bias=True,
                   kernel_initializer=self.initializer,
                   name=f'{name}conv{self.layer_counter}'
                   )(x)
        x = self.act(name=f'{name}act{self.layer_counter}')(x)
        return x
    return ca_block


def depthwise_conv(self, kernel=3, stride=1, padding='same', act=True, name=None):
    self.layer_counter += 1
    name = '' if name is None else f'{name}_'

    # noinspection PyCallingNonCallable
    def depthwise_block(x):
        # if self.folded:
        #     x = QDepthwiseConv2DBatchnorm(kernel_size=kernel,
        #                                   strides=stride,
        #                                   padding=padding,
        #                                   use_bias=self.bias,
        #                                   depthwise_initializer=self.initializer,
        #                                   depthwise_quantizer=None,
        #                                   name=f'{name}_folded')(x)
        # else:
        x = DepthwiseConv2D(kernel_size=kernel,
                            strides=stride,
                            padding=padding,
                            use_bias=True,
                            depthwise_initializer=self.initializer,
                            name=name
                            )(x)
        # x = BatchNormalization(
        #     axis=-1,
        #     epsilon=1e-3,
        #     momentum=0.999,
        #     name=f'{name}_BN')(x)
        if act:
            x = self.act(name=f'{name}_relu')(x)
        return x
    return depthwise_block


def residual(self, shape, depth, kernel=3, stride=1, expansion=1, split=False, name='res'):
    self.layer_counter += 1
    # name = '' if name is None else name

    def res(x):
        ch_in = shape[-1]
        hidden_dim = int(round(ch_in * expansion))
        shortcut = x

        if expansion != 1:
            x = self.cba(hidden_dim, kernel=1, name=f'{name}expand')(x)
        if stride == 2:
            x = ZeroPadding2D(
                padding=imagenet_utils.correct_pad(x, kernel), name=name + 'pad')(x)

        x = self.separable(depth, kernel, stride, split=split, name=name if expansion != 1 else 'expanded_conv_')(x)
        if stride == 1 and ch_in == depth:
            x = Add(name=f'{name}add')([shortcut, x])
        return x
    return res


def residual_model(self, shape, depth, kernel=3, stride=1, expansion=1, split=False, name='res_model'):
    self.layer_counter += 1
    # name = '' if name is None else f'{name}_'

    ch_in = shape[-1]
    x = _inputs = Input(shape[1:], name='input')
    hidden_dim = int(round(ch_in * expansion))
    shortcut = x

    if expansion != 1:
        x = self.cba(hidden_dim, kernel=1, name=f'{name}expand')(x)
    if stride == 2:
        x = ZeroPadding2D(
            padding=imagenet_utils.correct_pad(x, kernel), name=name + 'pad'
        )(x)

    x = self.separable(depth, kernel, stride, split=split, name=name if expansion != 1 else 'expanded_conv_')(x)
    if stride == 1 and ch_in == depth:
        x = Add(name=f'{name}add')([shortcut, x])

    return Model(_inputs, x, name=name)


def separable(self, depth, kernel=3, stride=1, split=True, name=None):
    self.layer_counter += 1
    # name = '' if name is None else f'{name}_'

    def sep(x):
        if split:
            x = DepthwiseConv2D(
                kernel_size=3,
                strides=stride,
                activation=None,
                use_bias=False,
                padding='same' if stride == 1 else 'valid',
                name=name + 'depthwise')(
                x)
            x = BatchNormalization(
                axis=-1,
                epsilon=1e-3,
                momentum=0.999,
                name=name + 'depthwise_BN')(
                x)
            x = self.act(name=f'{name}depthwise_relu')(x)

            x = Conv2D(depth,
                       kernel_size=1,
                       activation=None,
                       use_bias=False,
                       padding='same' if stride == 1 else 'valid',
                       name=name + 'project')(
                       x)
            x = BatchNormalization(
                axis=-1,
                epsilon=1e-3,
                momentum=0.999,
                name=name + 'project_BN')(
                x)
        else:
            x = SeparableConv2D(depth,
                                kernel_size=kernel,
                                strides=(stride, stride),
                                padding='same' if stride == 1 else 'valid',
                                depthwise_initializer=self.initializer,
                                pointwise_initializer=self.initializer,
                                use_bias=True,
                                name=f'{name}separable{self.layer_nr}'
                                )(x)
        return x
    return sep
