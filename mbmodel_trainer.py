import datetime
import keras.callbacks

from model import MainModel
from mbmodel import MobileNet
from keras.applications.mobilenet_v2 import MobileNetV2
from keras.applications.mobilenet_v2 import preprocess_input, decode_predictions

from keras.layers import *
from keras.models import Model
from qkeras import *
from tensorflow import optimizers
from keras import losses
from keras.regularizers import L2
import tensorflow_model_optimization as tfmot
from keras.applications import imagenet_utils
import callbacks
import os

from keras import backend as K
from keras import regularizers
import math
import time

import logging
logging.getLogger('proplot').setLevel(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class MobileNetTrainer(MobileNet):
    def __init__(self, name=False, batch_size=512):
        self.batch_size = batch_size
        super().__init__(name=name)
        self.batch_size = batch_size
        self.initial_epoch = 0
        self.num_epochs = 10
        self.best_loss = 1000
        self.val_set = None

    def lr_scheduler(self, epoch, lr):
        return lr * 0.98

    def get_callbacks(self, quantization=False):
        if quantization:
            file_path = f'{self.mdir}{self.name_ref}{self.now}/{self.head}/{self.name}/cp_{self.qbit}_l{self.quantize_layer}.ckpt'

        else:
            file_path = f'{self.mdir}{self.name_ref}{self.now}/{self.head}/{self.name}/cp.ckpt'

        lr_callback = keras.callbacks.LearningRateScheduler(self.lr_scheduler)
        checkpoint_callback = keras.callbacks.ModelCheckpoint(
            filepath=file_path,
            save_weights_only=True,
            monitor='val_accuracy',
            mode='max',
            save_best_only=True,
            save_freq='epoch'
        )
        runlog = callbacks.MobileNetCallback(self.log)
        tb_callback = keras.callbacks.TensorBoard(log_dir=self.summary_writer_path, update_freq='epoch')
        return [
            lr_callback,
            checkpoint_callback,
            runlog,
            tb_callback
        ]

    def train_layer_n(self, layer, trial=0, last=False):
        if layer == 11:
            self.layer_nr = 10
        else:
            self.layer_nr = layer

        # Create model
        self.log.info(f'layer: {self.layer_nr}')
        self.get_mbmodel(self.layer_nr)
        self.name = 'Q' + self.name
        self.log.info(f'created: {self.name}')
        load_ckpt = self.load_ckpt

        # Load weights
        if self.individual_q:
            self.load_weights(0)
            self.log.info(f'loaded pretrain weights')
        elif last:
            self.load_weights(0)
            self.log.info(f'loaded pretrain weights')
        else:
            self.load_weights(layer - 1)
            self.log.info(f'loaded weights layer: {layer - 1}')

        # Training setup
        callbacks = self.get_callbacks(quantization=True)
        self.log.info(f'starting learning rate is: {self.lr}')
        # self.optim_fn = optimizers.SGD(learning_rate=self.lr) #, momentum=0.9, nesterov=True)
        self.optim_fn = optimizers.RMSprop(learning_rate=self.lr, momentum=0.9)
        self.model.compile(optimizer=self.optim_fn, loss='categorical_crossentropy', metrics='accuracy')

        self.log.info('Evaluate best model')
        loss, acc = self.model.evaluate(self.val_set, verbose=2)
        self.log.info(f'Evaluating model ---  loss: {loss} and accuracy: {acc}')
        max_epochs = self.initial_epoch + self.num_epochs

        # Training:
        self.model.fit(self.train_dataset,
                       validation_data=self.val_set,
                       batch_size=self.batch_size,
                       shuffle=True,
                       epochs=max_epochs,
                       initial_epoch=self.initial_epoch,
                       verbose=2,
                       callbacks=callbacks)
        # self.model_fit(max_epochs)

        self.log.info('Evaluate best model')
        self.load_weights(self.layer_nr)
        self.initial_epoch = max_epochs

    def save_model(self, epoch, step, loss):
        if loss < self.best_loss:
            self.log.info(f'Saving ... {loss:3f} is smaller than before: {self.best_loss:3f}')
            print(f'Saving ... {loss:3f} is smaller than before: {self.best_loss:3f}')
            self.best_loss = loss
            self.model.save_weights(f'{self.mdir}{self.name_ref}{self.now}/{self.head}/{self.name}/'
                                              f'e_{epoch}_b_{step % self.steps_per_epoch}.ckpt')
            self.log.info(f'Saved weights: {self.mdir}/{self.name_ref}{self.now}/{self.head}/'
                          f'{self.name}/e_{epoch}_b_{step % self.steps_per_epoch}.ckpt')

            self.basemodel.save_weights(f'{self.mdir}{self.name_ref}{self.now}/'
                                        f'{self.name}/e_{epoch}_b_{step % self.steps_per_epoch}.ckpt')
            self.basemodel.save(f'{self.mdir}{self.name_ref}{self.now}/model/{self.name}.h5')

    def train_equal_q(self, bit=8, forward=True, num_epochs=20, individual=False, load_ckpt=0):
        self.qbit = bit
        self.quantize = True
        self.name = 'Q' + self.name if self.name[0] != 'Q' else self.name
        self.individual_q = individual
        self.backwards = False if forward else True
        self.num_epochs = num_epochs
        self.load_ckpt = load_ckpt
        self.initial_epoch = load_ckpt * num_epochs
        # self.qint = 1

        # Load normal model
        self.get_mbmodel(self.load_ckpt)
        self.load_weights()
        self.load_ckpt = load_ckpt
        self.optim_fn = optimizers.Adam(learning_rate=self.lr) #optimizers.RMSprop(learning_rate=1e-5, momentum=0.9)

        self.model.compile(optimizer=self.optim_fn, loss='categorical_crossentropy', metrics='accuracy')
        self.model.evaluate(self.val_set, verbose=2)

        # Define direction
        # for layer in range(load_ckpt + 1, self.model_len + 1):
        #     self.lr = 1e-2
        #     self.train_layer_n(layer=layer)
        #     self.log_model_bits()

        if load_ckpt == 10:
            self.lr = 1e-6
            self.train_layer_n(layer=10)
            self.log_model_bits()

    def train_last_qlayer(self, num_epochs=20):
        self.backwards = True
        self.head = 'ARC'
        self.quantize = True
        self.num_epochs = num_epochs

        self.get_mbmodel(0)
        self.load_weights(0)
        self.log.info(f"Original scores were: LFW ({self.score_cont[0]}), AgeDB-30 ({self.score_cont[1]})"
                      f" and CFP-FP ({self.score_cont[2]})")
        for bit in range(8, 0, -1):
            self.qbit = bit
            self.lr = 1e-3
            print(f'Converting to {self.qbit} bits')

            self.name = 'Q' + self.name + f'_q{self.qbit}' if self.name[0] != 'Q' else self.name + f'_q{self.qbit}'
            self.train_layer_n(layer=3, last=True)
            self.log_model_bits()









