import numpy as np
import random
import tensorflow as tf

import keras.backend as K
from keras.applications import imagenet_utils
from keras.layers import *
from keras.models import Model, clone_model
from keras import initializers
from keras import optimizers
from keras import losses
from qkeras import *
import qkeras.bn_folding_utils as bnf
import callbacks
import tensorflow_model_optimization as tfmot
from qkeras.utils import model_save_quantized_weights
import tflite_helper as tfh
import pandas as pd

import proplot as pplt
import logging
logging.getLogger('proplot').setLevel(logging.WARNING)
import os
import csv
import pickle
import datetime


def _get_interesting_weights(weights):
    # weight_list = [w.numpy().reshape(-1) for w in weights if 'dense' not in w.name]
    weight_list = [w.numpy().reshape(-1) for w in weights[:-2] if 'bias' not in w.name]
    return np.array([item for sublist in weight_list for item in sublist])


class MainModel(object):
    """" The main model object"""
    def __init__(self):
        super().__init__()
        # # Visualisation init
        # self.rows = 1
        # self.cols = 1
        # self.init_wax()
        self.layer_nr = 1
        self.openlogger = False
        self.log = None
        self.log_fh = None

        self.mqfig = pplt.figure(figsize=(18, 6), sharey=False, sharex=False, spanx=False, spany=False)
        plot_array = [
            [1, 3, 5, 7],
            [2, 4, 6, 8]
        ]
        self.mqaxs = self.mqfig.subplots(plot_array)

        # General Convolutional stuff
        # self.conv_types = ['conv', 'separable', 'depthwise']
        self.initializer = initializers.HeNormal(seed=2)
        self.loss_fn = None
        self.optim_fn = None
        self.score_init = [0, 0]
        self.score_cont = [0, 0]
        self.score_qkeras = [0, 0]
        self.score_tf_qmodel = [0, 0]
        self.qbit = 8
        self.scoreseries = None
        self.layer_counter = 0
        self.lr = 0
        self.qalpha = 'auto'
        self.qint = 0
        self.bias = False
        self.stride = 1
        self.now = '_' + datetime.datetime.now().isoformat(timespec='seconds').replace(':', '-')

        # Should be initialized later
        self.name = ''
        self.name_ref = ''
        self.log_name = ''
        self.callback_v = None
        self.quantize = False
        self.model = None
        self.qmodel = None
        self.tf_qmodel = None
        self.tf_preqmodel = None
        self.get_model = None
        self.mdir = None
        self.img_s = None
        self.img_r = None
        self.img_w = None
        self.dataset = ''
        self.layer_type = None
        self.layer_name = None
        self.mut_config = None
        self.folded = False
        self.seed = None

        self.tflite_scores = 'score_log.csv'
        self.df_scores_name = 'df_score_bits.csv'
        self.df_model = 'df_model.csv'
        self.score_dict = {
            'score_pretrain':   0,
            'score_train':      0,
            'score_qkeras_quant': 0,
            'score_qkeras':     0,
            'score_tf_preQAT':  0,
            'score_tf_QAT':     0,

            'score(%)_tflite0': 0,
            'score(%)_tflite1': 0,
            'score(%)_tflite2': 0,
            'score(%)_tflite3': 0,

            'size(kB)_tflite0': 0,
            'size(kB)_tflite1': 0,
            'size(kB)_tflite2': 0,
            'size(kB)_tflite3': 0,

            'inf(ms)_tflite0': 0,
            'inf(ms)_tflite1': 0,
            'inf(ms)_tflite2': 0,
            'inf(ms)_tflite3': 0,
        }

    def init_paths(self):
        self.img_w = 'img/' + self.name_ref + '/weights/'
        self.img_r = 'img/' + self.name_ref + '/reponses/'
        self.img_s = 'img/' + self.name_ref + '/model_structure/'
        self.mdir = 'models/' + self.name_ref + '/'

        os.makedirs(self.img_w, exist_ok=True)
        os.makedirs(self.img_r, exist_ok=True)
        os.makedirs(self.img_s, exist_ok=True)
        os.makedirs(self.mdir, exist_ok=True)
        os.makedirs('logs/', exist_ok=True)

        # ('You must install pydot (`pip install pydot`) and install graphviz
        # (see instructions at https://graphviz.gitlab.io/download/) ', 'for plot_model/model_to_dot to work.')
        # and pip install pydotplus
        tf.keras.utils.plot_model(self.model, self.img_s + self.name + '.png', show_shapes=True)
        self._update_weight_log([['epoch', 'layer_name']], 'w')

    def init_log(self, name=False):
        if name:
            self.log_name = 'logs/'+ name
        else:
            self.log_name = 'logs/LOG_' + self.name + self.now + '.log'
        self.log = logging.getLogger()
        self.log.setLevel(logging.INFO)
        self.log_fh = logging.FileHandler(filename=self.log_name)
        formatter = logging.Formatter(
            fmt='%(asctime)s %(levelname)s: %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S'
        )
        self.log_fh.setFormatter(formatter)
        self.log.addHandler(self.log_fh)
        self.openlogger = True

    def set_lr(self, learning_rate):
        self.lr = learning_rate

    def plot_quantdiff(self):
        # if self.tf_qmodel is None:
        plot_array = [
            [1, 1],
            [2, 2]
        ]
        fig = pplt.figure(figsize=(6, 4), sharey=False, sharex=False, spanx=False, spany=False)
        # else:
        #     plot_array = [
        #         [1, 3],
        #         [2, 4]
        #     ]
        #     model_weights = _get_interesting_weights(self.model.trainable_weights)
        #     qatmodel_weights = _get_interesting_weights(self.tf_qmodel.trainable_weights)
        #     diff_weights2 = model_weights - qatmodel_weights
        #     fig = pplt.figure(figsize=(12, 4), sharey=False, sharex=False, spanx=True, spany=False)

        pre_quant_weights = _get_interesting_weights(self.qmodel.trainable_weights)
        qmodel_weights = _get_interesting_weights(self.qmodel_quantized.trainable_weights)
        diff_weights1 = pre_quant_weights - qmodel_weights
        mean_diff = np.mean(diff_weights1)
        std_diff = np.std(diff_weights1)

        axs = fig.subplots(plot_array)
        axs[0].format(
            suptitle='Quantization Error and Difference',
            title=f'Accuracy QKeras before {round(self.score_qkeras[1]*100, 2)}% and '
                  f'after quantizing {round(self.score_qkeras_quantize[1]*100, 2)}%',
            xlabel='weights', ylabel='weight value',
        )
        axs[1].format(
            xlabel='weights', ylabel='Difference'
        )
        axs[0].step(pre_quant_weights, label='QKeras pre-quantize')
        axs[0].step(qmodel_weights, label='QKeras post-quantize')
        axs[1].step(diff_weights1, label='Difference due Quantization')
        axs[1].axhline(np.mean(diff_weights1), label=f'Mean difference {mean_diff:.4f}'r'$\pm$'f'{std_diff:.2f}', c='orange9')
        axs[0].legend(loc='b', ncols=2)
        axs[1].legend(loc='b', ncols=2)

        # if self.tf_qmodel is not None:
        #     axs[2].format(
        #         title=f'Accuracy original model {round(self.score_init[1]*100, 2)}% and '
        #               f'after TF-QAT {round(self.score_qkeras[1]*100, 2)}%',
        #     )
        #     axs[2].step(model_weights, label='Original weights')
        #     axs[2].step(qatmodel_weights, label='TF-QAT pre-quantize')
        #     axs[3].step(diff_weights2, label='Difference due TF-QAT')
        #     axs[3].axhline(np.mean(diff_weights2), label=f'Mean difference {np.mean(diff_weights2)}', c='orange9')
        #     axs[2].legend(loc='b', ncols=2)
        #     axs[3].legend(loc='b', ncols=2)
        fig.save(self.img_w + self.name_ref + '_q_error.png')

    def plot_multi_quantdiff(self):
        nr = int(4 - self.qbit/2)
        pre_quant_weights = _get_interesting_weights(self.qmodel_unfolded.trainable_weights)
        qmodel_weights = _get_interesting_weights(self.qmodel_quantized.trainable_weights)
        diff_weights = pre_quant_weights - qmodel_weights
        mean_diff = np.mean(diff_weights)
        std_diff = np.std(diff_weights)

        self.mqaxs[0 + nr*2].format(
            suptitle='Quantization Error and Difference',
            title=f'Accuracy no quantization {round(self.score_cont[1]*100, 2)}% \nand '
                  f'after QKeras {self.qbit}-bit quantizing {round(self.score_qkeras[1]*100, 2)}%',
            xlabel='weights', ylabel='weight value',
        )
        self.mqaxs[1 + nr*2].format(
            xlabel='weights', ylabel='Difference'
        )
        self.mqaxs[0 + nr*2].step(pre_quant_weights, label='QKeras pre-quantize')
        self.mqaxs[0 + nr*2].step(qmodel_weights, label='QKeras post-quantize')
        self.mqaxs[1 + nr*2].step(diff_weights, label='Difference due Quantization')
        self.mqaxs[1 + nr*2].axhline(np.mean(diff_weights), label=f'Mean difference {mean_diff:.4f}'r'$\pm$'f'{std_diff:.2f}', c='orange9')
        self.mqaxs[0 + nr*2].legend(loc='b', ncols=2)
        self.mqaxs[1 + nr*2].legend(loc='b', ncols=1)
        self.mqfig.save(self.img_w + self.name_ref + '_mq_error.png')
        if nr == 0:
            pplt.close(self.mqfig)

    def log_scores_bits(self):
        pd.options.display.float_format = "{:,.2f}".format
        acc_loss = round((self.score_cont[1] - self.score_qkeras[1]) * 100, 3)
        newscore = pd.DataFrame({
            'Layer_type': [self.layer_type],
            'Configuration': [self.mut_config],
            'Name': [self.name_ref],
            'Stride': [self.stride],
            'Bias': [self.bias],
            'QAlpha': [self.qalpha],
            'Dataset': ['MNIST' + self.dataset],
            'Q-bits': [self.qbit],
            'Accuracy (%)': [round(self.score_qkeras[1] * 100, 2)],
            'Reference (%)': [round(self.score_cont[1] * 100, 2)],
            'Accuracy loss (%)': [acc_loss],
            'Folded': [self.folded],
            'Seed': [self.seed],
            'Integer': [self.qint],
        })
        if os.path.exists(self.df_scores_name):
            scores = pd.read_csv(self.df_scores_name)
            if not scores.loc[(scores['Name'] == self.name_ref) &
                              (scores['Q-bits'] == self.qbit) &
                              (scores['Seed'] == self.seed), 'Accuracy (%)'].empty:
                print('overwrite')
                scores.loc[(scores['Name'] == self.name_ref) &
                           (scores['Q-bits'] == self.qbit) &
                           (scores['Seed'] == self.seed), 'Accuracy (%)'] = round(self.score_qkeras[1] * 100, 2)
                scores.loc[(scores['Name'] == self.name_ref) &
                           (scores['Q-bits'] == self.qbit) &
                           (scores['Seed'] == self.seed), 'Reference (%)'] = round(self.score_cont[1] * 100, 2)
                scores.loc[(scores['Name'] == self.name_ref) &
                           (scores['Q-bits'] == self.qbit) &
                           (scores['Seed'] == self.seed), 'Accuracy loss (%)'] = acc_loss
            else:
                scores = pd.concat([scores, newscore], ignore_index=True)
        else:
            scores = pd.DataFrame(columns=['Layer_type',
                                           'Configuration',
                                           'Name',
                                           'Stride',
                                           'Bias',
                                           'QAlpha',
                                           'Dataset',
                                           'Q-bits',
                                           'Accuracy (%)',
                                           'Reference (%)',
                                           'Accuracy loss (%)',
                                           'Folded',
                                           'Seed'])
            scores = pd.concat([scores, newscore], ignore_index=True)
        scores.to_csv(self.df_scores_name, index=False)

    def log_model_bits(self):
        pd.options.display.float_format = "{:,.2f}".format
        acc_loss = np.round(np.subtract(self.score_cont, self.score_qkeras) * 100, 3)
        newscore = pd.DataFrame({
            'Name': [self.name],
            'Layer Type': [self.layer_type],
            'Layer Name': [self.layer_name],
            'Q-bits': [self.qbit],
            'Accuracy LFW (%)': [round(self.score_qkeras[0] * 100, 2)],
            'Reference LFW (%)': [round(self.score_cont[0] * 100, 2)],
            'Accuracy loss LFW (%)': [acc_loss[0]],
            'Accuracy AgeDB-30 (%)': [round(self.score_qkeras[1] * 100, 2)],
            'Reference AgeDB-30 (%)': [round(self.score_cont[1] * 100, 2)],
            'Accuracy loss AgeDB-30 (%)': [acc_loss[1]],
            'Accuracy CFP_FP (%)': [round(self.score_qkeras[2] * 100, 2)],
            'Reference CFP_FP (%)': [round(self.score_cont[2] * 100, 2)],
            'Accuracy loss CFP_FP (%)': [acc_loss[2]],
            'Folded': [self.folded]
        })
        if os.path.exists(self.df_model):
            scores = pd.read_csv(self.df_model)
            if not scores.loc[(scores['Name'] == self.name) & (scores['Q-bits'] == self.qbit), 'Accuracy LFW (%)'].empty:
                print('overwrite')
                scores.loc[
                    (scores['Name'] == self.name) & (scores['Q-bits'] == self.qbit), 'Accuracy LFW (%)'] = \
                    round(self.score_qkeras[0] * 100, 2)
                scores.loc[
                    (scores['Name'] == self.name) & (scores['Q-bits'] == self.qbit), 'Reference LFW (%)'] = \
                    round(self.score_cont[0] * 100, 2)
                scores.loc[
                    (scores['Name'] == self.name) & (scores['Q-bits'] == self.qbit), 'Accuracy loss LFW (%)'] = \
                    acc_loss[0]
                scores.loc[
                    (scores['Name'] == self.name) & (scores['Q-bits'] == self.qbit), 'Accuracy AgeDB-30 (%)'] = \
                    round(self.score_qkeras[0] * 100, 2)
                scores.loc[
                    (scores['Name'] == self.name) & (scores['Q-bits'] == self.qbit), 'Reference AgeDB-30 (%)'] = \
                    round(self.score_cont[0] * 100, 2)
                scores.loc[
                    (scores['Name'] == self.name) & (scores['Q-bits'] == self.qbit), 'Accuracy loss AgeDB-30(%)'] = \
                    acc_loss[1]
                scores.loc[
                    (scores['Name'] == self.name) & (scores['Q-bits'] == self.qbit), 'Accuracy CFP_FP (%)'] = \
                    round(self.score_qkeras[0] * 100, 2)
                scores.loc[
                    (scores['Name'] == self.name) & (scores['Q-bits'] == self.qbit), 'Reference CFP_FP (%)'] = \
                    round(self.score_cont[0] * 100, 2)
                scores.loc[
                    (scores['Name'] == self.name) & (scores['Q-bits'] == self.qbit), 'Accuracy loss CFP_FP (%)'] = \
                    acc_loss[2]
            else:
                scores = pd.concat([scores, newscore], ignore_index=True)
        else:
            scores = pd.DataFrame(columns=['Name',
                                           'Layer Type',
                                           'Layer Name',
                                           'Q-bits',
                                           'Accuracy LFW (%)',
                                           'Reference LFW (%)',
                                           'Accuracy loss LFW (%)',
                                           'Accuracy AgeDB-30 (%)',
                                           'Reference AgeDB-30 (%)',
                                           'Accuracy loss AgeDB-30 (%)',
                                           'Accuracy CFP_FP (%)',
                                           'Reference CFP_FP (%)',
                                           'Accuracy loss CFP_FP (%)',
                                           'Folded'])
            scores = pd.concat([scores, newscore], ignore_index=True)
        scores.to_csv(self.df_model, index=False)


    # def _read_model_layers(self):

    # def close_figs(self):
    #     pplt.close(self.fig_weights)

    def close_log(self):
        if self.openlogger:
            self.log.removeHandler(self.log_fh)
            self.openlogger = False

    def open_log(self):
        if not self.openlogger:
            self.log.addHandler(self.log_fh)
            self.openlogger = True

    def print_qstats(self, model):
        model_ops = extract_model_operations(model)
        ops_table = defaultdict(lambda: 0)

        self.log.info("")
        self.log.info("Number of operations in model:")
        for name in sorted(model_ops):
            mode, _, sizes, signs = model_ops[name]["type"]
            number = model_ops[name]["number_of_operations"]
            sign = "s" if sum(signs) > 0 else "u"
            op_name = sign + mode + "_" + str(sizes[0]) + "_" + str(sizes[1])
            ops_table[op_name] += number
            self.log.info("    {:30}: {:5} ({})".format(str(name), str(number), str(op_name)))

        self.log.info("")
        self.log.info("Number of operation types in model:")
        for key in sorted(ops_table.keys()):
            if ops_table[key] > 0:
                self.log.info("    {:30}: {}".format(key, ops_table[key]))

        self.log.info("")
        self.log.info("Weight profiling:")
        for name in sorted(model_ops):
            weight_type = model_ops[name]["type_of_weights"]
            n_weights = model_ops[name]["number_of_weights"]
            if isinstance(weight_type, list):
                for i, (w_type, w_number) in enumerate(zip(weight_type, n_weights)):
                    _, w_sizes, _ = w_type
                    self.log.info("    {:30} : {:5} ({}-bit unit)".format(
                        str(name) + "_weights_" + str(i), str(w_number), str(w_sizes)))
            else:
                _, w_sizes, _ = weight_type
                self.log.info("    {:30} : {:5} ({}-bit unit)".format(
                    str(name) + "_weights", str(n_weights), str(w_sizes)))
            _, b_sizes, _ = model_ops[name]["type_of_bias"]
            b_number = model_ops[name]["number_of_bias"]
            self.log.info("    {:30} : {:5} ({}-bit unit)".format(
                str(name) + "_bias", str(b_number), str(b_sizes)))

        self.log.info("")
        self.log.info("Weight sparsity:")
        total_sparsity, per_layer = get_model_sparsity(self.model, per_layer=True)
        for layer in per_layer:
            self.log.info("    {:30} : {:.4f}".format(str(layer[0]), layer[1]))
        self.log.info("    " + ("-" * 40))
        self.log.info("    {:30} : {:.4f}".format("Total Sparsity", total_sparsity))

    def setup_training(self, x_test, y_test, loss=None, optimizer=None):
        self.log.info('Initialize training...')
        self.valset = (x_test, y_test)

        if loss is not None:
            self.loss_fn = loss
        if optimizer is not None:
            self.optim_fn = optimizer

    def _setup_training(self, model):
        self.callback_v = callbacks.CustomCallback(model, self.name, self.log, self.valset[0], self.valset[1])
        model.compile(loss=self.loss_fn,
                      optimizer=self.optim_fn,
                      metrics=['accuracy'])

    def pre_train_model(self, x_train, y_train, epochs=20, batch_size=128, shuffle=False):
        self.open_log()
        self.log.info('Pre-train model...')
        self.name_ref = self.name
        self.model.summary()
        self._setup_training(self.model)

        self.model.fit(x_train, y_train,
                       batch_size=batch_size,
                       shuffle=shuffle,
                       epochs=epochs,
                       verbose=0,
                       validation_data=self.valset,
                       callbacks=[self.callback_v])

        self.score_init = self.model.evaluate(self.valset[0], self.valset[1], verbose=2)
        self.log.info("Test loss {:.4f}, accuracy {:.2f}%".format(self.score_init[0], self.score_init[1] * 100))
        self.model.save_weights(self.mdir + 'pre_trained/' + self.name)
        self.model.save(self.mdir + 'pre_trained/' + self.name)

        self.model.fit(x_train, y_train,
                       batch_size=batch_size,
                       shuffle=shuffle,
                       epochs=epochs,
                       verbose=0,
                       validation_data=self.valset,
                       callbacks=[self.callback_v])

        self.score_cont = self.model.evaluate(self.valset[0], self.valset[1], verbose=2)
        self.log.info("Test loss {:.4f}, accuracy {:.2f}%".format(self.score_cont[0], self.score_cont[1] * 100))
        self.model.save_weights(self.mdir + 'trained/' + self.name)
        self.model.save(self.mdir + 'trained/' + self.name)
        self.close_log()

    def qkeras_train_bit(self, x_train, y_train, epochs=20, batch_size=128, shuffle=False):
        self.open_log()
        other_pretrained = False
        if self.score_cont[1] == 0:
            try:
                pretrained = self.name_ref[-1] + '0'
                self.model.load_weights(f'models/{pretrained}/trained/{pretrained}')
                self._setup_training(self.model)
                other_pretrained = True
            except:
                self.pre_train_model(x_train, y_train)
            self.score_cont = self.model.evaluate(self.valset[0], self.valset[1], verbose=2)
        self.log.info('Quantize model using QKeras...')
        if other_pretrained:
            self.model.load_weights(f'models/{pretrained}/pre_trained/{pretrained}')
        else:
            self.model.load_weights(self.mdir + 'pre_trained/' + self.name_ref)

        self.quantize = True
        for nr, bit in enumerate(range(8, 1, -1)):
            self.log.info(f'Quantizing {bit}-bits...')
            self.qbit = bit
            self.qmodel = self.get_model()
            # self.qmodel.summary()
            self.name = self.name_ref + '_Qkeras' + str(self.qbit)
            self._setup_training(self.qmodel)
            if other_pretrained:
                self.model.load_weights(f'models/{pretrained}/pre_trained/{pretrained}')
            else:
                self.model.load_weights(self.mdir + 'pre_trained/' + self.name_ref)
            self.qmodel.fit(x_train, y_train,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            epochs=epochs,
                            verbose=0,
                            validation_data=self.valset,
                            callbacks=[self.callback_v])
            self.qmodel.save(self.mdir + 'QKeras/' + self.name)
            self.name = self.name_ref + '_Qkeras_quantized' + str(self.qbit)
            if self.folded:
                self.qmodel_unfolded = bnf.unfold_model(self.qmodel)
            else:
                self.qmodel_unfolded = self.qmodel

            self.qmodel_quantized = self.get_model()
            self.name = self.name_ref + '_Qkeras_quantized' + str(self.qbit)
            self.qmodel_quantized.set_weights(self.qmodel.get_weights())
            if self.folded:
                self.qmodel_quantized = bnf.unfold_model(self.qmodel_quantized)
            self._setup_training(self.qmodel_quantized)

            print('quantization: ')
            model_save_quantized_weights(self.qmodel_quantized, self.mdir + 'QKeras/' + self.name)
            self.qmodel_quantized.save(self.mdir + 'QKeras/' + self.name)
            self._setup_training(self.qmodel_quantized)
            self.score_qkeras = self.qmodel_quantized.evaluate(self.valset[0], self.valset[1], verbose=2)
            self.log.info("Test loss {:.4f}, accuracy {:.2f}%".format(self.score_qkeras[0], self.score_qkeras[1] * 100))
            self.log_scores_bits()
            if bit % 2 == 0:
                self.print_qstats(self.qmodel_quantized)
                self.plot_multi_quantdiff()
        self.close_log()

    # def qkeras_train(self, x_train, y_train, epochs=20, batch_size=128, shuffle=False):
    #     self.open_log()
    #     self.log.info('Quantize model using QKeras...')
    #     self.model.load_weights(self.mdir + 'pre_trained/' + self.name_ref)
    #     self.quantize = True
    #     self.qmodel = self.get_model()
    #     self.name = self.name_ref + '_Qkeras'
    #     self.qmodel.summary()
    #     self._setup_training(self.qmodel)
    #     tf.keras.utils.plot_model(self.qmodel, self.img_s + self.name + '.png', show_shapes=True)
    #     self.qmodel.load_weights(self.mdir + self.name_ref)
    #     self.qmodel.fit(x_train, y_train,
    #                     batch_size=batch_size,
    #                     shuffle=shuffle,
    #                     epochs=epochs,
    #                     verbose=0,
    #                     validation_data=self.valset,
    #                     callbacks=[self.callback_v])
    #
    #     print('score before quantization: ')
    #     print(self.qmodel.trainable_weights[0].numpy()[0][0])
    #     self.score_qkeras = self.qmodel.evaluate(self.valset[0], self.valset[1], verbose=1)
    #     self.score_dict['score_qkeras'] = round(self.score_qkeras[1] * 100, 2)
    #     self.log.info("Test loss {:.4f}, accuracy {:.2f}%".format(self.score_qkeras[0], self.score_qkeras[1] * 100))
    #
    #     self.qmodel_quantized = self.get_model()
    #     self.qmodel_quantized.set_weights(self.qmodel.get_weights())
    #     self._setup_training(self.qmodel_quantized)
    #
    #     print('quantization: ')
    #     model_save_quantized_weights(self.qmodel_quantized, self.mdir + 'QKeras/' + self.name)
    #     print(self.qmodel_quantized.trainable_weights[0].numpy()[0][0])
    #     self._setup_training(self.qmodel_quantized)
    #     self.score_qkeras_quantize = self.qmodel_quantized.evaluate(self.valset[0], self.valset[1], verbose=1)
    #     self.score_dict['score_qkeras_quant'] = round(self.score_qkeras_quantize[1] * 100, 2)
    #
    #     self.print_qstats(self.qmodel_quantized)
    #     self.plot_quantdiff()
    #     self.close_log()

    def tfqat_train(self, x_train, y_train, epochs=20, batch_size=128, shuffle=False):
        self.open_log()
        self.log.info('Quantize model using Tensorflow Quantize Aware Training...')
        if self.model is None:
            return "Train model first"
        self.quantize = False
        self.tf_qmodel = self.get_model()
        self.name = self.name_ref + '_TFQAT'
        self.tf_qmodel.load_weights(self.mdir + 'pre_trained/' + self.name_ref)
        self.tf_qmodel.summary()

        self.tf_qmodel = tfmot.quantization.keras.quantize_model(self.tf_qmodel)
        self._setup_training(self.tf_qmodel)

        self.score_dict['score_tf_preQAT'] = round(self.tf_qmodel.evaluate(self.valset[0], self.valset[1], verbose=2)[1] * 100, 2)
        self.tf_qmodel.fit(x_train, y_train,
                           batch_size=batch_size,
                           shuffle=shuffle,
                           epochs=epochs,
                           verbose=0,
                           validation_data=self.valset,
                           callbacks=[self.callback_v])
        self.score_tf_qmodel = self.tf_qmodel.evaluate(self.valset[0], self.valset[1], verbose=2)
        self.score_dict['score_tf_QAT'] = round(self.score_tf_qmodel[1] * 100, 2)
        self.log.info("Test loss {:.4f}, accuracy {:.2f}%".format(self.score_tf_qmodel[0], self.score_tf_qmodel[1] * 100))
        self.tf_qmodel.save_weights(self.mdir + 'TF_QAT/' + self.name)

        self.plot_quantdiff()
        self.close_log()

    def tfqat_test(self, x_train):
        def representative_data_gen():
            for input_value in tf.data.Dataset.from_tensor_slices(x_train).batch(1).take(100):
                # Model has only one input so each data point has one element.
                yield [input_value]

        self.open_log()
        self.log.info('Transform Tensorflow Quantize Aware Trained model to TFLite...')

        for i in range(4):
            converter = tf.lite.TFLiteConverter.from_keras_model(self.tf_qmodel)
            converter.experimental_enable_resource_variables = True
            if i > 0:
                converter.optimizations = [tf.lite.Optimize.DEFAULT]
            elif i > 1:
                converter.representative_dataset = representative_data_gen
            elif i > 2:
                # Ensure that if any ops can't be quantized, the converter throws an error
                converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
                # Set the input and output tensors to uint8 (APIs added in r2.3)
                converter.inference_input_type = tf.uint8
                converter.inference_output_type = tf.uint8
            tflite = converter.convert()
            file_name = self.mdir + self.name + '_tflite' + str(i) + '.tflite'
            with open(file_name, 'wb') as f:
                f.write(tflite)
            self.score_dict['size(kB)_tflite'+str(i)] = os.path.getsize(file_name) / 1000

            try:
                test_img_idx = range(self.valset[0].shape[0])
                predicts, timing = tfh.run_tflite_model(self.log, tflite, self.valset[0], self.valset[1], test_img_idx)
                accuracy = (np.sum(np.argmax(self.valset[1], axis=1) == predicts) * 100) / len(self.valset[0])
                self.score_dict['inf(ms)_tflite'+str(i)] = round(timing * 1000, 3)
                self.score_dict['score(%)_tflite' + str(i)] = round(accuracy, 2)
                self.log.info('tflite%s model accuracy is %.4f%% (Number of test samples=%d)' % (
                    str(i), accuracy, len(self.valset[0])))

            except Exception as e:
                print('tflite'+str(i), e)
                self.log.error('tflite'+str(i) + ': ' + str(e))

        scores = {self.name_ref: self.score_dict}
        scores_df = pd.DataFrame(scores).transpose()

        if os.path.exists(self.tflite_scores):
            scores_total = pd.read_csv(self.tflite_scores, index_col=0)
            scores_total = pd.concat([scores_total, scores_df])
        else:
            scores_total = scores_df
        scores_total.to_csv(self.tflite_scores)

        self.close_log()







    # def _fill_array(self):
    #     i = 1
    #     l = 0
    #
    #     for dlayer in self.model.layers:
    #         l_name = self._get_layer_type(dlayer.name)
    #         try:
    #             layer = dlayer.layer if self.quantize else dlayer
    #         except:
    #             layer = dlayer
    #
    #         if l_name == 'input':
    #             np.put(self.array[:, l], self.array.shape[l] // 2, i)
    #             i += 1
    #             l += 1
    #
    #         if l_name == 'conv':
    #             for j in range(layer.filters):
    #                 np.put(self.array[:, l], j, i)
    #                 i += 1
    #                 np.put(self.array[:, l + 1], j, i)
    #                 i += 1
    #             l += 2
    #         if l_name == 'separable':
    #             for j in range(layer.depthwise_kernel.shape[2]):
    #                 np.put(self.array[:, l], j, i)
    #                 i += 1
    #                 np.put(self.array[:, l + 1], j, i)
    #                 i += 1
    #                 if j < layer.filters:
    #                     np.put(self.array[:, l + 2], j, i)
    #                     i += 1
    #             l += 3
    #         if l_name == 'add':
    #             for j in range(layer.output_shape[-1]):
    #                 np.put(self.array[:, l], j, i)
    #                 i += 1
    #             l += 1
    #         if l_name == 'bn' or l_name == 'act':
    #             for j in range(layer.output_shape[-1]):
    #                 np.put(self.array[:, l], j, i)
    #                 i += 1
    #             l += 1
    #         if l_name == 'dense':
    #             self.array[:, l] = i

    # def _get_layer_type(self, name):
    #     try:
    #         tmp = int(name[-1])
    #         return name.split("_")[-2]
    #     except:
    #         return name.split("_")[-1]
    #
    # def _get_filter_first(self, dlayer, kernel):
    #     try:
    #         layer = dlayer.layer if self.quantize else dlayer
    #     except:
    #         layer = dlayer
    #
    #     filter_nr = layer.filters
    #     filter_index = kernel.shape.index(filter_nr)
    #     if filter_index == 0:
    #         return kernel
    #     if filter_index == 1:
    #         return np.transpose(kernel, (1, 0))
    #     if filter_index == 2:
    #         return np.transpose(kernel, (2, 0, 1))
    #     if filter_index == 3:
    #         return np.transpose(kernel, (3, 0, 1, 2))

    def _update_weight_log(self, array, writable='a'):
        with open(self.img_w + 'weight_log.csv', writable, newline='') as f:
            writer = csv.writer(f)
            writer.writerows(array)
            f.close()








