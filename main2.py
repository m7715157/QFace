import numpy as np
import random
import os

import tensorflow as tf
import keras.callbacks
import keras.utils as ku
from tflite_helper import *
from models2 import MNISTModel
from keras.datasets import fashion_mnist
from keras.datasets import mnist
from keras.optimizers import *
import matplotlib.pyplot as plt
import matplotlib
from collections import Counter
import logging
import pandas as pd
logging.getLogger('matplotlib').setLevel(logging.WARNING)
logging.getLogger('tensorflow').setLevel(logging.WARNING)
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
os.environ['TF_DETERMINISTIC_OPS'] = '1'
matplotlib.use('Agg')


def set_seed():
    random.seed(2)
    np.random.seed(2)
    tf.random.set_seed(2)
    try:
      tf.config.experimental.enable_op_determinism()
    except:
      pass


# Load data to the correct datatype
def pre_process_float(x1, x2, y1, y2, is_float=True):
    """ Load data to the correct datatype """
    if is_float:
        x1 = (x1.reshape(-1, 28, 28, 1) - 127.5).astype('float32')
        x2 = (x2.reshape(-1, 28, 28, 1) - 127.5).astype('float32')
        x1 /= 127.5
        x2 /= 127.5
    else:
        x1 = (x1.reshape(-1, 28, 28, 1) - 128).astype('float32')
        x2 = (x2.reshape(-1, 28, 28, 1) - 128).astype('float32')
    print(x1.shape[0], 'train samples')
    print(x2.shape[0], 'test samples')
    num_classes = 10

    # convert class vectors to binary class matrices
    y1 = ku.np_utils.to_categorical(y1, num_classes)
    y2 = ku.np_utils.to_categorical(y2, num_classes)
    return x1, x2, y1, y2, num_classes


# Load dataset as train and test sets
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test, y_train, y_test, num_class = pre_process_float(x_train, x_test, y_train, y_test)

# modellst = ['mnist0', 'mnist1', 'mnist2', 'mnist3', 'mnist4', 'mnist5']
modellst = ['mnist0', 'mnist1', 'mnist2', 'mnist3', 'mnist4', 'mnist5', 'mnist6', 'mnist7', 'mnist8', 'mnist9', 'mnist10', 'mnist11', 'mnist12', 'mnist13']

for mdl in modellst:
    set_seed()

    # Create model
    print(mdl)
    model = MNISTModel(mdl)
    model.summary()
    try:
        model.setup_training(x_test, y_test)
        model.pre_train_model(x_train, y_train)
        model.qkeras_train_bit(x_train, y_train)
    except:
        print(f'!!!!!!!!!!!!!ERROR in quantizing MNIST {mdl}!!!!!!!!!!!!!!!!!!!!')

# Load dataset as train and test sets
(x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()
x_train, x_test, y_train, y_test, num_class = pre_process_float(x_train, x_test, y_train, y_test)

for mdl in modellst:
    set_seed()

    # Create model
    print(mdl)
    model = MNISTModel(mdl, dataset='Fashion')
    model.summary()
    try:
        model.setup_training(x_test, y_test)
        model.pre_train_model(x_train, y_train)
        model.qkeras_train_bit(x_train, y_train)
    except:
        print(f'!!!!!!!!!!!!!ERROR in quantizing MNIST FASHION {mdl}!!!!!!!!!!!!!!!!!!!!')